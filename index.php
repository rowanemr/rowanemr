<?php
	require_once 'core/init.php';
	include 'templates/header.php';
	if(!logged_in()) {
		include 'templates/index-logged-out.php';
	}
	else {
		include 'templates/index-logged-in.php';
	}
	include 'templates/footer.php';
?>