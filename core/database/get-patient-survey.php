<?php
    require "../init.php"; //Initialize database and core functions.
    global $db; //Database connection.
    $title = "";
    $surveyData = array();
    
    $sql = "SELECT id, title FROM SurveyBank";
    $result = $db->query($sql);
    
    /**
     * We will select a random survey to give to the patient.
     */
    if($result->num_rows > 0)
    {
        $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
        $size = sizeof($row);
        $pickID = rand(0, $size-1); //Get random id between 0 and size of array of ids returned.
        $title = $row[$pickID]["title"]; //The pickID is the random survey selected.
        array_push($surveyData, $title);
        array_push($surveyData, $row[$pickID]["id"]);
        
        $sql = "SELECT survey_id, question, twoAnswers, comment FROM Surveys WHERE survey_id='" . $row[$pickID]["id"] . "'";
        $result = $db->query($sql);
        
        if($result->num_rows > 0)
        {
            $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
            
            for($i = 0; $i < sizeof($row); $i++)
            {
                $question = new stdClass; //Create anonymous question object.
                $question->question = $row[$i]["question"];
                $question->twoAnswers = $row[$i]["twoAnswers"];
                $question->comment = $row[$i]["comment"];
                
                array_push($surveyData, $question);
            }
            echo json_encode($surveyData);
        }
        else
        {
            $error = "selectDetailsFailed";
            echo json_encode("selectDetailsFailed");
        }
    }
    else //This isn't necessarily bad; just means no one created a survey yet.
    {
        $error = "empty";
        echo json_encode($error);
    }
?>