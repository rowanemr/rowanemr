<?php
/**
 * Created by PhpStorm.
 * User: amandeepsingh
 * Date: 12/4/15
 * Time: 9:34 PM
 */


include '../init.php';
error_reporting(E_ALL);
ini_set('display_errors', 1);

//testing patient ID
if (!isset($_GET['token']) or !is_valid_token($_GET['token']))
    die("Valid Token required");
$token = $_GET['token'];
$patient_id =  get_patient_id_from_token($token);
$slot_id = get_slot_id_from_token($token);

//Getting from POST
$conditions = $_POST["conditions"];
$diagnosis_dates = $_POST["diagnosis-dates"];
$medications = $_POST["medications"];
$surgeries = $_POST["surgeries"];
$surgery_dates = $_POST["surgery-dates"];

$medication_changes = clean_up($_POST["medication-changes"]);
$allergies = clean_up($_POST["allergies"]);
$physical_trauma = clean_up($_POST["physical-trauma"]);
$goals = clean_up($_POST["goals"]);
$health_expectations = clean_up($_POST["health-expectations"]);
$nutritional_topics = clean_up($_POST["nutritional-topics"]);
$medical_concerns = clean_up($_POST["medical-concerns"]);
$do_aware = clean_up($_POST["do-aware"]);
$do_visited = clean_up($_POST["do-visited"]);
$omm_aware = clean_up($_POST["omm-aware"]);
$omm_performed = clean_up($_POST["omm-performed"]);
$hear_about = clean_up($_POST["hear-about"]);
if($hear_about == 'other'){
    $hear_about = clean_up($_POST["other-explanation"]);
}

delete_old_records($patient_id); //delete old conditions so we can update with the new
//  Insert Existing Medical Conditions
for ($i = 0; $i < count($conditions); $i++)
    insert_medical_condition($patient_id, $slot_id, $conditions[$i], $diagnosis_dates[$i]);

//  Insert Medications
for ($i = 0; $i<count($medications); $i++)
    insert_medication($patient_id, $slot_id, $medications[$i]);

//  Insert Surgery/Surgery Dates
for ($i = 0; $i<count($surgeries); $i++)
    insert_surgery($patient_id, $slot_id, $surgeries[$i], $surgery_dates[$i]);

//INSERT or UPDATE DATABASE

$addnew = true;
if ($addnew == 1) {
    //create insert query
    $sql = "INSERT INTO PatientExistingConditions (patient_id, visit_date_id, changes_medication_two_weeks, has_allergies, has_physical_trauma, personal_goals, health_expectations, interested_nutritional_topics, other_concerns, knows_about_do, been_do_before, knows_omm, had_omm_before, heard_about_clinic, created, last_modified) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),now())";
}
else {
    //create update query
    $sql = "UPDATE PatientExistingConditions SET changes_medication_two_weeks=?, has_allergies=?, has_physical_trauma=?, personal_goals=?, health_expectations=?, interested_nutritional_topics=?, other_concerns=?, knows_about_do=?, been_do_before=?, knows_omm=?, had_omm_before=?, heard_about_clinic=?, created=?, last_modified=? WHERE patient_id = $patient_id";
}


//prepare
if(!($stmt = $db->prepare($sql))) {
    echo "Prepare failed: (" . $db->errno . ") " . $db->error;
}
//bind
if (!$stmt->bind_param("ssssssssssssss", $patient_id, $slot_id, $medication_changes, $allergies, $physical_trauma, $goals, $health_expectations, $nutritional_topics, $medical_concerns, $do_aware, $do_visited, $omm_aware, $omm_performed, $hear_about)){
    echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
}

//execute
if ($stmt->execute()) {
    if ($addnew) {
        $patient = $db->insert_id; // get the id, if insert, to pass in the redirect url
    }
    $result = $db->query("UPDATE Schedule SET completed_paperwork = 'Y' WHERE token = '$token'");
    die('Thank You. Please return this tablet. <br><a href ="../../">Click here to login again</a>');
    //TODO ADD LINK
    //header("Location: ../../"); This will direct to the next form to be added
}
else {
    echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
}


$db->close();

// To handle the update functionality, we are going to delete what we previously had and just re-insert
// This is much easier than trying to figure out what the patient deleted on the form itself
// E.g. on the Existing Medical Conditions/Surgeries page you can add/remove conditions, medications, surgeries
function delete_old_records($patient_id)
{
    global $db;
    $query = "DELETE FROM PatientCurrentMedicine WHERE patient_id = '$patient_id'";
    $db->query($query) or die(mysqli_error($db));

    $query = "DELETE FROM PatientMedicalConditions WHERE patient_id = '$patient_id'";
    $db->query($query) or die(mysqli_error($db));

    $query = "DELETE FROM PatientPastSurgery WHERE patient_id = '$patient_id'";
    $db->query($query) or die(mysqli_error($db));

}


function insert_medical_condition($patient_id, $visit_date_id, $condition, $date)
{
    global $db;
    if (trim($condition) == '' or trim($date) == '')
        return; // don't insert
    $condition = clean($condition);
    $date = clean($date); // not a date but a string, since patient might not know exact date (last year sometime?)
    $query = "INSERT INTO PatientMedicalConditions (patient_id, visit_date_id, medical_condition, date_of_diagnosis, created, last_modified)  ";
    $query .= "VALUES (?, ?, ?, ?, now(), now()) ";
    if (!($stmt = $db->prepare($query)))
        echo "Prepare failed: (" . $db->errno . ") " . $db->error;

    if (!($stmt->bind_param("ssss", $patient_id, $visit_date_id, $condition, $date)))
        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;

    if (!($stmt->execute()) )
        echo "Insert Med Condition Execute failed: $query  (" . $stmt->errno . ") " . $stmt->error;
    $stmt->close();
}

function insert_medication($patient_id, $visit_date_id, $medication)
{
    global $db;
    if (trim($medication) == '')
        return; //don't insert
    $medication = clean($medication);
    $query = "INSERT INTO PatientCurrentMedicine (patient_id, visit_date_id, medicine_name, created, last_modified) ";
    $query .= "VALUES (?, ?, ?, now(), now()) ";
    if (!($stmt = $db->prepare($query)))
        echo "Prepare failed: (" . $db->errno . ") " . $db->error;

    if (!($stmt->bind_param("sss", $patient_id, $visit_date_id, $medication)))
        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;

    if (!($stmt->execute()) )
        echo "Insert Medication Execute failed: $query  (" . $stmt->errno . ") " . $stmt->error;
    $stmt->close();

}

function insert_surgery($patient_id, $visit_date_id, $surgery, $date)
{
    global $db;
    if (trim($surgery) == '' or trim($date) == '')
        return; // don't insert
    $date = clean($date);
    $surgery = clean($surgery);
    $query = "INSERT INTO PatientPastSurgery (patient_id, visit_date_id, type_surgery, date_of_surgery, created, last_modified)  ";
    $query .= "VALUES (?, ?, ?, ?, now(), now()) ";
    if (!($stmt = $db->prepare($query)))
        echo "Prepare failed: (" . $db->errno . ") " . $db->error;

    if (!($stmt->bind_param("ssss", $patient_id, $visit_date_id, $surgery, $date)))
        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;

    if (!($stmt->execute()) )
        echo "Insert Surgery Execute failed: $query  (" . $stmt->errno . ") " . $stmt->error;
    $stmt->close();

}


