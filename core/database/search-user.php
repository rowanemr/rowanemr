<?php
/**
 * Created by PhpStorm.
 * User: Darren
 * Date: 1/5/16
 * Time: 10:18 AM
 */
if(!isset($_GET['query'])) {
    echo json_encode([]);
    exit();
}
	$db = new PDO('mysql:host=rwsmith.me;dbname=RowanEMR', 'dev', 'rowanemr');
	$query = "SELECT username AS username FROM Authentication ";
	$username = $db->prepare($query);
	// and return to typeahead
	$username->execute([
        'query' => "{$_GET['query']}%"
    ]);
	$array = array();
	while($row = $username->fetch(PDO::FETCH_ASSOC)) {
        $array[] = array (
            'username' => $row['username'],

        );
    }
	//var_dump($username->fetchALL);
	echo json_encode($array);
	flush();
?>