<?php
    require "../init.php"; //Initialize database and core functions.
    global $db; //Database connection.
    
    $survey = $_POST["survey"]; //Get the survey we want to get the results for.
    $surveyResults = array(); //User responses array.
    $questions = array(); //All questions from survey.
    $package = array(); //data to be returned back.
    
    $sql = "SELECT id FROM SurveyBank WHERE title='" . $survey ."'";
    $result = $db->query($sql);
    
    if($result->num_rows > 0)
    {
        $row = $result->fetch_assoc();
        $sql = "SELECT question FROM Surveys WHERE survey_id='" . $row["id"] . "'";
        $result = $db->query($sql);
        
        if($result)
        {
            $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
            for($i = 0; $i < sizeof($row); $i++)
            {
                array_push($questions, $row[$i]["question"]);
            }
            
            array_push($package, $questions); //Load into package.
            
            $sql = "SELECT question, question_number, answer FROM SurveyResponses WHERE title='" . $survey . "'";
            $result = $db->query($sql);
            
            if($result)
            {
                $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
                for($i = 0; $i < sizeof($row); $i++)
                {
                    $response = new stdClass; //Create anonymous Response object.
                    $response->question = $row[$i]["question"];
                    $response->questionNumber = $row[$i]["question_number"]; 
                    $response->answer = $row[$i]["answer"];
                    
                    array_push($surveyResults, $response);
                }
                
                array_push($package, $surveyResults);
                echo json_encode($package);
            }
            else
            {
                //No one answered this survey yet.
                $noResults = true;
                json_encode($noResults);
            }
        }
        else
        {
            $error = "selectSurveyFailed";
            echo json_encode($error);
        }
    }
    else
    {
        $error = "selectSurveyBankFailed";
        echo json_encode($error);
    }
?>