<?php
require '../init.php';
global $db;

  //if user does not have ADMIN role send them back to index
  if(!get_user_role($_SESSION['id']) === "ADMIN") {
    header("Location: index.php");  
  }

  $id = clean_up($_GET['id']);

  //First delete all logs from this user in Log table because each log record has a foreign key constraint 
  //referencing the user_id in the Authentication table
  $sql = "DELETE FROM Log WHERE log_user_id = '$id'";
  if ($db->query($sql) === TRUE) {
    //successfully deleted logs 
  } else {
      echo "Error deleting record: " . $db->error;
      $db->close();
  }
  
  //Now we can delete the user from Authentication table without foreign key constraint problems
  $sql = "DELETE FROM Authentication WHERE user_id = '$id'";
  if ($db->query($sql) === TRUE) {
    //successfully deleted user
    $db->close();
    header("Location: ../../users.php");  
  } else {
      echo "Error deleting record: " . $db->error;
        $db->close();
  }

