<?php
    require "../init.php"; //Initialize database and core functions.
    global $db; //Database connection.
    
    $titles = array(); //holds all titles in database.
    
    $sql = "SELECT * FROM SurveyBank";
    $result = $db->query($sql);
    
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            array_push($titles, $row["title"]); //Push urls to array.
        }
        
        echo json_encode($titles);
    }
    else
    {
        $empty = true;
        echo json_encode($empty);
    }
?>