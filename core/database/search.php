<?php
if(!isset($_GET['query'])) {
	echo json_encode([]);
	exit();
}
	$db = new PDO('mysql:host=rwsmith.me;dbname=RowanEMR', 'dev', 'rowanemr');
	$query = "SELECT last_name, first_name, patient_id, DATE_FORMAT(date_of_birth,'%m/%d/%Y') AS dob FROM Patients ";
	$query .= "WHERE last_name LIKE :query OR first_name LIKE :query "; // check for last or first name
    $query .= "OR CONCAT(first_name, ' ', last_name) LIKE :query"; // check the case where it's already done
	$patients = $db->prepare($query);
	// and return to typeahead
	$patients->execute([
		'query' => "{$_GET['query']}%"
	]);
	$array = array();
	while($row = $patients->fetch(PDO::FETCH_ASSOC)) {
		$array[] = array (
			'first_name' => $row['first_name'],
			'last_name' => $row['last_name'],
			'full_name' => "{$row['first_name']} {$row['last_name']}",
			'patient_id' => $row['patient_id'],
			'dob' => $row['dob']
		);
	}
	//var_dump($patients->fetchALL);
	echo json_encode($array);
	flush();
?>