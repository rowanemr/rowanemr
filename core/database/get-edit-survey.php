<?php
    require "../init.php"; //Initialize database and core functions.
    global $db; //Database connection.
    
    $title = $_POST["title"];
    $surveyData = array();
    
    $sql = "SELECT id FROM SurveyBank WHERE title='" . $title . "'";
    $result = $db->query($sql);
    if($result->num_rows === 1)
    {
        $row = $result->fetch_assoc();
        $surveyID = $row["id"];
        
        $sql = "SELECT question, twoAnswers, comment FROM Surveys WHERE survey_id='" . $surveyID . "'";
        $result = $db->query($sql);
        
        if($result->num_rows > 0)
        {
            $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
            array_push($surveyData, $surveyID);
            for($i = 0; $i < sizeof($row); $i++)
            {
                $question = new stdClass; //Create anonymous question object.
                $question->question = $row[$i]["question"];
                $question->twoAnswers = $row[$i]["twoAnswers"];
                $question->comment = $row[$i]["comment"];
                
                array_push($surveyData, $question);
            }
             echo json_encode($surveyData);
        }
        else
        {
            $error = "errorSurveys";
            echo json_encode($error);
        }
    }
    else
    {
        $error = "errorSurveyBank";
        echo json_encode($error);
    }
?>