<?php
include '../init.php';
if(empty($_POST) == false) {
	$username = $_POST['username'];
	//Encrypt password with SHA-256.
	$password = eclean($_POST['password']);

	if(empty($username) == true || empty($password) == true) {
		$errors[] = 'Enter a username and password';
	}
	else if (user_exists($username) == false) {
		$errors[] = 'Username does not exist!';
	}
	else {
		$login = login($username, $password);
		if($login == false) {
			$errors[] = 'Invalid password.';
		}
		else {
			$_SESSION['id'] = $login; //login will return the id
			//Log the action. First argument is the user's id, second is a string describing the action taken.
   			create_log($_SESSION['id'], "Logged into system");
			header('Location: ../../index.php');
		}
	}
	print_r($errors);
}

?>
