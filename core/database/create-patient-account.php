<?php
    require '../init.php';
    global $db; //Database connection.
    
    if(empty($_POST) === false)
    {
        $firstName = $_POST["firstname"];
        $lastName = $_POST["lastname"];
        $dateOfBirth = $_POST["dateofbirth"];
        //Hash with bcrypt.
        $password = password_hash(eclean($_POST["password"]), PASSWORD_BCRYPT);
        
         if(empty($firstName) == true || empty($lastName) == true || empty($dateOfBirth) == true || empty($password)) {
            $errors[] = 'Enter a firstname, lastname and date of birth';
            print_r($errors);
        }
        else if (user_exists($firstName) == false) {
            $errors[] = 'Firstname does not exist!';
            print_r($errors);
        }
        else if (user_exists($lastName) == false) {
            $errors[] = 'Lastname does not exist!';
            print_r($errors);
        }
        
        else
        {
            $login = get_patient_id_create_if_new($firstName, $lastName, $dateOfBirth);
            if($login == false)
            {
                $errors[] = 'Invalid date of birth.';
                print_r($errors);
            }
            else
            {
                $sql = "INSERT INTO PatientAccounts (patient_id, first_name, last_name, date_of_birth, password, created, last_modified) VALUES ('" . $login . "', '" . $firstName . "', '" . $lastName . "', '" . sql_friendly_date($dateOfBirth) . "', '" . $password . "', now(), now())";
                $result = $db->query($sql);
                
                if($result)
                {
                    header("Location: ../../templates/patient-login-content.php");
                }
            }
        }
    }
?>