<?php
require '../init.php';
global $db;

    //If user does not have ADMIN role send them back to index
    if(!get_user_role($_SESSION['id']) === "ADMIN")
    {
      header("Location: index.php");  
    }
    
    $title = eclean($_POST["title"]); //Clean user input.
    
    //Get survey id.
    $sql = "SELECT id FROM SurveyBank WHERE title='" . $title . "'";
    $result = $db->query($sql);
    
    if($result->num_rows === 1) //Assing survey id.
    {
        $row = $result->fetch_assoc();
        $surveyID = $row["id"];
        
        $sql = "DELETE FROM SurveyBank WHERE id='" . $surveyID ."'";
        $result = $db->query($sql);
        
        if($result) //Survey in bank deleted.
        {
            //Delete survey questions, types, ids, etc.
            $sql = "DELETE FROM Surveys WHERE survey_id='" . $surveyID . "'";
            $result = $db->query($sql);
            
            if($result === false) //Deletion failed.
            {
                $error = "surveysFailed";
                echo json_encode($error);
            }
            else
            {
                $error = "success";
                echo json_encode($error);
            }
        }
        else
        {
            $error = "surveyBankFailed";
            echo json_encode($error);
        }
    }
    else
    {
        $error = "selectFailed";
        echo json_encode($error);
    }
?>