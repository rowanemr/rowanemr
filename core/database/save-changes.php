<?php
    require "../init.php"; //Initialize database and core functions.
    global $db; //Database connection.
    
    $title = eclean($_POST["title"]); //Check title for vaild input.
    $questions = $_POST["questions"]; //Question array.
    $length = sizeof($questions);
    $surveyID = $_POST["surveyID"];
    $noIssues = true; //If we have no issues we will just continue onto sending success value.
    
    //Check each question for blanks or SQL injection.
    for($i=0; $i < $length; $i++)
    {
        $stringToClean = $questions[$i]["question"];
        eclean($stringToClean);
    }
    
    //First get the ids for each question in Surveys.
    $sql = "SELECT id FROM Surveys WHERE survey_id='" . $surveyID . "'";
    $result = $db->query($sql);
    
    if($result)
    {
        //Get unique id for each question under the proper survey.
        $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
        for($i = 0; $i < $length; $i++)
        {
            //Update question first.
            $sql = "UPDATE Surveys SET question='" . $questions[$i]["question"] . "' WHERE id='" . $row[$i]["id"] . "'";
            $result = $db->query($sql);
            
            if($result) //Check if Update was successful.
            {
               if($questions[$i]["type"] === "yes/no") //Check if type is yes/no.
               {
                    //If it is, update boolean values in table.
                    $sql = "UPDATE Surveys SET twoAnswers=TRUE, comment=FALSE WHERE id='" . $row[$i]["id"] . "'";
                    $result = $db->query($sql);
                    
                    if(!$result) //Check success.
                    {
                        $error = "typeFailed";
                        $noIssues = false;
                        echo json_encode($error);
                        break;
                    }
               }
               else if($questions[$i]["type"] === "comment") //Check if type is comment.
               {
                    $sql = "UPDATE Surveys SET comment=TRUE, twoAnswers=FALSE WHERE id='" . $row[$i]["id"] . "'";
                    $result = $db->query($sql);
                    
                    if(!$result) //Check success.
                    {
                        $error = "typeFailed";
                        $noIssues = false;
                        echo json_encode($error);
                        break;
                    }
               }
            }
            else //Question could not be updated.
            {
                $error = "questionFail";
                $noIssues = false;
                echo json_encode($error);
                break;
            }
        }
        if($noIssues === true) //Everything went smoothely.
        {
            $error = "success";
            echo json_encode($error);
        }
    }
    else //Could not select anything.
    {
        $error = "selectFailed";
        echo json_encode($error);
    }
?>