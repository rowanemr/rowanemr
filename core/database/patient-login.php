<?php
/**
 * Created by PhpStorm.
 * User: Darren
 * Date: 12/22/15
 * Time: 9:49 AM
 */
require '../init.php';
global $db; //Database connection.

if(empty($_POST) === false) {
    $first_name = $_POST['firstname'];
    $last_name = $_POST['lastname'];
    $date_of_birth = $_POST['dateofbirth'];
    //Hash password via BCrypt .
    $password = eclean($_POST['password']);
    
    if(empty($first_name) == true || empty($last_name) == true || empty($date_of_birth) == true || empty($password)) {
        $errors[] = 'Enter a firstname, lastname and date of birth';
        print_r($errors);
    }
    else if (user_exists($first_name) == false) {
        $errors[] = 'Firstname does not exist!';
        print_r($errors);
    }
    else if (user_exists($last_name) == false) {
        $errors[] = 'Lastname does not exist!';
        print_r($errors);
    }
    else {
        $login = get_patient_id($first_name, $last_name, $date_of_birth);
        if($login == false) {
            $errors[] = 'Invalid date of birth.';
            print_r($errors);
        }

        else {
            $sql = "SELECT first_name, last_name, date_of_birth, password FROM PatientAccounts WHERE patient_id='" . $login . "'";
            $result = $db->query($sql);
            
            if($result)
            {
                $row = $result->fetch_assoc();
                if($row["first_name"] === $first_name && $row["last_name"] === $last_name && $row["date_of_birth"] === sql_friendly_date($date_of_birth)
                   && password_verify($password, $row["password"]))
                {
                    $_SESSION['patientID'] = $login; //login will return the id
                    //Log the action. First argument is the user's id, second is a string describing the action taken.
                    //create_log($login, "Logged into system");
                    header('Location: ../../templates/patient-dash.php');
                }
                else
                {
                     $errors[] = $password;
                     print_r($errors);
                }
            }
            else
            {
                $errors[] = "It looks like you didn't create an account. Try creating one!";
                print_r($errors);
            }
        }
    }
}

?>