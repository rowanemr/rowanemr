<?php
    require "../init.php"; //Initialize database and core functions.
    global $db; //Database connection.
    
    $title = eclean($_POST["title"]);   //Clean string and check for empty values. Also prevents SQL injection.
    $questions = $_POST["questions"]; //This is an array.
    $length = sizeof($questions);
    $surveyID;
    
    //Check each question for blanks or SQL injection.
    for($i=0; $i < $length; $i++)
    {
        $stringToClean = $questions[$i]["question"];
        eclean($stringToClean);
    }
    
    //Need to check if title exists already to avoid duplicates.
    $sql = "SELECT title FROM SurveyBank WHERE title='" . $title . "'";
    $result = $db->query($sql);
    
    if($result->num_rows === 0)
    {
    
        //First, create the survey blue print for the survey bank.
        $sql = "INSERT INTO SurveyBank (title, created, last_modified) VALUES ('" . $title ."', now(), now())";
        $result = $db->query($sql); //execute.
        
        if($result) //Insertion successful.
        {
            //Now get the new survey id.
            $sql = "SELECT id FROM SurveyBank WHERE title='" . $title . "'";
            $result = $db->query($sql); //execute.
            
            if($result->num_rows > 0)
            {
                $row = $result->fetch_assoc();
                $surveyID = $row["id"];
                
                //Go through each element in array.
                for($i = 0; $i < $length; $i++)
                {
                    //Check if question is a Yes/No or comment question.
                    if($questions[$i]["type"] === "yes/no")
                    {
                        $sql = "INSERT INTO Surveys (survey_id, question, twoAnswers, comment, created, last_modified) VALUES('" . $surveyID . "', '" . $questions[$i]['question'] . "', TRUE, FALSE, now(), now())";
                        $result = $db->query($sql);
                        
                        if(!$result)
                        {
                            $success = "insertFailed";
                            echo json_encode($success);
                            break;
                        }
                    }
                    else if($questions[$i]["type"] === "comment")
                    {
                        $sql = "INSERT INTO Surveys (survey_id, question, twoAnswers, comment, created, last_modified) VALUES('" . $surveyID . "', '" . $questions[$i]['question'] . "', FALSE, TRUE, now(), now())";
                        $result = $db->query($sql);
                        
                        if(!$result)
                        {
                           $success = "insertFailed";
                            echo json_encode($success);
                        }
                    }
                    else //Invalid type.
                    {
                        $success = "invalidType";
                        echo json_encode($success);
                        break;
                    }
                }
                //All instertion successful.
                $success = true;
                echo json_encode($success);
            }
            else //No results found.
            {
                $success = "noTitle";
                echo json_encode($success); 
            }
        }
        else
        {
            $success = "insertFailed";
            echo json_encode($success);
        }
    }
    else
    {
        $success = "titleExists";
        echo json_encode($success);
    }
?>