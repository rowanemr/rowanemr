<?php
require '../init.php';
global $db;
//check if user exists
if (!isset($_GET['token']) or !is_valid_token($_GET['token']))
  die("Valid Token required");

$token = $_GET['token'];
$patient_id = get_patient_id_from_token($token);
$addnew = true; //assume this is a new patient unless we find records
if ($result = $db->query("SELECT * FROM PatientDemographics WHERE patient_id = '$patient_id'"))
{
  $row = $result->fetch_assoc();
  if ($result->num_rows !== 0)
    $addnew = false;
}

  // Get info from post
  $first_name = clean_up($_POST['first-name']);
  $last_name = clean_up($_POST['last-name']);
  $preferred_name = clean_up($_POST['preferred-name']);
  $date_entered = sql_friendly_date(clean_up($_POST['today-date']));
  $date_of_birth = sql_friendly_date(clean_up($_POST['birth-date']));
  $gender = clean_up($_POST['gender-options']);
  $daily_work_performed = clean_up($_POST['daily-work']);
  $completed_education = clean_up($_POST['education-options']);
  $has_history_tobacco = clean_up($_POST['tobacco-options']);
  $alcohol_usage = clean_up($_POST['alcohol-options']);
  $ethnicity = clean_up($_POST['ethnicity-options']);

  //INSERT or UPDATE DATABASE
  if ($addnew) {
      //create insert query
      // Note we insert patient_id on the end of this INSERT query so that
      // we can use the same bind_param() method for both INSERT INTO and UPDATE queries
      $sql = "INSERT INTO PatientDemographics (preferred_name, date_entered, date_of_birth, gender, daily_work_performed, completed_education, has_history_tobacco, alcohol_usage, ethnicity, patient_id, created, last_modified) VALUES (?,?,?,?,?,?,?,?,?,?,now(), now())";
  } 
  else {
      //create update query
    	$sql = "UPDATE PatientDemographics SET preferred_name=?, date_entered=?, date_of_birth=?, gender=?, daily_work_performed=?, completed_education=?, has_history_tobacco=?, alcohol_usage=?, ethnicity=?, last_modified=now() WHERE patient_id = ? LIMIT 1";
  }   

  //prepare
  if(!($stmt = $db->prepare($sql))) {
    echo "Prepare failed: (" . $db->errno . ") " . $db->error;
  }
  //bind
  if (!$stmt->bind_param("ssssssssss", $preferred_name, $date_entered, $date_of_birth, $gender, $daily_work_performed, $completed_education, $has_history_tobacco, $alcohol_usage, $ethnicity, $patient_id)){
    echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
  }

  //execute
  if ($stmt->execute()) {
		if ($addnew) {
			$patient = $db->insert_id; // get the id, if insert, to pass in the redirect url
		}

		header("Location:../../pain-form.php?token=$token");
	}
	else {
	    echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$db->close();
?>