<?php
    require "../init.php"; //Initialize database and core functions.
    global $db; //Database connection.
    
    $title = eclean($_POST["title"]); //Title of survey (check for bad input).
    $surveyID = $_POST["surveyID"]; //ID of the survey.
    $input = $_POST["input"]; //User input array.
    $questions = $_POST["questions"]; //The actual question string.
    $noProblems = true; //Will be set to false if we have a problem inserting.
    
    //Check for bad input.
    for($i = 0; $i < sizeOf($input); $i++)
    {
        $stringToClean = $input[$i]["input"];
        eclean($stringToClean);
    }
    
    //Insert each answer into the database.
    for($i = 0; $i < sizeOf($input); $i++)
    {
        $sql = "INSERT INTO SurveyResponses (survey_id, title, question, question_number, answer, created, last_modified) VALUES ('" . (int)$surveyID . "', '" . $title . "', '" . $questions[$i] . "', '" . (int)$input[$i]["questionNum"] . "', '" . $input[$i]["input"] . "', now(), now())";
        $result = $db->query($sql);
        
        if(!$result)
        {
            $success = false;
            $noProblems = false;
            echo json_encode($success);
            break;
        }
    }
    
    if($noProblems === true)
    {
        $success = true;
        echo json_encode($success);
    }
?>