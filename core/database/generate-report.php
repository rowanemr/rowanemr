<?php
	include '../init.php';
	//Log the action. First argument is the user's id, second is a string describing the action taken.
   	create_log($_SESSION['id'], "Exported Data");

	// pd.gender
	global $db;

	$fields = $_POST['field'];
	$array_length = count($fields);
	$counter = 0;
	$sql = "SELECT";
	$csv = "";
	while($counter <  $array_length) {
		switch($fields[$counter]) {
			case '1':	
				$sql .= " pd.gender";
				$csv .= "gender";
				break;
			case '2':
				$sql .= " pd.alcohol_usage";
				$csv .= "alcohol_usage";
				break;
			case '3':
				$sql .= " pd.has_history_tobacco";
				$csv .= "has_history_tobacco";
				break;
			case '4':
				$sql .= " pp.has_pain_now";
				$csv .= "has_pain_now";
				break;
			case '5':
				$sql .= " pp.activity_onset_pain";
				$csv .= "activity_onset_pain";
				break;
			case '6':
				$sql .= " pp.pain_right_now";
				$csv .= "pain_right_now";
				break;
			case '7':
				$sql .= " pp.pain_at_worst";
				$csv .= "pain_at_worst";
				break;
			case '8':
				$sql .= " pp.pain_at_best";
				$csv .= "pain_at_best";
				break;
			case '9':
				$sql .= " pp.pain_on_average";
				$csv .= "pain_on_average";
				break;
			case '10':
				$sql .= " pec.has_allergies";
				$csv .= "has_allergies";
				break;
			case '11':
				$sql .= " pec.has_physical_trauma";
				$csv .= "has_physical_trauma";
				break;
			case '12':
				$sql .= " pec.knows_about_do";
				$csv .= "knows_about_do";
				break;
			case '13':
				$sql .= " pec.been_do_before";
				$csv .= "been_do_before";
				break;
			case '14':
				$sql .= " pec.knows_omm";
				$csv .= "knows_omm";
				break;
			case '15':
				$sql .= " pec.had_omm_before";
				$csv .= "had_omm_before";
				break;
			case '16':
				$sql .= " pec.heard_about_clinic";
				$csv .= "heard_about_clinic";
				break;
			case '17':
				$sql .= " ov.heart_rate";
				$csv .= "heart_rate";
				break;
			case '18':
				$sql .= " ov.resp_rate";
				$csv .= "resp_rate";
				break;
			case '19':
				$sql .= " ov.height";
				$csv .= "height";
				break;
			case '20':
				$sql .= " ov.weight";
				$csv .= "weight";
				break;
			case '21':
				$sql .= " ov.systolic_bp, ov.diastolic_bp";
				$csv .= "systolic_bp, diastolic_bp";
				break;
			default:
				break;					
		}
		if(1 + $counter == $array_length) {
			$csv .= " \n";
		}
		else {
			$sql .= ",";
			$csv .= ",";
		}
		$counter++;
	}
	$sql .= " FROM PatientDemographics pd";
	$sql .= " JOIN PatientPain pp ON pp.patient_id = pd.patient_id";
	$sql .= " JOIN PatientExistingConditions pec ON pec.patient_id = pp.patient_id AND pec.visit_date_id = pp.visit_date_id";
	$sql .= " JOIN OfficeVisitForm ov ON ov.patient_id = pec.patient_id AND ov.visit_date_id = pec.visit_date_id";
	
	$results = $db->query($sql);
	if($results == false)	{ // fail db  query
		printf("Errormessage: %s\n", $db->error);
	}
	else {  // sql query to string
		$num_col = mysqli_num_fields($results);
		$num_row = mysqli_num_rows($results);
		while($row = $results->fetch_row()) {
			$counter = 0;
			while($counter < $num_col) {
				$csv .= $row[$counter];
				if($counter + 1 != $num_col) {
					$csv .= ',';
				}
				else {
					$csv .= "\n";
				}
				$counter++;
			}
		}
	}
	$filename = "OMM_data_export_" . date('Ymd') . ".csv";
  	header("Content-Disposition: attachment; filename=\"$filename\"");
  	header("Content-Type: application/vnd.ms-excel");
	//echo var_dump($_POST['field']);
	echo $csv;
	//echo var_dump($sql);
	//echo '<br>'.$counter;

	
?>