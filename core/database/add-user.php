<?php
require '../init.php';
global $db;

  // Get info from post
  $username = clean_up($_POST['user_name']);
  $full_name = clean_up($_POST['full_name']);
  //Hash password with SHA-256 encryption.
  $password = password_hash(clean_up($_POST['password']), PASSWORD_BCRYPT);
  $user_role = clean_up($_POST['user_role']);

  if(user_exists($username)) {
    header("Location: ../../add-user.php?userexists=0");
  }
  
 $sql = "INSERT INTO Authentication (username, name, password, user_role, created, last_modified) VALUES (?,?,?,?,now(), now())";
  
  //prepare
  if(!($stmt = $db->prepare($sql))) {
    echo "Prepare failed: (" . $db->errno . ") " . $db->error;
  }
  //bind
  if (!$stmt->bind_param("ssss", $username, $full_name, $password, $user_role)){
    echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
  }

  //execute
  if ($stmt->execute()) {
		if ($addnew) {
			$patient = $db->insert_id; // get the id, if insert, to pass in the redirect url
		}

		header("Location:../../users.php");
	}
	else {
	    echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$db->close();ss
?>$username, $full_name, $password, $user_role, 