<?php
    require "../init.php"; //Initialize database and core functions.
    global $db; //Database connection.
    $url = array(); //url tags for video.
    
    $patientID = $_SESSION["patientID"];
    
    $sql = "SELECT url, description FROM PatientVideos WHERE patient_id='" . $patientID . "'";
    $result = $db->query($sql); //execute.
    
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $URL = new stdClass;
            $URL->url = $row["url"];
            $URL->description = $row["description"];
            array_push($url, $URL); //Push urls to array.
        }
        
        echo json_encode($url);
    }
    else
    {
        $empty = true;
        echo json_encode($empty);
    }
?>