<?php
ini_set('display_errors', 1);

	$db = new mysqli('rwsmith.me', 'dev', 'rowanemr', 'RowanEMR');
	if($db->connect_errno) {
		die("Could not connect to database");
	}


	/**
	 * Will return an escaped version of argument.
	 * Used to sanitize user input and prevent SQL injection.
	 * Either clean() or eclean() should be called on EVERY input.
	 * @param string $toclean String to clean
	 * @return string
	*/
	function clean($toclean)
	{
		global $db;
		return $db->real_escape_string($toclean);
	}

	/**
	 * Prevent empty fields and do user input sanitization.
	 * Will call die() if arg is empty, otherwise will return sanitized string.
	 * Used to prevent empty text input and prevent SQL injection.
	 * Either eclean() or clean() should be called on ALL user input.
	 * @param string $toclean String to clean
	 * @return string
	*/
	function eclean($toclean)
	{
		$toclean = trim($toclean);
		if ($toclean == '')
		{
			// This is mainly to prevent someone trying to mess with our database
			// Client-side validation is performed first and will tell the user
			// which fields to check. This is just a pre-cauation, so we do not
			// have to be report to user what fields to check. If they get this
			// die() message, they have disabled JavaScript or are trying to
			// mess with the system.
			die('Please fill out all form fields.');
		}
		return clean($toclean);
	}



?>
