<?php

require "../init.php"; //Initialize database and core functions.
global $db; //Database connection.

$array = $_POST['posting'];

$patientID = $array['patient_id'];
$desc = $array['description'];
$vidURL = $array['url'];
$videoID = $array['id'];

$headers = get_headers('https://www.youtube.com/oembed?format=json&url=http://www.youtube.com/watch?v=' . $videoID);

if (is_array($headers) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/', $headers[0]) : false)
{
    $query = "INSERT INTO PatientVideos";
    $query .= "(patient_id, description, url, created, last_modified)";
    $query .= "VALUES (' $patientID ', '$desc', '$vidURL', now(), now())";
    $result = $db->query($query); //execute.
    echo "Video added successfully!";
}
else
{
    echo "VIDEO DOES NOT EXIST";
}