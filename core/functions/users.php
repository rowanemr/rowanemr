<?php

function logged_in() {
	return(isset($_SESSION['id'])) ? true : false;
}

function user_exists($username) {
	global $db;
	$username = clean_up($username);
	$db->query("SELECT * FROM Authentication WHERE username = '$username'");
	if(empty($db))
		return false;
	else
		return true;
}

	/**
	 * Returns True if the token is valid, False otherwise
	 * Token is stored in Schedule.Token
	 * @param $token
	 * @return boolean
	 */
	function is_valid_token($token)
	{
		global $db;
		$token = clean_up($token);
		$result = $db->query("SELECT token FROM Schedule WHERE token = '$token'");
		return ($result->num_rows === 1); // 1 row returned means valid token
	}

	function get_patient_id_from_token($token)
	{
		global $db;
		$token = clean_up($token);
		$result = $db->query("SELECT scheduled_patient_id FROM Schedule WHERE token = '$token' LIMIT 1") or die(mysqli_error($db));;
		if ($result->num_rows !== 1)
			die("NUM_ROWS != 1 in get_patient_id_from_token()");
		$row = $result->fetch_assoc();
		return $row['scheduled_patient_id'];
	}

	function get_slot_id_from_token($token)
	{
		global $db;
		$token = clean_up($token);
		$result = $db->query("SELECT slot_id FROM Schedule WHERE token = '$token'");
		if ($result->num_rows != 1)
			die("NUM_ROWS != 1 in get_slot_id_from_token()");
		$row = $result->fetch_assoc();
		return $row['slot_id'];
	}


	/*
	 * Returns the patient ID for the patient having firstName, lastName, birthday
	 * $birthday is in the format of MM/DD/YYYY, ex: 05/10/1995
	 * The function will convert to SQL standard dates
	 */
	function get_patient_id($first_name, $last_name, $birthday)
	{
		global $db;
		$first_name = clean_up($first_name);
		$last_name =  clean_up($last_name);
		$birthday = sql_friendly_date($birthday);
		$result = $db->query("SELECT * FROM Patients WHERE first_name LIKE '$first_name' AND last_name LIKE '$last_name' AND date_of_birth = '$birthday'");
		if (empty($result))
			return null;
		else
		{
			$row = $result->fetch_assoc();
			return $row["patient_id"];
		}
	}

/*
 * Returns the patient first name, last name, birthday of a patient with given ID as an associative array
 * Ex: $info = get_patient_info('1');
 *     $info["first_name"], $info["last_name"], $info["date_of_birth"] are all available
 * Returns null if patient_id could not be found
 * date_of_birth is in MM/DD/YYYY format and not SQL format
 * The function will convert to SQL standard dates
 */
function get_patient_info($patient_id)
{
	global $db;
	$patient_id = clean_up($patient_id);
	$result = $db->query("SELECT first_name, last_name, DATE_FORMAT(date_of_birth,'%m/%d/%Y') AS date_of_birth  FROM Patients WHERE patient_id = '$patient_id'");
	if (empty($result))
		return null;
	else
	{
		$row = $result->fetch_assoc();
		return $row;
	}
}


function sql_friendly_date($date_str)
{
		//$date_str = M/D/Y, ex: 05/20/1995
	$date_str = clean_up($date_str);
	$date_obj = date_create_from_format("m/d/Y", $date_str);
	if ($date_obj == false)
	{
			// This is to handle if we are already given a sql friendly date, e.g. YYYY-MM-DD
			// Chrome by default will use a datepicker giving us the YYYY-MM-DD format
			// But user input or some other datepicker might not
		$date_obj = date_create_from_format("Y-m-d", $date_str);
	}
	if ($date_obj == false)
	{
			// If it's still false, try another format: m-d-Y (dashes instead of slashes)
		$date_obj = date_create("m-d-Y");
	}
	return date_format($date_obj, "Y-m-d");
}

function sql_friendly_timestamp($timestamp_str)
{
		//$timestamp_str = M/D/Y H:iA, ex: 05/20/1995 7:30AM
	$date_obj = date_create_from_format("m/d/Y h:iA", clean_up($timestamp_str));
	return date_format($date_obj, "Y-m-d H:i:s");
}

function get_patient_id_create_if_new($first_name, $last_name, $birthday)
{
	global $db;
	$id = get_patient_id($first_name, $last_name, $birthday);
	if (empty($id)) {
			// ID was null, so the patient is new, so let's insert them
			// We are preferring prepared statements, especially for INSERTion

		$query = "INSERT INTO Patients";
		$query .= "(first_name, last_name, date_of_birth, created, last_modified)";
		$query .= "VALUES (?, ?, ?, now(), now())";
		$birthday = sql_friendly_date($birthday);
		if (!($stmt = $db->prepare($query)))
			echo "Prepare failed: (" . $db->errno . ") " . $db->error;

		if (!($stmt->bind_param("sss", $first_name, $last_name, $birthday)))
			echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;

		if ($stmt->execute()) {
				//header("Location: ../../"); This will direct to the next form to be added
		}
		else
			echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;

			$id = $stmt->insert_id; // The new patient_id of what we INSERTed
		}
		return $id;
	}

	function get_user_id($username) {
		global $db;
		$username = clean_up($username);
		$result = $db->query("SELECT user_id FROM Authentication WHERE username LIKE '$username'");
		$row = $result->fetch_assoc();
		return $row["user_id"];
	}

	function get_user_name($id){
		global $db;
		return $results = $db->query("SELECT name FROM Authentication WHERE user_id =  '$id'");
	}

	function login($username, $password) {
		global $db;
		$user_key = get_user_id($username);
		
		$sql = "SELECT password FROM Authentication WHERE username='" . $username . "'";
		$result = $db->query($sql);
		if($result)
		{
			$row = $result->fetch_assoc();
		
			if(!password_verify($password, $row["password"]))
				return false;
			else
				return $user_key;
		}
	}

	function clean_up($data) {
		global $db;
		return $db->real_escape_string($data);
	}

	/*
	* Returns the name of the user_role corresponding to the given user_id
	*/
	function get_user_role($id){
		global $db;
		$result = $db->query("SELECT user_role FROM Authentication WHERE user_id =  '$id'") or die (mysqli_error($db));
		$row = $result->fetch_assoc();
		return $row["user_role"];
	}

	function make_swap_code($show_this_one, $tabs)
	{
		foreach ($tabs as $key => $value) {
			if ($key !== $show_this_one) {
				echo "$('$key').hide();";
			}
		}
		echo "$('$show_this_one').show();";
		//echo "history.pushState({}, '$tabs[$show_this_one]', '".$_SERVER['PHP_SELF'].'?tab='.$show_this_one."');";
		
	}
	function make_show_all_tabs_code($tabs)
	{
		foreach ($tabs as $key => $value) {
			echo "$('$key').show();";
		}
	}
	// html
	function make_div_line_code($value, $show)
	{	
		echo "id='$value' ".(($show)?(""):("style='display:none;'"));
	}

	/*
	* This function is used to insert a Log into the Log table. This can be called whenever
	* a user performs an action that should be logged. Just give it the user id and action as arguments.
	* No return value
	*/
	function create_log($id, $action)
	{
		global $db;
		//create sql query
		$query = "INSERT INTO Log";
		$query .= "(log_user_id, action, created, last_modified)";
		$query .= "VALUES (?, ?, now(), now())";

		//prepare
		if (!($stmt = $db->prepare($query)))
		{
			echo "Prepare failed: (" . $db->errno . ") " . $db->error;
		}
		//bind
		if (!($stmt->bind_param("is", $id, $action)))
		{
			echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
		}
		//sanitize the binded variables
		$id = clean($id);
		$action = clean($action);

		//execute
		if ($stmt->execute()) {
			//record successfully added
		} 
		else {
			echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
		}

	}

/**
 * Returns the form status, stored in one of the constants at the top of this file
 * Example Usage: get_form_status(1, 1) => STATUS_EMPTY
 * @param $patient_id int The patient ID to look for
 * @param $slot_id int The scheduled slot aka visit date ID (OfficeVisitForm.visit_date_id)
 * @return string The status of this form: one of: (STATUS_EMPTY, STATUS_AWAITING_APPROVAL, STATUS_LOCKED)
 */
function get_form_status($patient_id, $slot_id)
{
	global $db;
	$patient_id = clean_up($patient_id);
	$slot_id = clean_up($slot_id);
	$result = $db->query("SELECT status FROM OfficeVisitForm WHERE patient_id = '$patient_id' AND visit_date_id = '$slot_id'");
	$row = $result->fetch_assoc();
	if ($result->num_rows === 0)
		return STATUS_EMPTY; // doesn't exist inside the table, so this is empty.
	else
		return $row['status']; // return the status, either waiting for doctor approval or locked.

}

	?>
