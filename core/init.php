<?php
	date_default_timezone_set("America/New_York"); // Set default
	// Constants for Office Visit Form, values are stored in table OfficeVisitForm.status
	define ("STATUS_EMPTY", "EMPTY"); // Waiting for Med Student to fill this form out
	define ("STATUS_AWAITING_APPROVAL", "AWAITING DOCTOR APPROVAL"); //After Med Student fills it out
	define ("STATUS_LOCKED", "LOCKED"); // After Doctor approves the form. Lock this down.
	session_start();

	require 'database/connect.php';
	require 'functions/users.php';
	require 'functions/admin.php';

?>