<?php
	require_once 'core/init.php';
	if(!logged_in())
		header("Location: index.php");
	include 'templates/header.php';
	include 'templates/navbar.php';
	include 'templates/search-content.php';
	include 'templates/footer.php';
?>