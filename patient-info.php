<?php
	include "core/init.php";
	if(!logged_in())
		header("Location: index.php");
	//Log the action. First argument is the user's id, second is a string describing the action taken.
   	create_log($_SESSION['id'], "Searched a patient");
	if (!isset($_GET['tab'])) {
	    $tab = 'demographics';
	  } else {
	    $tab = clean_up($_GET['tab']);
	  }
	include "templates/header.php";
	include "templates/navbar.php";
	include "templates/patient-info-content.php";
	include "templates/footer.php";
?>