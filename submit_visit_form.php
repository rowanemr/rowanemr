<?php
// Called after the Office Visit Form is submitted.
// If you need an example backend script, please see `submit_pain_form.php`

include 'core/init.php';
/* For Debugging */
error_reporting(E_ALL);
ini_set('display_errors', 1);
/* patient_id will be propagated through a GET variable */

$form_status = STATUS_EMPTY; //assume EMPTY until we determine otherwise
if (!isset($_GET['patient_id']) or !isset($_GET['slot_id']) or !isset($_GET['addnew']))
    die("Patient ID and Slot ID and Addnew required");
$patient_id = clean_up($_GET['patient_id']);
$slot_id = clean_up($_GET['slot_id']);
$addnew = clean_up($_GET['addnew']); // whether this is new (SQL INSERT) or adding (SQL UPDATE)
$form_status = get_form_status($patient_id, $slot_id);
$user_role = get_user_role($_SESSION['id']); //role of user (doctor, med student, admin); not the patient
if ($form_status == STATUS_LOCKED and $user_role != 'ADMIN') // Form is locked, sorry, you can't edit it!
    die("Sorry, this form is locked! <a href = 'patient-info.php?patient_id=$patient_id'>Click here for a read-only copy of this patient's data</a>");
/*----------------------------------------------------------------------------------------------------------------------
 * Step 1: Collect our POST variables into PHP variables for readability.
 * We will perform data validation with helper functions.
 * Pay careful attention to variables which have bounds
 * As well as date parsing. SQL date convention is: YYYY-MM-DD
 * As well as Yes/No or True/False responses. SQL convention dictates we
 * store such information as CHAR(1): 'Y' or 'N'.
 * There is a lot of information here, but we are simply just collecting the information from POST variables
 * and performing any cleaning or validation
 *----------------------------------------------------------------------------------------------------------------------
*/
$chief_complaint = clean($_POST['chief_complaint']);
$location = clean($_POST['location']);
$quality = clean($_POST['quality']);
$severity = clean($_POST['severity']);
$duration = clean($_POST['duration']);
$timing = clean($_POST['timing']);
$radiation = clean($_POST['radiation']);
$context = clean($_POST['context']);
$modifying_factors = clean($_POST['modifying_factors']);
$signs_symptoms = clean($_POST['signs_symptoms']);
$medical_history = clean($_POST['medical_history']);
$surgical_history = clean($_POST['surgical_history']);
$medications = clean($_POST['medications']);
$allergies = clean($_POST['allergies']);
$social_history = clean($_POST['social_history']);
$review_of_symptoms = clean($_POST['review_of_symptoms']);
$heart_rate = clean($_POST['heart_rate']);
$resp_rate = clean($_POST['resp_rate']);
$systolic_bp = clean($_POST['systolic_bp']);
$diastolic_bp = clean($_POST['diastolic_bp']);
$height = intval(doubleval($_POST['height_feet']) * 12 + doubleval($_POST['height_inches'])); //convert to inches
$weight = clean($_POST['weight']);

/* General Exam */
/* An array coming from the form, HTTP input attribute name=cardiac_exam */
/* Check if it's set as POST var, if so, use it. This prevents errors if all parts of an exam portion were abnormal*/
/* If all parts of an exam region were abnormal, the post variable index would be undefined (eg: nothing checked) */
$gen_exam_array = isset($_POST['gen_exam']) ? $_POST['gen_exam'] : array();
/* The values in this $gen_exam_array array come from the HTML value `attribute` on the checkboxes */
$gen_awake_alert = in_array("awake", $gen_exam_array) ? "Y" : "N"; /* A checkbox, so store Y or N in database */
$gen_no_acute_distress = in_array("noacutedistress", $gen_exam_array) ? "Y" : "N";
$gen_abnormalities = clean($_POST['gen_abnormalities']);

/* HEENT Exam */
$heent_exam_array = isset($_POST['heent_exam']) ? $_POST['heent_exam'] : array();
$heent_ears_clear_intact = in_array("tmclear", $heent_exam_array) ? "Y" : "N";
$heent_ears_canals_clear = in_array("canalsclear", $heent_exam_array) ? "Y" : "N";
$heent_neck_supple_nontender = in_array("supple", $heent_exam_array) ? "Y" : "N";
$heent_neck_no_thyromegaly = in_array("nothyromegaly", $heent_exam_array) ? "Y" : "N";
$heent_mouth_mucous_membranes_moist = in_array("mucous", $heent_exam_array) ? "Y" : "N";
$heent_mouth_no_erythema = in_array("noerythema", $heent_exam_array) ? "Y" : "N";
$heent_abnormalities = clean($_POST['heent_abnormalities']);

/* Cardiac Exam */
$cardiac_exam_array = isset($_POST['cardiac_exam']) ? $_POST['cardiac_exam'] : array();
$cardiac_regular_rate = in_array("regularrate", $cardiac_exam_array) ? "Y" : "N";
$cardiac_murmurs = in_array("nomurmurs", $cardiac_exam_array) ? "Y" : "N";
$cardiac_abnormalities = clean($_POST['cardiac_abnormalities']);

/* Lung Exam */
$lung_exam_array = isset($_POST['lung_exam']) ? $_POST['lung_exam'] : array();
$lung_clear = in_array("clearauscultation", $lung_exam_array) ? "Y" : "N";
$lung_abnormalities = clean($_POST['lung_abnormalities']);

/* GI Exam */
$gi_exam_array = isset($_POST['gi_exam']) ? $_POST['gi_exam'] : array();
$gi_abdomen_soft = in_array("abdomensoft", $gi_exam_array) ? "Y" : "N";
$gi_no_masses = in_array("nomasses", $gi_exam_array) ? "Y" : "N";
$gi_abnormalities = clean($_POST['gi_abnormalities']);

/* EXT Exam */
$ext_exam_array = isset($_POST['ext_exam']) ? $_POST['ext_exam'] : array();
$ext_no_edema = in_array("noedema", $ext_exam_array) ? "Y" : "N";
$ext_abnormalities = clean($_POST['ext_abnormalities']);

/* MSK Exam */
$msk_exam_array = isset($_POST['msk_exam']) ? $_POST['msk_exam'] : array();;
$msk_full_range_motion = in_array("fullmotion", $msk_exam_array) ? "Y" : "N";
$msk_no_joint_deformity = in_array("nojoint", $msk_exam_array) ? "Y" : "N";
$msk_no_muscle_hypertonicity = in_array("nomuscle", $msk_exam_array) ? "Y" : "N";
$msk_abnormalities = clean($_POST['msk_abnormalities']);

/* Neuro Exam */
$neuro_exam_array = isset($_POST['neuro_exam']) ? $_POST['neuro_exam'] : array();
$neuro_cranial_nerves_intact = in_array("cranial", $neuro_exam_array) ? "Y" : "N";
$neuro_muscle_strength = in_array("musclestrength", $neuro_exam_array) ? "Y" : "N";
$neuro_sensation_intact = in_array("sensational", $neuro_exam_array) ? "Y" : "N";
$neuro_deep_tendon_reflexes = in_array("deeptendon", $neuro_exam_array) ? "Y" : "N";
$neuro_abnormalities = clean($_POST['neuro_abnormalities']);

/* The diagnosis/somatic dysfunction */
/* Head Region */
$head = isset($_POST['head']) ? clean($_POST['head']) : ""; /* diagnosis/somatic dysfunction */
$head_omm = isset($_POST['headomm']) ? clean($_POST['headomm']) : "";
/* An ARRAY of treatment methods used */
/* Can't call clean() on an array */
$head_treatments = isset($_POST['headtreatmentmethod']) ? ($_POST['headtreatmentmethod']) : "";
$head_result = isset($_POST['headresult']) ? clean($_POST['headresult']) : "";
//TODO: If head set, check that head_omm, head_treatments, head_result are also set. if not, die. Front end should to this too!
/* Cervical Region */
$cervical = isset($_POST['cervical']) ? clean($_POST['cervical']) : ""; /* diagnosis/somatic dysfunction */
$cervical_omm = isset($_POST['cervicalomm']) ? clean($_POST['cervicalomm']) : "";
/* An ARRAY of treatment methods used */
$cervical_treatments = isset($_POST['cervicaltreatmentmethod']) ? ($_POST['cervicaltreatmentmethod']) : "";
$cervical_result = isset($_POST['cervicalresult']) ? clean($_POST['cervicalresult']) : "";

/* Thoracic Region */
$thoracic = isset($_POST['thoracic']) ? clean($_POST['thoracic']) : ""; /* diagnosis/somatic dysfunction */
$thoracic_omm = isset($_POST['thoracicomm']) ? clean($_POST['thoracicomm']) : "";
/* An ARRAY of treatment methods used */
$thoracic_treatments = isset($_POST['thoracictreatmentmethod']) ? ($_POST['thoracictreatmentmethod']) : "";
$thoracic_result = isset($_POST['thoracicresult']) ? clean($_POST['thoracicresult']) : "";

/* Lumbar Region */
$lumbar = isset($_POST['lumbar']) ? clean($_POST['lumbar']) : ""; /* diagnosis/somatic dysfunction */
$lumbar_omm = isset($_POST['lumbaromm']) ? clean($_POST['lumbaromm']) : "";
/* An ARRAY of treatment methods used */
$lumbar_treatments = isset($_POST['lumbartreatmentmethod']) ? ($_POST['lumbartreatmentmethod']) : "";
$lumbar_result = isset($_POST['lumbarresult']) ? clean($_POST['lumbarresult']) : "";

/* Sacrum Region */
$sacrum = isset($_POST['sacrum']) ? clean($_POST['sacrum']) : ""; /* diagnosis/somatic dysfunction */
$sacrum_omm = isset($_POST['sacrumomm']) ? clean($_POST['sacrumomm']) : "";
/* An ARRAY of treatment methods used */
$sacrum_treatments = isset($_POST['sacrumtreatmentmethod']) ? ($_POST['sacrumtreatmentmethod']) : "";
$sacrum_result = isset($_POST['sacrumresult']) ? clean($_POST['sacrumresult']) : "";

/* Pelvis / Innominates Region */
$pelvis = isset($_POST['pelvis']) ? clean($_POST['pelvis']) : ""; /* diagnosis/somatic dysfunction */
$pelvis_omm = isset($_POST['pelvisomm']) ? clean($_POST['pelvisomm']) : "";
/* An ARRAY of treatment methods used */
$pelvis_treatments = isset($_POST['pelvistreatmentmethod']) ? ($_POST['pelvistreatmentmethod']) : "";
$pelvis_result = isset($_POST['pelvisresult']) ? clean($_POST['pelvisresult']) : "";

/* Ribcage Region */
$ribcage = isset($_POST['ribcage']) ? clean($_POST['ribcage']) : ""; /* diagnosis/somatic dysfunction */
$ribcage_omm = isset($_POST['ribcageomm']) ? clean($_POST['ribcageomm']) : "";
/* An ARRAY of treatment methods used */
$ribcage_treatments = isset($_POST['ribcagetreatmentmethod']) ? ($_POST['ribcagetreatmentmethod']) : "";
$ribcage_result = isset($_POST['ribcageresult']) ? clean($_POST['ribcageresult']) : "";

/* Abdomen Region */
$abdomen = isset($_POST['abdomen']) ? clean($_POST['abdomen']) : ""; /* diagnosis/somatic dysfunction */
$abdomen_omm = isset($_POST['abdomenomm']) ? clean($_POST['abdomenomm']) : "";
/* An ARRAY of treatment methods used */
$abdomen_treatments = isset($_POST['abdomentreatmentmethod']) ? ($_POST['abdomentreatmentmethod']) : "";
$abdomen_result = isset($_POST['abdomenresult']) ? clean($_POST['abdomenresult']) : "";

/* Upper Extremity Region */
$upperextremity = isset($_POST['upperextremity']) ? clean($_POST['upperextremity']) : ""; /* diagnosis/somatic dysfunction */
$upperextremity_omm = isset($_POST['upperextremityomm']) ? clean($_POST['upperextremityomm']) : "";
/* An ARRAY of treatment methods used */
$upperextremity_treatments = isset($_POST['upperextremitytreatmentmethod']) ? ($_POST['upperextremitytreatmentmethod']) : "";
$upperextremity_result = isset($_POST['upperextremityresult']) ? clean($_POST['upperextremityresult']) : "";

/* lower Extremity Region */
$lowerextremity = isset($_POST['lowerextremity']) ? clean($_POST['lowerextremity']) : ""; /* diagnosis/somatic dysfunction */
$lowerextremity_omm = isset($_POST['lowerextremityomm']) ? clean($_POST['lowerextremityomm']) : "";
/* An ARRAY of treatment methods used */
$lowerextremity_treatments = isset($_POST['lowerextremitytreatmentmethod']) ? ($_POST['lowerextremitytreatmentmethod']) : "";
$lowerextremity_result = isset($_POST['lowerextremityresult']) ? clean($_POST['lowerextremityresult']) : "";

/* Additional Notes */
$notes = clean($_POST['notes']);
$assessment = clean($_POST['assessment']);
$plan = clean($_POST['plan']);

$med_student_signature = isset($_POST['student_signature']) ? clean($_POST['student_signature']) : "";
$physician_signature = isset($_POST['physician_signature']) ? clean($_POST['physician_signature']) : "";

/*----------------------------------------------------------------------------------------------------------------------
 * Step 2: Put our information into the database.
 * Everything goes into this table `OfficeVisitForm` except Treatments (since they are checkboxes)
 *----------------------------------------------------------------------------------------------------------------------
 */
global $db;
$new_status = STATUS_AWAITING_APPROVAL; // assume we are waiting for approval unless changed
$user_role = get_user_role($_SESSION['id']);
$signature = ""; // the med student or physician signature

if ($user_role == 'DOCTOR' or $user_role == 'ADMIN') {
    $new_status = STATUS_LOCKED; // if it's a doctor or admin, lock this form.
    $signature = $physician_signature;
}
else
    $signature = $med_student_signature; //if not a doctor, write the med student signature

if ($addnew == 1) {
    $query = "INSERT INTO OfficeVisitForm ";
    // Note that patient_id and visit_date_id inserted at end of this query
    // So we can use same bind_param for both of these
    $query .= "(created, last_updated, student_signature, status, chief_complaint, location, severity, quality, duration, timing, radiation, context, modifying_factors, associated_signs_symptoms, past_medical_history, past_surgical_history, medications, allergies, social_history, review_of_symptoms, heart_rate, resp_rate, systolic_bp, diastolic_bp, height, weight, gen_awake_alert, gen_no_acute_distress, gen_abnormal, heent_ears_clear_intact, heent_ears_canals_clear, heent_neck_supple_nontender, heent_neck_no_thyromegaly, heent_mouth_mucous_membranes_moist, heent_mouth_no_erythema, cardiac_regular_rate, cardiac_murmurs, lungs_clear, gi_abdomen_soft, gi_no_masses, ext_no_edema, msk_full_range_motion, msk_no_joint_deformity, msk_no_muscle_hypertonicity, neuro_cranial_nerves_intact, neuro_muscle_strength, neuro_sensation_intact, neuro_deep_tendon_reflexes, head_diagnosis, head_omm, head_result, cervical_diagnosis, cervical_omm, cervical_result, thoracic_diagnosis, thoracic_omm, thoracic_result, lumbar_diagnosis, lumbar_omm, lumbar_result, sacrum_diagnosis, sacrum_omm, sacrum_result, pelvis_diagnosis, pelvis_omm, pelvis_result, ribcage_diagnosis, ribcage_omm, ribcage_result, abdomen_diagnosis, abdomen_omm, abdomen_result, upper_diagnosis, upper_omm, upper_result, lower_diagnosis, lower_omm, lower_result, notes, assessment, plan, patient_id, visit_date_id) ";
    $query .= "VALUES (now(), now() " . str_repeat(",? ", 81) . ")";
} else {
    $query = "UPDATE OfficeVisitForm SET physician_signature = ?, status = ?, chief_complaint = ?, location = ?, severity = ?, quality = ?, duration = ?, timing = ?, radiation = ?, context = ?, modifying_factors = ?, associated_signs_symptoms = ?, past_medical_history = ?, past_surgical_history = ?, medications = ?, allergies = ?, social_history = ?, review_of_symptoms = ?, heart_rate = ?, resp_rate = ?, systolic_bp = ?, diastolic_bp = ?, height = ?, weight = ?, gen_awake_alert = ?, gen_no_acute_distress = ?, gen_abnormal = ?, heent_ears_clear_intact = ?, heent_ears_canals_clear = ?, heent_neck_supple_nontender = ?, heent_neck_no_thyromegaly = ?, heent_mouth_mucous_membranes_moist = ?, heent_mouth_no_erythema = ?, cardiac_regular_rate = ?, cardiac_murmurs = ?, lungs_clear = ?, gi_abdomen_soft = ?, gi_no_masses = ?, ext_no_edema = ?, msk_full_range_motion = ?, msk_no_joint_deformity = ?, msk_no_muscle_hypertonicity = ?, neuro_cranial_nerves_intact = ?, neuro_muscle_strength = ?, neuro_sensation_intact = ?, neuro_deep_tendon_reflexes = ?,  head_diagnosis = ?, head_omm = ?, head_result = ?, cervical_diagnosis = ?, cervical_omm = ?, cervical_result = ?, thoracic_diagnosis = ?, thoracic_omm = ?, thoracic_result = ?, lumbar_diagnosis = ?, lumbar_omm = ?, lumbar_result = ?, sacrum_diagnosis = ?, sacrum_omm = ?, sacrum_result = ?, pelvis_diagnosis = ?, pelvis_omm = ?, pelvis_result = ?, ribcage_diagnosis = ?, ribcage_omm = ?, ribcage_result = ?, abdomen_diagnosis = ?, abdomen_omm = ?, abdomen_result = ?, upper_diagnosis = ?, upper_omm = ?, upper_result = ?, lower_diagnosis = ?, lower_omm = ?, lower_result = ?, notes = ?, assessment = ?, plan = ?, last_updated = now() ";
    $query .= "WHERE patient_id = ? AND visit_date_id = ?"; // don't use ? here to use same parameter binding
}
if (!($stmt = $db->prepare($query))) {
    echo "Prepare failed: (" . $db->errno . ") " . $db->error;
}
//bind
if (!($stmt->bind_param(str_repeat("s", 81), $signature, $new_status, $chief_complaint, $location, $severity, $quality, $duration, $timing, $radiation, $context, $modifying_factors, $signs_symptoms, $medical_history, $surgical_history, $medications, $allergies, $social_history, $review_of_symptoms, $heart_rate, $resp_rate, $systolic_bp, $diastolic_bp, $height, $weight, $gen_awake_alert, $gen_no_acute_distress, $gen_abnormalities, $heent_ears_clear_intact, $heent_ears_canals_clear, $heent_neck_supple_nontender, $heent_neck_no_thyromegaly, $heent_mouth_mucous_membranes_moist, $heent_mouth_no_erythema, $cardiac_regular_rate, $cardiac_murmurs, $lung_clear, $gi_abdomen_soft, $gi_no_masses, $ext_no_edema, $msk_full_range_motion, $msk_no_joint_deformity, $msk_no_muscle_hypertonicity, $neuro_cranial_nerves_intact, $neuro_muscle_strength, $neuro_sensation_intact, $neuro_deep_tendon_reflexes, $head, $head_omm, $head_result, $cervical, $cervical_omm, $cervical_result, $thoracic, $thoracic_omm, $thoracic_result, $lumbar, $lumbar_omm, $lumbar_result, $sacrum, $sacrum_omm, $sacrum_result, $pelvis, $pelvis_omm, $pelvis_result, $ribcage, $ribcage_omm, $ribcage_result, $abdomen, $abdomen_omm, $abdomen_result, $upperextremity, $upperextremity_omm, $upperextremity_result, $lowerextremity, $lowerextremity_omm, $lowerextremity_result, $notes, $assessment, $plan, $patient_id, $slot_id))) {
    echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
}
//execute
if ($stmt->execute()) {
    //header("Location: ../../"); This will direct to the next form to be added
} else {
    echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
}

$office_visit_form_id_result = $db->query("SELECT office_visit_form_id FROM OfficeVisitForm WHERE patient_id = '$patient_id' AND visit_date_id = '$slot_id'"); /* Will be used as a foreign key to JOIN against */
$office_visit_form_id = $office_visit_form_id_result->fetch_assoc()['office_visit_form_id'];

/*----------------------------------------------------------------------------------------------------------------------
 * Step 3: Put our treatments into the table OfficeVisitFormTreatments
 *----------------------------------------------------------------------------------------------------------------------
 */

/* Store an array of arrays (all our treatments) in $all_treatments to do INSERTion.
 * We will first delete all values we have, in case this was updated (much easier than Upsert and Delete)
 * Ex: $head_treatments = (ME, MFR, HVLA), $cervical_treatments = (HVLA), etc.
 * This allows us to programatically insert our different treatments (ME, MFR, etc) for different regions (Head, etc).
 * The indexes in $all_treatments corresponds to indexes in $region_names
 * This is so we can associate an array ($head_treatments) with a region name ("Head")
 */
global $db;
// Delete so we can re-add anything that was checked. There might be large changes, so easier to do this.
$db->query("DELETE FROM OfficeVisitFormTreatments WHERE office_visit_id = '$office_visit_form_id'");
$all_treatments = array($head_treatments, $cervical_treatments, $thoracic_treatments, $lumbar_treatments, $sacrum_treatments, $pelvis_treatments, $ribcage_treatments, $abdomen_treatments, $upperextremity_treatments, $lowerextremity_treatments);
$region_names = array("Head", "Cervical", "Thoracic", "Lumbar", "Sacrum", "Pelvis/Innominates", "Ribcage", "Abdomen", "Upper Extremity", "Lower Extremity");
for ($i = 0; $i < count($all_treatments); $i++) {
    $current_treatment_arr = &$all_treatments[$i];
    $current_region = $region_names[$i];
    if (!is_array($current_treatment_arr))
        continue; //Not an array means they didn't fill this portion out. That's okay. Go to next region.
    foreach ($current_treatment_arr as &$treatment_method) {
        // We have an array like $head_treatments as $current_treatment_arr that contains treatments like (ME, MFR, ...)
        // Insert the region name like Head and the treatment type like ME as separate rows in OfficeVisitFormTreatments
        insert_treatment($current_region, $treatment_method, $office_visit_form_id);
    }
}
/*----------------------------------------------------------------------------------------------------------------------
 * Step 4: We are done, let's tell the student doctor to wait for physician sign off
 *----------------------------------------------------------------------------------------------------------------------
 */
if ($form_status == STATUS_EMPTY)
    echo 'Data filled out. All logged in Doctors will be notified.';
elseif (get_user_role($_SESSION['id']) == 'DOCTOR' or get_user_role($_SESSION['id']) == 'ADMIN')
    echo 'Thank you for approving this. This form is now locked (read-only).';
else
    echo 'Data filled out.';
echo '<br><a href="index.php">Go back to EMR System</a>';

/**
 * This will insert a specific treatment into OfficeVisitFormTreatments table
 * This is necessary because the treatments are stored as checkboxes
 * I did not want to add 10 regions * 9 treatments = 90 columns that might be null most of the time to the main
 * office visit table. MySQL does not have arrays (Postgres > MySQL)
 * The proper way to build a normalized DB here is to store each of those checkboxes as a new row
 * Example query ran by this function:
 *    INSERT INTO OfficeVisitFormTreatments (office_visit_id, region, treatment_method)
 *    VALUES (1, 'Head', 'ME')
 * If there were multiply treatments for head region, we'd have a similar row with MFR, HVLA, etc as treatments.
 * @param string $region The region to put insert: Head, Cervical, Thoracic, Lumbar, Sacrum, Pelvis/Innominates, Ribcage, etc.
 * @param string $treatment_method The treatment method to insert: ME, MFR, HVLA, ART, ST, FPR, CS, BLT, OCMM
 * @param string $office_visit_form_id The ID from the OfficeVisitForm table. This is used to JOIN the tables together.
 */
function insert_treatment($region, $treatment_method, $office_visit_form_id)
{
    global $db;
    $region = clean($region);
    $treatment_method = clean($treatment_method);
    $query = "INSERT INTO OfficeVisitFormTreatments (office_visit_id, region, treatment_method) ";
    $query .= "VALUES (?, ?, ?) ";
    if (!($stmt = $db->prepare($query))) {
        echo "Prepare failed: (" . $db->errno . ") " . $db->error;
    }
//bind
    if (!($stmt->bind_param("sss", $office_visit_form_id, $region, $treatment_method))) {
        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    }

//execute
    if ($stmt->execute()) {
        //header("Location: ../../"); This will direct to the next form to be added
    } else {
        echo "Treatment Execute failed: $query  (" . $stmt->errno . ") " . $stmt->error;
    }
}


?>
