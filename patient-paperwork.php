<?php
require_once 'core/init.php';
// Log the user out (logout) !
session_destroy();
// Check if token is invalid and if so re-direct
// We must do so in this file, because by including header.php first, we cannot do any header("Location: ")
// because we previously sent out information
//Note: PHP Storm's internal web server won't care about this but Apache does!
$token = isset($_GET['token']) ? $_GET['token'] : '';
if ($token != '' and is_valid_token($token)) {
    // It's a valid token, so perform the re-direct.
    header("Location: patient-demographic.php?token=$token");
}

include 'templates/header.php';
include 'templates/patient-paperwork-content.php';
include 'templates/footer.php';

?>