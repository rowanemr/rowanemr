//This function is used to add new text fields when an add button is pressed
$(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $(this).parents('.controls'),
            currentEntry = $(this).parents('.entry:first');
            

        if(currentEntry.find('input:first').val() != ""){ //only allows adding a new text field if the current one is not empty
                                                          //to prevent a user from creating a bunch of empty fields 
            newEntry = $(currentEntry.clone());
            newEntry.appendTo(controlForm);

            newEntry.find('input').val(''); //sets the input to empty string for the new entry
            controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
        }
    }).on('click', '.btn-remove', function(e)
    {
		$(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});
