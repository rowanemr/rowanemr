/**
 * Loads the responses from the survey into a comprehensive
 * piece of data that can be easily analyzed.
 *
 * @author Richard Cerone
 * @team Rowan EMR
 * @date 01/04/2016
 */

window.onload = function()
{
    var questions = JSON.parse(sessionStorage.questions);
    var surveyResponses = JSON.parse(sessionStorage.surveyResponses);
    var canvas = document.getElementById("results").getContext("2d");
    
    //Setup data:
    var data = {
        labels: [], // currently empty; will contain all the labels for the data points.
        datasets: [
          {
            label: "Yes",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [] // currently empty; will contain all the data points.
          },
          {
            label: "No",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: [] // currently empty; will contain all the data points.
          }
        ]
    };
    //Setup options:
    var options = {
        tooltipTemplate: "<%= value %>%"
    };
    
    var chart = new Chart(canvas).Bar(data, options); //Creates chart.
    //Generate legend.
    document.getElementById("legend").innerHTML = "Legend: " + chart.generateLegend();
    
    //We have to dynamically add data to the chart.
    var yes = 0; //How many people said yes for each question.
    var no = 0; //How many people said no for each question.
    var twoAnswers = [] //Collect the questions that are yes/no.
    for (var i = 0; i < questions.length; i++)
    {
        var question = questions[i];
        for(var j = 0; j < surveyResponses.length; j++)
        {
            //Check if person said yes for the two answer question.
            if (surveyResponses[j]["question"] === question && surveyResponses[j]["answer"] === "yes")
            {
                yes++;
            }
            //Check if person said no for the two answer question.
            else if (surveyResponses[j]["question"] === question && surveyResponses[j]["answer"] === "no")
            {
                no++
            }
            if (j === surveyResponses.length-1)
            {
                if (yes > 0 || no > 0) //Check it's not a comment question.
                {
                    //Add data and reset yes and no values for next question.
                    var label = "Question " + (i+1);
                    twoAnswers.push(question);
                    chart.addData([yes, no], label); 
                    yes = 0;
                    no = 0;   
                }
            }
        }
    }
    
    if (twoAnswers.length !== questions.length)
    {
        //Now we need to display the actual questions correlated to the bar graph.
        var div = document.getElementById("table");
        div.innerHTML = "<h4 align='left'>Yes/No Question Key:</h4><br>"
        div.innerHTML += "<table id='questionKey' class='table table-striped'>" +
                                "<tr>" +
                                    "<td>Question #</td>" +
                                    "<td>Question</td>" +
                                "</tr>" +
                          "</table><br><br>";
        
        var table = document.getElementById("questionKey");
        for(var i = 0; i < twoAnswers.length; i++)
        {
            var row = table.insertRow(i+1);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Question " + (i+1);
            cell2.innerHTML = twoAnswers[i];
        }
        
        //Now display the comment questions.
        div.innerHTML += "<h4 align='left'>Comment Questions:</h4><br>";
        var titleCreated = false; //Will prevent multiple headers from being created.
        
        for(var i = 0; i < questions.length; i++)
        {
            //Create table.
            table = document.createElement("table");
            table.className = "table table-striped";
            
            var question = questions[i];
            for(var j = 0; j < surveyResponses.length; j++)
            {
                //Check if question is a comment.
                if (surveyResponses[j]["question"] === question && surveyResponses[j]["answer"] !== "yes" &&
                    surveyResponses[j]["question"] === question && surveyResponses[j]["answer"] !== "no")
                {
                    if (titleCreated !== true)
                    {
                        titleCreated = true; //Prevents multiple headers from being created.
                        div.innerHTML += "<h4>" + question + "</h4><br>";   
                    }
                    var row = document.createElement("tr");
                    var tableData = document.createElement("td");
                    var comment = document.createTextNode(surveyResponses[j]["answer"]);
                    tableData.appendChild(comment);
                    row.appendChild(tableData);
                    table.appendChild(row);
                }
                
                //Prepare for new table to be added.
                if (j === surveyResponses.length-1 && titleCreated === true)
                {
                    titleCreated = false;
                    div.appendChild(table);
                    div.innerHTML += "<br><br>";
                }
            }
        }   
    }
    else
    {
        var div = document.getElementById("table");
        div.innerHTML = "<h4 align='center'>No comment questions were in this survey.</h4>";
    }
};
