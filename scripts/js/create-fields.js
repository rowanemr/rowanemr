/**
 * Creates a new field and asks what kind of question it is. It can be
 * yes/no or a comment field.
 *
 * @author Richard Cerone
 * @team Rowan EMR
 * @date 12/23/2015
 */

/**
 * Creates the number of fields based on the drop down option selected.
 */
function createFields()
{
    var content = document.getElementById("content");
    content.innerHTML = ""; //Clear div incase of value change.
    
    var dropDown = document.getElementById("numOfFields").value;
    dropDown = parseInt(dropDown); //Get int value of drop down.    
    
    for(var i = 0; i < dropDown; i++ )
    {
        content.innerHTML += "<input type='text' class='no-clear typeahead formcontrol' placeholder='Type the question you want to ask...' id='question" + i +
                             "'></input><br>" +
                             "Type Of Question: <input type='radio' value='Yes/No' id='twoAnswer" + i + "' onclick='checkTwoAnswer(" + i +")'>Yes/No</input> <input type='radio' value='Comment' id='comment" + i + "' onclick='checkComment(" + i + ")'>Comment</input><br><br>";
    }
}

/**
 * Makes sure that when Yes/No option is clicked the comment option becomes
 * unclicked.
 *
 * @param {Integer} num the value of the current radio button
 */
function checkTwoAnswer(num)
{
   document.getElementById("comment" + num).checked = false;
}

/**
 * Makes sure that when comment option is clicked the Yes/No option becomes
 * unclicked.
 *
 * @param {Integer} num the value of the current radio button
 */
function checkComment(num)
{
    document.getElementById("twoAnswer" + num).checked = false;
}

//Create event listener:
var drop = document.getElementById("numOfFields");

if (drop.addEventListener)
{
   drop.addEventListener("click", createFields, false);
}
else
{
   drop.attachEvent("onclick", createFields);
}