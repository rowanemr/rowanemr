/**
 * Submits the survey to the database.
 *
 * @author Richard Cerone
 * @team Rowan EMR
 * @date 12/23/2015
 */

/**
 * Checks if the fields are filled out properly.
 */
function checkFields()
{
    var title = document.getElementById("title").value;
    if (title === "")
    {
        alert("Your survey needs a title!");
        return false;
    }
    else if (title.length > 32)
    {
        alert("Your title is too long!");
        return false;
    }
    else
    {
        var content = document.getElementById("content").childNodes; //Get all of the content div's children.
        for(var i = 0; i < content.length; i++)
        {
            if (content[i].value === "")
            {
                alert("You left question " + i + " blank.");
                return false;
            }
            else if (content[i].checked === false)
            {
                if (content[i+1].checked === true)
                {
                    alert("You left radio button " + i + " unchecked.");
                    return false;
                }
            }
        }
        
        return true;
    }
}

/**
 * Creates the actual survey and inserts into the database.
 */
function createSurvey()
{
    if (checkFields() === true)
    {
        var numFields = document.getElementById("numOfFields").value;
        numFields = parseInt(numFields); //Convert to int.
        
        var title = document.getElementById("title").value;
        
        var questions = []; //Array holding questions.
        for(var i = 0; i < numFields; i++)
        {
            var questionText = document.getElementById("question" + i).value;
            var type;
            if (document.getElementById("twoAnswer" + i).checked === true)
            {
                type = "yes/no";
            }
            else if (document.getElementById("comment" + i).checked === true)
            {
                type = "comment";
            }
            
            var q = new question(questionText, type);
            questions.push(q);
        }
        
    
        $.post("core/database/insert-survey.php", {title: title, questions: questions}, function(data)
         {
            console.log(data);
            data = JSON.parse(data); //Parse to JSON array.
            if (data === true)
            {
               alert("Survey has been created.");
               window.location = "index.php";
            }
            else if (data === "invalidType")
            {
                alert("The type entered was invalid. Contact support.");
            }
            else if (data === "noTitle")
            {
                alert("No title could be found. Contact support.");
            }
            else if (data === "insertFailed")
            {
                alert("The data failed to be inserted. Contact support.");
            }
            else if (data === "titleExists")
            {
                alert("The title for this survey already exists");
            }
         }) 
    }
}

/**
 * A JavaScript object that takes the question and type of question.
 * @param {String} question is a string of the question being asked.
 * @param {String} is yes/no if it is a yes or no question or comment
 * if the question is expecting a detailed answer.
 */
function question(question, type)
{
    this.question = question;
    this.type = type;
}

//Setup event listeners.
var button = document.getElementById("create");

if (button.addEventListener)
{
    button.addEventListener("click", createSurvey, false);
}
else
{
    button.attachEvent("onclick", createSurvey);    
}