/**
 * Loads the page with the survey content that needs to be edited.
 *
 * @author Richard Cerone
 * @team Rowan EMR
 * @date 12/29/2015
 */

var title = sessionStorage.title; //Get title name for html session.
var content = document.getElementById("content"); //Get div content for page.
var numOfQuestions = 0; //We will count the number of questions we have for editing as we generate them.

//Begin load content.
$.post("core/database/get-edit-survey.php", {title: title}, function(data)
{
    data = JSON.parse(data);
    if (data === "errorSurveyBank")
    {
        alert("There was an error accessing the Survey tables. Please contact support.");
    }
    else if (data === "errorSurveys")
    {
        alert("There was an error accessing the Survey tables. Please contact support.");
    }
    else
    {
        sessionStorage.surveyID = data[0]; //Store the id in html session.
        var content = document.getElementById("content");
        content.innerHTML = "<h3>Editing Survey: '" + title + "'</h3><br>";
        var innerContent = document.createElement("div"); //Create inner div to retrieve important elements.
        innerContent.id = "innerContent";
        content.appendChild(innerContent);
        
        for(var i = 1; i < data.length; i++)
        {
            innerContent.innerHTML += "<input type='text' class='no-clear typeahead formcontrol' id='question" + (i-1) + "' value='" + data[i]['question'] + "' placeholder='Type the question you want to ask...'></input><br>";
            if(data[i]["twoAnswers"] === "1" && data[i]["comment"] === "0")
            {
                innerContent.innerHTML += "Type of Question: <input type='radio' value='Yes/No' id='twoAnswer" + (i-1) + "' onclick='checkTwoAnswer(" + (i-1) +")' checked='checked'>Yes/No</input> <input type='radio' value='Comment' id='comment" + (i-1) + "' onclick='checkComment(" + (i-1) + ")'>Comment</input><br>";
            }
            else if(data[i]["twoAnswers"] === "0" && data[i]["comment"] === "1")
            {
                innerContent.innerHTML += "Type of Question: <input type='radio' value='Yes/No' id='twoAnswer" + (i-1) + "' onclick='checkTwoAnswer(" + (i-1) +")'>Yes/No</input> <input type='radio' value='Comment' id='comment" + (i-1) + "' onclick='checkComment(" + (i-1) + ")' checked='checked'>Comment</input><br>";
            }
            
            numOfQuestions++; //Increment question field count.
        }
        
        content.innerHTML += "<br><button id='save' class='btn btn-primary'>Save</button>";
        $.getScript("scripts/js/save-changes.js"); //Load rest of script when done generating HTML.
    }
});

/**
 * Makes sure that when Yes/No option is clicked the comment option becomes
 * unclicked.
 *
 * @param {Integer} num the value of the current radio button
 */
function checkTwoAnswer(num)
{
   document.getElementById("comment" + num).checked = false;
   
}

/**
 * Makes sure that when comment option is clicked the Yes/No option becomes
 * unclicked.
 *
 * @param {Integer} num the value of the current radio button
 */
function checkComment(num)
{
    document.getElementById("twoAnswer" + num).checked = false;
}