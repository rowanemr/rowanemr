$(document).ready(function(){
	var patients = new Bloodhound({
		datumTokenizer: function (d) { return Bloodhound.tokenizers.whitespace(d.full_name); },
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote:
		{
			url: './core/database/search.php?query=%QUERY',
            wildcard: '%QUERY',
            transform: function(response) { return response;  }
        },
	});

	patients.initialize();

	$('#prefetch .typeahead').typeahead(
        {
            minLength: 0,
            highlight: true,
            hint: true
        },{
            limit: 6,
            source: patients.ttAdapter(),
			displayKey: 'full_name',
				templates: {
					empty: ['<div>No matches</div>'],

					suggestion: function (data)
					{
						return '<p><a href = "patient-info.php?patient_id=' + data.patient_id + '"><strong>' + data.full_name + '</strong>:' + data.dob + '</a></p>';
					},
					footer: function(data)
					{
						return '<div>Searched for <strong>' + data.query + '</strong>';
					}
				}
	});
	console.log(patients);
});