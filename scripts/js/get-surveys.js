/**
 * Loads surveys for the admin to delete.
 *
 * @author Richard Cerone
 * @team Rowan EMR
 * @date 12/28/2015
 */

$.post("core/database/get-surveys.php", function(data)
{
    data = JSON.parse(data);
    var content = document.getElementById("content"); //Get div.
    
    if (data == true)
    {
        content.innerHTML = "<h3>There are currently no surveys. Try Creating one by clicking on the 'Create Survey' Tab.</h3>";
    }
    else //Create select box.
    {
        var title = data;
        content.innerHTML = ""; //Clear any html in the div.
        //Create empty select box.
        content.innerHTML += "<fieldset>" +
                                   "<div align='center'>" +
                                       "<select id='surveys' class='form-control' size='21' style='height: 100%; width:250px;'>" +
                                           //options go here...
                                       "</select>" +
                                   "</div>" +
                             "</fieldset><br>" +
                             "<input type='submit' id='edit' onclick='editSurvey()' class='btn btn-primary' value='Edit'></input> <input type='submit' id='results' onclick='viewResults()' class='btn btn-primary' value='View Results'></input> <input type='submit' id='delete' onclick='deleteSurvey()' class='btn btn-danger' value='Delete'></input>"   ;
        
        var surveys = document.getElementById("surveys"); //Select empty select box.
        for(var i = 0; i < title.length; i++) //Fill select box with all surveys in the database.
        {
            var option = document.createElement("option");
            option.text = title[i];
            option.id = title[i];
            surveys.add(option);
        }
    }
});

/**
 * Brings the admin to the edit page.
 */
function editSurvey()
{
    var surveys = document.getElementById("surveys");
    if (surveys.selectedIndex != -1)
    {
        var title = surveys.options[surveys.selectedIndex].text;
        sessionStorage.title = title; //Store title in html session to use on redirected page.
        window.location = "edit-survey.php";
    }
    else
    {
        alert("You need to select a survey first before editing.");
    }
}

/**
 * Deletes the survey for the admin.
 */
function deleteSurvey()
{
    var surveys = document.getElementById("surveys");
    if (surveys.selectedIndex != -1)
    {
        var title = surveys.options[surveys.selectedIndex].text;
        $.post("core/database/delete-survey.php", {title: title}, function(data)
        {
            data = JSON.parse(data);
            
            if (data === "success")
            {
                alert("Survey was deleted successfully.");
                window.location = "manage-surveys.php";
            }
            else if (data === "selectFailed")
            {
                alert("Could not select data. Please contact support.");
            }
            else if (data === "surveyBankFailed")
            {
                alert("Could not delete survey. Please contact support.");
            }
            else if (data === "surveysFailed")
            {
                alert("Could not delete survey specifics. Please contact support.");
            }
        });
    }
    else
    {
        alert("You need to select a survey first before deleting.");
    }
}

/**
 * Gets the user input for the selected survey. This will redirect
 * to a new page showing a data analysis of the responses for the
 * survey for each question.
 */
function viewResults()
{
    var surveys = document.getElementById("surveys");
    if (surveys.selectedIndex != -1)
    {
        var survey = surveys.options[surveys.selectedIndex].text;
        $.post("core/database/get-survey-results.php", {survey: survey}, function(data)
        {
            data = JSON.parse(data);
            
            if (data != true)
            {
                sessionStorage.questions = JSON.stringify(data[0]);
                sessionStorage.surveyResponses = JSON.stringify(data[1]); //Store results array in HTML session.
                window.location = "show-survey-results.php";
            }
            else if (data === true)
            {
                alert("No one has filled out this survey yet.");    
            }
        });
    }
    else
    {
        alert("You need to select a survey first before viewing results.");
    }
}
