/**
 * Loads surveys onto the patient dashboard in the patient portal.
 *
 *@author Richard Cerone
 *@team Rowan EMR
 *@date 07/07/2015
 */

var numOfQuestions = 0; //Use this to count how many questions were loaded.
var questions = []; //Store the questions for later use.

$.post("../core/database/get-patient-survey.php", function(data)
{
    data = JSON.parse(data);
    
    if (data === "empty")
    {
        var dash = document.getElementById("dash");
        dash.innerHTML = "<h3>Looks like there are no surveys for you to fill out right now.</h3>";
    }
    else if (data === "selectSurveyDetailsFailed")
    {
        alert("Could not get survey details. Please inform the staff.");
    }
    else
    {
        var title = data[0];
        var surveyID = data[1];
        //Store title and survey id into a HTML session for when we input user data.
        sessionStorage.title = title;
        sessionStorage.surveyID = surveyID;
        
        var dash = document.getElementById("dash");
        dash.innerHTML = "";
           
        for(var i = 0; i < data.length-2; i++) //-2 because we have title and id besides question objects in array.
        {
            var question = data[i+2]["question"];
            var twoAnswers = parseInt(data[i+2]["twoAnswers"]);
            var comment = parseInt(data[i+2]["comment"]);
            
            dash.innerHTML += (i+1) + "." + question + ":<br>";
            questions.push(question);
            
            if (twoAnswers === 1 && comment === 0)
            {
                dash.innerHTML += "<input type='radio' value='Yes' id='yes" + i + "' onclick='checkYes(" + i +")'>Yes</input> <input type='radio' value='No' id='no" + i + "' onclick='checkNo(" + i + ")'>No</input><br><br>";
                numOfQuestions++;
            }
            else if (comment === 1 && twoAnswers === 0)
            {
                dash.innerHTML += "<textarea id='comment" + i + "' rows='4' cols='50' maxlength='364'></textArea>"
                numOfQuestions++;
            }
        }
        dash.innerHTML += "<br><br><button id='submit' class='btn btn-primary'>Submit</button>";
        $.getScript("../scripts/js/get-user-data.js");
    }
});

/**
 * Unchecks no if yes is checked by user.
 */
function checkYes(num)
{
    document.getElementById("no" + num).checked = false;
}

/**
 * Unchecks yes if no is checked by user.
 */
function checkNo(num)
{
    document.getElementById("yes" + num).checked = false;
}



