/**
 * Gets the data entered in the survey by the user.
 *
 * @author Richard Cerone
 * @team Rowan EMR
 * @date 12/31/2015
 */

function checkFields()
{   
    //Note: numOfQuestions is a global variable count of questions that were loaded.
    for(var i = 0; i < numOfQuestions; i++)
    {
        var yes; //yes radio button.
        var no; //no radio button.
        var comment; //comment box.
        
        //Since question values vary we need to check if the element exists.
        if (document.getElementById("yes" + i) !== null && document.getElementById("no" + i) !== null)
        {
            yes = document.getElementById("yes" + i).checked;
            no = document.getElementById("no" + i).checked;
        }
        if (document.getElementById("comment" + i) !== null)
        {
            comment = document.getElementById("comment" + i).value;
        }
        
        //Once we assign an element we can check it's value.
        if (yes === false)
        {
            if (no === false)
            {
                alert("You didn't answer question " + (i+1) + ".");
                return false;
            }
        }
        else if (comment === "")
        {
            alert("You didn't answer question " + (i+1) + ".");
            return false;
        }
    }
    return true;
}

function collectData()
{
    if (checkFields() === true)
    {
        var input = [];
        var yes;
        var no;
        var comment;
        
        for (var i = 0; i < numOfQuestions; i++)
        {
            if (document.getElementById("yes" + i) !== null && document.getElementById("no" + i) !== null)
            {
                yes = document.getElementById("yes" + i).checked;
                no = document.getElementById("no" + i).checked;
            }
            if (document.getElementById("comment" + i) !== null)
            {
                comment = document.getElementById("comment" + i).value;
            }
            
            var questionNum = parseInt(i+1); //Question number.
            var answer = ""; //Answer to question.
            
            if (yes === true)
            {
                answer = "yes";
                input[i] = new UserInput(questionNum, answer);
                yes = false;
            }
            else if (no === true)
            {
                answer = "no";
                input[i] = new UserInput(questionNum, answer);
                no = false;
            }
            else if(comment !== "") //It's a comment.
            {
                answer = comment;
                input[i] = new UserInput(questionNum, answer);
                comment = "";
            }
        }
        
        var title = sessionStorage.title;
        var surveyID = sessionStorage.surveyID;
        $.post("../core/database/store-survey-answers.php", {input: input, title: title, surveyID: surveyID, questions: questions}, function(data)
        {
            data = JSON.parse(data);
            
            if (data === true)
            {
                alert("Thank you for particpating in this survey!");
                window.location = "patient-dash.php";
            }
            else
            {
                alert("We're sorry, we could not submit your survey at this time. Please alert the staff of this issue.");
            }
        });
    }
}

/**
 * This is a JavaScript object that stores user information.
 *
 * @param{Integer} question number that was answered.
 * @param{String} Answer to the question.
 */
function UserInput(questionNum, input)
{
    this.questionNum = questionNum;
    this.input = input;
}

//Setup event listener.
var button = document.getElementById("submit");

if (button.addEventListener)
{
    button.addEventListener("click", collectData, false);
}
else //IE
{
    button.attachEvent("onclick", collectData);
}