/**
 * Created by Darren on 1/5/16.
 */
$(document).ready(function(){
    var user = new Bloodhound({
        datumTokenizer: function (d) { return Bloodhound.tokenizers.whitespace(d.username); },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote:
        {
            url: './core/database/search-user.php?query=%QUERY',
            wildcard: '%QUERY',
            transform: function(response) { return response;  }
        },
    });

    user.initialize();

    $('#prefetch .typeahead').typeahead(
        {
            minLength: 0,
            highlight: true,
            hint: true
        },{
            limit: 6,
            source: user.ttAdapter(),
            displayKey: 'username',
            templates: {
                empty: ['<div>No matches</div>'],

                suggestion: function (data)
                {
                    return '<p><a>' + data.username + '</a></p>';
                },
                footer: function(data)
                {
                    return '<div>Searched for <strong>' + data.query + '</strong>';
                }
            }
        });
    console.log(user);
});