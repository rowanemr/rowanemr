/**
 * Saves any changes of current database for the backend of the database.
 *
 * @author Richard Cerone
 * @team Rowan EMR
 * @date 12/30/2015
 */

/**
 * Checks if the fields are filled out properly.
 */
function checkFields()
{
    for (var i = 0; i < numOfQuestions; i++)
    {
        var question = document.getElementById("question" + i);
        var twoAnswers = document.getElementById("twoAnswer" + i);
        var comment = document.getElementById("comment" + i);
        
        if (question.value === "")
        {
            alert("You left question " + (i + 1) + " blank.");
            return false;
        }
        else if (twoAnswers.checked === false && comment.checked === false)
        {
            alert("You left radio button " + (i + 1) + " unchecked.");
            return false;
        }
    }
    return true;
}

function saveChanges()
{
    if (checkFields() === true)
    {
        var title = sessionStorage.title;
        var surveyID = sessionStorage.surveyID;
        var questions = [];
        
        //Note: numOfQuestions is global from load-edit-survey.js
        for (var i = 0; i < numOfQuestions; i++)
        {
            var questionText = document.getElementById("question" + i).value;
            var type;
            if (document.getElementById("twoAnswer" + i).checked === true)
            {
                type = "yes/no";
            }
            else if (document.getElementById("comment" + i).checked === true)
            {
                type = "comment";
            }
            
            var q = new question(questionText, type);
            questions.push(q);
        }
        
         $.post("core/database/save-changes.php", {title: title, surveyID: surveyID, questions: questions}, function(data)
        {
            data = JSON.parse(data);
            console.log(data);
            
            if (data === "success")
            {
                alert("Survey was updated.");
                window.location = "manage-surveys.php";
            }
            else if (data === "selectFailed")
            {
                alert("Could not select data. Please contact support.");
            }
            else if (data === "questionFail")
            {
                alert("Could not update question. Please contact support.");
            }
            else if (data === "typeFailed")
            {
                alert("Could not update type. Please contact support.");
            }
        });
    }
}

/**
 * A JavaScript object that takes the question and type of question.
 * @param {String} question is a string of the question being asked.
 * @param {String} is yes/no if it is a yes or no question or comment
 * if the question is expecting a detailed answer.
 */
function question(question, type)
{
    this.question = question;
    this.type = type;
}

//Setup event listeners
var button = document.getElementById("save");
if (button.addEventListener)
{
    button.addEventListener("click", saveChanges, false);
}
else //IE
{
    button.attachEvent("onclick", saveChanges);
}