/**
 * Loads the urls onto the page via AJAX.
 *
 * @author Richard Cerone
 * @team Rowan EMR
 * @date 12/22/2015
 */

window.onload = function()
{
    $.post("../core/database/get-patient-content.php", function(data)
    {
        data = JSON.parse(data); //Parse to JSON array.
        var dash = document.getElementById("dash");
        
        if (data === true)
        {
           dash.innerHTML = "<h3 align='center'>Looks like you have no videos to watch :(</h3>";
        }
        
        else
        {
            var url = data;
            dash.innerHTML = "<h3>Videos For You</h3>"; //Clear previous data.
            
            //Create intital table setup.
            var table = document.createElement("table");
            table.className = " table table-striped";
            
            var initialRow = document.createElement("tr");
                
            var initialTableData1 = document.createElement("td");
            initialTableData1.appendChild(document.createTextNode("Video"));
            
            var initialTableData2 = document.createElement("td");
            initialTableData2.appendChild(document.createTextNode("Description"));
            
            initialRow.appendChild(initialTableData1);
            initialRow.appendChild(initialTableData2);
            
            table.appendChild(initialRow);
            
            //Begin creating inner table.
            for(var i = 0; i < url.length; i++)
            {   
                var innerRow = document.createElement("tr");
                
                var innerTableData1 = document.createElement("td");
                
                var link = document.createElement("a");
                link.setAttribute("href", url[i]["url"]);
                link.appendChild(document.createTextNode(url[i]["url"]));
                
                innerTableData1.appendChild(link);
                
                var innerTableData2 = document.createElement("td");
                innerTableData2.appendChild(document.createTextNode(url[i]["description"]));
                
                innerRow.appendChild(innerTableData1);
                innerRow.appendChild(innerTableData2);
                
                table.appendChild(innerRow);
            }
            
            dash.appendChild(table);
        }
    });
}