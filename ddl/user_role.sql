/* Setup the UserRole table with initial values.
 * A standard convention is to put such code in a `ddl` folder.
*/
INSERT INTO UserRole(role_name, created, last_modified) VALUES ('ADMIN', now(), now());

INSERT INTO UserRole(role_name, created, last_modified) VALUES ('DOCTOR', now(), now());

INSERT INTO UserRole(role_name, created, last_modified) VALUES ('MEDSTUDENT', now(), now());
