-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 13, 2016 at 09:35 AM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `RowanEMR`
--

-- --------------------------------------------------------

--
-- Table structure for table `Authentication`
--

CREATE TABLE IF NOT EXISTS `Authentication` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(256) NOT NULL,
  `name` text NOT NULL,
  `user_role` varchar(20) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  KEY `user_role` (`user_role`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `Authentication`
--

INSERT INTO `Authentication` (`user_id`, `username`, `password`, `name`, `user_role`, `created`, `last_modified`) VALUES
(24, 'rowanemr', '$2y$10$JyD6CM7lNlS/4BXO8mJove5rZRYvQsZ68toZa8ldqaYrVdoeIJjB2', 'Rowan EMR', 'ADMIN', '2016-01-07 15:47:30', '2016-01-07 15:11:39'),
(25, 'medstudent', '$2y$10$9ICNpGjMnShyW3.VXDPKKuvmbIT2/UlHzx05dO6DDVsLpvGQ6IeAq', 'Medical Student', 'MEDSTUDENT', '2016-01-07 15:12:41', '2016-01-07 15:12:41'),
(26, 'doctor', '$2y$10$XIs5rToftOFjT.YhfduDtuzXZjwfV32KXyG4nTpqXBxBa0AemYm9q', 'Doctor Demo', 'DOCTOR', '2016-01-07 15:13:00', '2016-01-07 15:13:00'),
(28, 'martin7c', '$2y$10$mJD1cNX/8WIEYFqKqlMOA.C4GMs0LlY2FKt8GvBMBp4Z4EMhIc7CK', 'Darren Martin', 'ADMIN', '2016-01-09 16:49:46', '2016-01-09 16:49:46');

-- --------------------------------------------------------

--
-- Table structure for table `Log`
--

CREATE TABLE IF NOT EXISTS `Log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log_user_id` int(11) NOT NULL,
  `action` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`log_id`),
  KEY `log_user_id` (`log_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=592 ;

--
-- Dumping data for table `Log`
--

INSERT INTO `Log` (`log_id`, `log_user_id`, `action`, `created`, `last_modified`) VALUES
(548, 24, 'Logged into system', '2016-01-07 15:43:20', '2016-01-07 15:43:20'),
(549, 24, 'Logged into system', '2016-01-07 15:43:39', '2016-01-07 15:43:39'),
(550, 25, 'Logged into system', '2016-01-07 15:46:27', '2016-01-07 15:46:27'),
(551, 25, 'Logged into system', '2016-01-07 15:46:39', '2016-01-07 15:46:39'),
(552, 24, 'Logged into system', '2016-01-07 15:47:38', '2016-01-07 15:47:38'),
(553, 24, 'Searched a patient', '2016-01-07 15:49:00', '2016-01-07 15:49:00'),
(554, 24, 'Searched a patient', '2016-01-07 15:52:21', '2016-01-07 15:52:21'),
(555, 24, 'Searched a patient', '2016-01-07 15:53:17', '2016-01-07 15:53:17'),
(556, 24, 'Searched a patient', '2016-01-07 15:53:26', '2016-01-07 15:53:26'),
(557, 24, 'Created patient appointment', '2016-01-07 15:53:54', '2016-01-07 15:53:54'),
(558, 24, 'Logged into system', '2016-01-07 15:54:49', '2016-01-07 15:54:49'),
(559, 24, 'Searched a patient', '2016-01-07 15:55:01', '2016-01-07 15:55:01'),
(560, 24, 'Logged into system', '2016-01-07 16:00:36', '2016-01-07 16:00:36'),
(561, 24, 'Logged into system', '2016-01-07 18:27:53', '2016-01-07 18:27:53'),
(562, 24, 'Logged into system', '2016-01-07 18:29:36', '2016-01-07 18:29:36'),
(563, 24, 'Logged into system', '2016-01-07 18:40:42', '2016-01-07 18:40:42'),
(564, 24, 'Logged into system', '2016-01-09 16:49:24', '2016-01-09 16:49:24'),
(566, 24, 'Created patient appointment', '2016-01-10 15:13:27', '2016-01-10 15:13:27'),
(567, 28, 'Logged into system', '2016-01-10 15:21:17', '2016-01-10 15:21:17'),
(568, 24, 'Logged into system', '2016-01-11 15:30:28', '2016-01-11 15:30:28'),
(569, 26, 'Logged into system', '2016-01-11 15:30:46', '2016-01-11 15:30:46'),
(570, 24, 'Logged into system', '2016-01-11 15:35:45', '2016-01-11 15:35:45'),
(571, 25, 'Logged into system', '2016-01-11 15:42:03', '2016-01-11 15:42:03'),
(572, 25, 'Searched a patient', '2016-01-11 15:47:53', '2016-01-11 15:47:53'),
(573, 24, 'Logged into system', '2016-01-11 15:48:19', '2016-01-11 15:48:19'),
(574, 25, 'Logged into system', '2016-01-11 15:49:28', '2016-01-11 15:49:28'),
(575, 24, 'Logged into system', '2016-01-11 15:49:36', '2016-01-11 15:49:36'),
(576, 24, 'Logged into system', '2016-01-11 16:27:02', '2016-01-11 16:27:02'),
(577, 24, 'Logged into system', '2016-01-11 16:34:45', '2016-01-11 16:34:45'),
(578, 25, 'Logged into system', '2016-01-11 16:37:26', '2016-01-11 16:37:26'),
(579, 24, 'Logged into system', '2016-01-11 16:51:54', '2016-01-11 16:51:54'),
(580, 24, 'Logged into system', '2016-01-11 16:57:02', '2016-01-11 16:57:02'),
(581, 25, 'Logged into system', '2016-01-12 14:36:16', '2016-01-12 14:36:16'),
(582, 24, 'Logged into system', '2016-01-12 14:36:34', '2016-01-12 14:36:34'),
(583, 24, 'Logged into system', '2016-01-12 14:37:16', '2016-01-12 14:37:16'),
(584, 24, 'Logged into system', '2016-01-12 15:33:21', '2016-01-12 15:33:21'),
(585, 25, 'Logged into system', '2016-01-12 15:35:22', '2016-01-12 15:35:22'),
(586, 24, 'Logged into system', '2016-01-12 15:44:00', '2016-01-12 15:44:00'),
(587, 24, 'Logged into system', '2016-01-12 15:44:55', '2016-01-12 15:44:55'),
(588, 24, 'Logged into system', '2016-01-12 15:56:59', '2016-01-12 15:56:59'),
(589, 24, 'Logged into system', '2016-01-12 16:00:31', '2016-01-12 16:00:31'),
(590, 24, 'Logged into system', '2016-01-12 16:00:48', '2016-01-12 16:00:48'),
(591, 24, 'Logged into system', '2016-01-13 14:28:59', '2016-01-13 14:28:59');

-- --------------------------------------------------------

--
-- Table structure for table `OfficeVisitForm`
--

CREATE TABLE IF NOT EXISTS `OfficeVisitForm` (
  `office_visit_form_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` text NOT NULL,
  `patient_id` int(11) NOT NULL,
  `visit_date_id` int(11) NOT NULL,
  `chief_complaint` text NOT NULL,
  `location` text NOT NULL,
  `quality` text NOT NULL,
  `severity` int(11) NOT NULL,
  `duration` text NOT NULL,
  `timing` text NOT NULL,
  `radiation` text NOT NULL,
  `context` text NOT NULL,
  `modifying_factors` text NOT NULL,
  `associated_signs_symptoms` text NOT NULL,
  `past_medical_history` text NOT NULL,
  `past_surgical_history` text NOT NULL,
  `medications` text NOT NULL,
  `allergies` text NOT NULL,
  `social_history` text NOT NULL,
  `review_of_symptoms` text NOT NULL,
  `heart_rate` int(11) NOT NULL,
  `resp_rate` int(11) NOT NULL,
  `systolic_bp` int(11) NOT NULL,
  `diastolic_bp` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `gen_awake_alert` char(1) NOT NULL,
  `gen_no_acute_distress` char(1) NOT NULL,
  `gen_abnormal` text,
  `heent_ears_clear_intact` char(1) NOT NULL,
  `heent_ears_canals_clear` char(1) NOT NULL,
  `heent_neck_supple_nontender` char(1) NOT NULL,
  `heent_neck_no_thyromegaly` char(1) NOT NULL,
  `heent_mouth_mucous_membranes_moist` char(1) NOT NULL,
  `heent_mouth_no_erythema` char(1) NOT NULL,
  `heent_abnormal` text,
  `cardiac_regular_rate` char(1) NOT NULL,
  `cardiac_murmurs` char(1) NOT NULL,
  `cardiac_abnormal` text,
  `lungs_clear` char(1) NOT NULL,
  `lungs_abnormal` text,
  `gi_abdomen_soft` char(1) NOT NULL,
  `gi_no_masses` char(1) NOT NULL,
  `gi_abnormal` text,
  `ext_no_edema` char(1) NOT NULL,
  `ext_abnormal` text,
  `msk_full_range_motion` char(1) NOT NULL,
  `msk_no_joint_deformity` char(1) NOT NULL,
  `msk_no_muscle_hypertonicity` char(1) NOT NULL,
  `msk_abnormal` text,
  `neuro_cranial_nerves_intact` char(1) NOT NULL,
  `neuro_muscle_strength` char(1) NOT NULL,
  `neuro_sensation_intact` char(1) NOT NULL,
  `neuro_deep_tendon_reflexes` char(1) NOT NULL,
  `neuro_abnormal` text,
  `head_diagnosis` text,
  `head_omm` char(1) DEFAULT NULL,
  `head_result` text,
  `cervical_diagnosis` text,
  `cervical_omm` char(1) DEFAULT NULL,
  `cervical_result` text,
  `thoracic_diagnosis` text,
  `thoracic_omm` char(1) DEFAULT NULL,
  `thoracic_result` text,
  `lumbar_diagnosis` text,
  `lumbar_omm` char(1) DEFAULT NULL,
  `lumbar_result` text,
  `sacrum_diagnosis` text,
  `sacrum_omm` char(1) DEFAULT NULL,
  `sacrum_result` text,
  `pelvis_diagnosis` text,
  `pelvis_omm` char(1) DEFAULT NULL,
  `pelvis_result` text,
  `ribcage_diagnosis` text,
  `ribcage_omm` char(1) DEFAULT NULL,
  `ribcage_result` text,
  `abdomen_diagnosis` text,
  `abdomen_omm` char(1) DEFAULT NULL,
  `abdomen_result` text,
  `upper_diagnosis` text,
  `upper_omm` char(1) DEFAULT NULL,
  `upper_result` text,
  `lower_diagnosis` text,
  `lower_omm` char(1) DEFAULT NULL,
  `lower_result` text,
  `notes` text,
  `assessment` text NOT NULL,
  `plan` text NOT NULL,
  `student_signature` text,
  `physician_signature` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`office_visit_form_id`),
  KEY `patient_id` (`patient_id`),
  KEY `visit_date_id` (`visit_date_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `OfficeVisitForm`
--

INSERT INTO `OfficeVisitForm` (`office_visit_form_id`, `status`, `patient_id`, `visit_date_id`, `chief_complaint`, `location`, `quality`, `severity`, `duration`, `timing`, `radiation`, `context`, `modifying_factors`, `associated_signs_symptoms`, `past_medical_history`, `past_surgical_history`, `medications`, `allergies`, `social_history`, `review_of_symptoms`, `heart_rate`, `resp_rate`, `systolic_bp`, `diastolic_bp`, `height`, `weight`, `gen_awake_alert`, `gen_no_acute_distress`, `gen_abnormal`, `heent_ears_clear_intact`, `heent_ears_canals_clear`, `heent_neck_supple_nontender`, `heent_neck_no_thyromegaly`, `heent_mouth_mucous_membranes_moist`, `heent_mouth_no_erythema`, `heent_abnormal`, `cardiac_regular_rate`, `cardiac_murmurs`, `cardiac_abnormal`, `lungs_clear`, `lungs_abnormal`, `gi_abdomen_soft`, `gi_no_masses`, `gi_abnormal`, `ext_no_edema`, `ext_abnormal`, `msk_full_range_motion`, `msk_no_joint_deformity`, `msk_no_muscle_hypertonicity`, `msk_abnormal`, `neuro_cranial_nerves_intact`, `neuro_muscle_strength`, `neuro_sensation_intact`, `neuro_deep_tendon_reflexes`, `neuro_abnormal`, `head_diagnosis`, `head_omm`, `head_result`, `cervical_diagnosis`, `cervical_omm`, `cervical_result`, `thoracic_diagnosis`, `thoracic_omm`, `thoracic_result`, `lumbar_diagnosis`, `lumbar_omm`, `lumbar_result`, `sacrum_diagnosis`, `sacrum_omm`, `sacrum_result`, `pelvis_diagnosis`, `pelvis_omm`, `pelvis_result`, `ribcage_diagnosis`, `ribcage_omm`, `ribcage_result`, `abdomen_diagnosis`, `abdomen_omm`, `abdomen_result`, `upper_diagnosis`, `upper_omm`, `upper_result`, `lower_diagnosis`, `lower_omm`, `lower_result`, `notes`, `assessment`, `plan`, `student_signature`, `physician_signature`, `created`, `last_updated`) VALUES
(1, 'LOCKED', 6, 6, 'chief complaint', 'l', 'l', 0, 'l', 'l', 'l', 'l', 'l', 'l', 'n', 'n', 'n', 'n', 'n', 'n', 90, 90, 90, 90, 41, 200, 'Y', 'Y', '', 'Y', 'Y', 'Y', 'N', 'Y', 'Y', NULL, 'Y', 'Y', NULL, 'Y', NULL, 'Y', 'Y', NULL, 'Y', NULL, 'Y', 'Y', 'Y', NULL, 'Y', 'Y', 'Y', 'Y', NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'notes', 'asessment', 'plan', 'MED STUDENT SIG', 'PHYSICIAN SIGNATURE', '2015-12-18 13:35:16', '2015-12-18 13:35:16'),
(2, 'LOCKED', 7, 7, 'Back hurts', 'Back', 'Bad', 10, '1 week', 'Often', 'No', 'Context', 'Walking', 'Pain', 'Medical history looks good', 'No surgeries', 'No medications', 'Yes, cats.', 'Drinks and Smokes', 'Basic back pain', 90, 15, 120, 80, 72, 200, 'Y', 'Y', '', 'Y', 'Y', 'Y', 'N', 'Y', 'Y', NULL, 'Y', 'Y', NULL, 'Y', NULL, 'Y', 'Y', NULL, 'Y', NULL, 'Y', 'Y', 'Y', NULL, 'Y', 'Y', 'Y', 'Y', NULL, 'Head hurts too', 'Y', 'Resolved', 'Cervical area hurts too', 'Y', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Patient is a great person/', 'The appointment went well', 'Give him medication, make him better.', 'Medical Student', 'Doctor', '2015-12-18 15:29:00', '2015-12-18 15:29:00'),
(3, 'LOCKED', 10, 10, '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 109, 0, 0, 0, 0, 0, 'Y', 'Y', '', 'Y', 'N', 'N', 'N', 'N', 'N', NULL, 'N', 'N', NULL, 'N', NULL, 'N', 'N', NULL, 'N', NULL, 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2015-12-25 00:25:53', '2015-12-25 00:25:53');

-- --------------------------------------------------------

--
-- Table structure for table `OfficeVisitFormTreatments`
--

CREATE TABLE IF NOT EXISTS `OfficeVisitFormTreatments` (
  `office_visit_form_treatment_id` int(11) NOT NULL AUTO_INCREMENT,
  `office_visit_id` int(11) NOT NULL,
  `region` text NOT NULL,
  `treatment_method` text NOT NULL,
  PRIMARY KEY (`office_visit_form_treatment_id`),
  KEY `office_visit_id` (`office_visit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `OfficeVisitFormTreatments`
--

INSERT INTO `OfficeVisitFormTreatments` (`office_visit_form_treatment_id`, `office_visit_id`, `region`, `treatment_method`) VALUES
(3, 2, 'Head', 'HVLA'),
(4, 2, 'Cervical', 'BLT');

-- --------------------------------------------------------

--
-- Table structure for table `PatientAccounts`
--

CREATE TABLE IF NOT EXISTS `PatientAccounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `date_of_birth` date NOT NULL,
  `password` varchar(256) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `PatientAccounts`
--

INSERT INTO `PatientAccounts` (`id`, `patient_id`, `first_name`, `last_name`, `date_of_birth`, `password`, `created`, `last_modified`) VALUES
(8, 8, 'Rich', 'Cerone', '1994-07-07', '$2y$10$cS1ptmfmfchDUn1wgDSszuKuOjjW310tbab073aW.GpjIs6vh1a8a', '2016-01-07 14:44:06', '2016-01-07 14:44:06');

-- --------------------------------------------------------

--
-- Table structure for table `PatientCurrentMedicine`
--

CREATE TABLE IF NOT EXISTS `PatientCurrentMedicine` (
  `patient_current_medicine_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_date_id` int(11) NOT NULL,
  `medicine_name` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`patient_current_medicine_id`),
  KEY `patient_id` (`patient_id`),
  KEY `visit_date_id` (`visit_date_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `PatientCurrentMedicine`
--

INSERT INTO `PatientCurrentMedicine` (`patient_current_medicine_id`, `patient_id`, `visit_date_id`, `medicine_name`, `created`, `last_modified`) VALUES
(1, 1, 1, 'Warfarin', '2015-12-18 04:14:19', '2015-12-18 04:14:19'),
(2, 1, 1, 'Aspirin', '2015-12-18 04:14:19', '2015-12-18 04:14:19'),
(3, 3, 3, 'medication', '2015-12-18 04:15:29', '2015-12-18 04:15:29'),
(4, 3, 3, 'medication2', '2015-12-18 04:15:29', '2015-12-18 04:15:29'),
(5, 3, 3, 'med3', '2015-12-18 04:15:29', '2015-12-18 04:15:29'),
(6, 2, 2, 'Coumadin', '2015-12-18 04:22:51', '2015-12-18 04:22:51'),
(7, 4, 4, '', '2015-12-18 04:31:03', '2015-12-18 04:31:03'),
(8, 5, 5, 'med 1', '2015-12-18 04:44:11', '2015-12-18 04:44:11'),
(9, 6, 6, 'med 1', '2015-12-18 05:25:19', '2015-12-18 05:25:19'),
(10, 6, 6, 'med2', '2015-12-18 05:25:19', '2015-12-18 05:25:19'),
(11, 6, 6, 'med3', '2015-12-18 05:25:19', '2015-12-18 05:25:19'),
(12, 7, 7, 'Aspirin', '2015-12-18 15:05:17', '2015-12-18 15:05:17'),
(13, 7, 7, 'Claritin', '2015-12-18 15:05:17', '2015-12-18 15:05:17'),
(15, 8, 12, 'none', '2016-01-06 14:40:43', '2016-01-06 14:40:43'),
(16, 14, 14, 'Tylenol', '2016-01-10 15:21:06', '2016-01-10 15:21:06');

-- --------------------------------------------------------

--
-- Table structure for table `PatientDemographics`
--

CREATE TABLE IF NOT EXISTS `PatientDemographics` (
  `demographics_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `preferred_name` text,
  `date_entered` date NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` text NOT NULL,
  `daily_work_performed` text NOT NULL,
  `completed_education` text,
  `has_history_tobacco` char(1) DEFAULT NULL,
  `alcohol_usage` text,
  `ethnicity` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`demographics_id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `PatientDemographics`
--

INSERT INTO `PatientDemographics` (`demographics_id`, `patient_id`, `preferred_name`, `date_entered`, `date_of_birth`, `gender`, `daily_work_performed`, `completed_education`, `has_history_tobacco`, `alcohol_usage`, `ethnicity`, `created`, `last_modified`) VALUES
(1, 1, '', '2015-12-17', '1923-12-20', 'male', ' C Programming Language Creator', 'GRADUATE', 'N', 'NONE', 'CAUCASIAN', '2015-12-18 04:13:00', '2015-12-18 04:13:00'),
(2, 3, 'J', '2015-12-17', '1983-12-22', 'male', ' work', 'HIGH SCHOOL', 'Y', 'NONE', 'BLACK', '2015-12-18 04:14:51', '2015-12-18 04:14:51'),
(3, 2, 'Dr. Dijkstra, Ph.D.', '2015-12-17', '1927-12-15', 'male', ' Creating great graph search algorithms', 'GRADUATE', 'N', 'NONE', 'CAUCASIAN', '2015-12-18 04:21:23', '2015-12-18 04:21:23'),
(4, 4, '', '2015-12-17', '1983-12-22', 'male', ' work', 'HIGH SCHOOL', 'Y', 'RARELY', 'NATIVE AMERICAN', '2015-12-18 04:29:41', '2015-12-18 04:29:41'),
(5, 5, '', '2015-12-17', '1983-12-22', 'male', ' work', 'HIGH SCHOOL', 'Y', 'RARELY', 'BLACK', '2015-12-18 04:42:58', '2015-12-18 04:42:58'),
(6, 6, '', '2015-12-18', '1983-12-22', 'male', ' work', 'HIGH SCHOOL', 'Y', 'RARELY', 'BLACK', '2015-12-18 05:24:09', '2015-12-18 05:24:09'),
(7, 7, 'John', '2015-12-18', '1983-12-09', 'male', ' Construction', 'HIGH SCHOOL', 'Y', 'RARELY', 'CAUCASIAN', '2015-12-18 15:01:22', '2015-12-18 15:01:22'),
(8, 8, 'Rich', '2016-01-06', '1994-07-07', 'male', ' nothing ', 'NO EDUCATION', 'N', 'SOCIALLY', 'CAUCASIAN', '2016-01-06 14:39:21', '2016-01-06 14:39:21'),
(9, 10, '', '2015-12-24', '2015-12-24', '', ' ', '', '', '', '', '2015-12-25 00:19:03', '2015-12-25 00:19:03'),
(10, 13, 'Richard', '2016-01-07', '2016-01-07', 'male', 'nothing', 'HIGH SCHOOL', 'N', 'SOCIALLY', 'CAUCASIAN', '2016-01-07 15:54:17', '2016-01-07 15:54:17'),
(11, 14, '', '2016-01-10', '2016-01-10', 'male', ' ', 'COLLEGE', 'N', 'SOCIALLY', 'BLACK', '2016-01-10 15:14:09', '2016-01-10 15:14:09');

-- --------------------------------------------------------

--
-- Table structure for table `PatientExistingConditions`
--

CREATE TABLE IF NOT EXISTS `PatientExistingConditions` (
  `patient_intake_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_date_id` int(11) NOT NULL,
  `changes_medication_two_weeks` text NOT NULL,
  `has_allergies` text NOT NULL,
  `has_physical_trauma` text NOT NULL,
  `personal_goals` text NOT NULL,
  `health_expectations` text NOT NULL,
  `interested_nutritional_topics` text NOT NULL,
  `other_concerns` text NOT NULL,
  `knows_about_do` text NOT NULL,
  `been_do_before` text NOT NULL,
  `knows_omm` text NOT NULL,
  `had_omm_before` text NOT NULL,
  `heard_about_clinic` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`patient_intake_id`),
  KEY `patient_id` (`patient_id`),
  KEY `visit_date_id` (`visit_date_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `PatientExistingConditions`
--

INSERT INTO `PatientExistingConditions` (`patient_intake_id`, `patient_id`, `visit_date_id`, `changes_medication_two_weeks`, `has_allergies`, `has_physical_trauma`, `personal_goals`, `health_expectations`, `interested_nutritional_topics`, `other_concerns`, `knows_about_do`, `been_do_before`, `knows_omm`, `had_omm_before`, `heard_about_clinic`, `created`, `last_modified`) VALUES
(1, 1, 1, 'No  changes', 'No', 'No', 'Touring the world', 'Remove headache', 'None', 'None', 'yes', 'yes', 'yes', 'yes', 'family-friend', '2015-12-18 04:14:19', '2015-12-18 04:14:19'),
(2, 3, 3, '', '', '', '', '', '', '', 'yes', 'no', 'yes', 'yes', 'family-friend', '2015-12-18 04:15:29', '2015-12-18 04:15:29'),
(3, 2, 2, 'No', 'No allergies', 'No accients', 'Being able to walk again', 'Being able to walk', 'No', 'No', 'yes', 'yes', 'yes', 'yes', 'family-friend', '2015-12-18 04:22:52', '2015-12-18 04:22:52'),
(4, 4, 4, '', '', '', '', '', '', '', 'yes', 'no', 'yes', 'no', 'family-friend', '2015-12-18 04:31:03', '2015-12-18 04:31:03'),
(5, 5, 5, '', 'no', '', '', '', '', 'yes', 'yes', 'no', 'yes', 'no', 'doctor', '2015-12-18 04:44:11', '2015-12-18 04:44:11'),
(6, 6, 6, '', '', '', '', '', '', '', 'yes', 'no', 'no', 'unsure', 'doctor', '2015-12-18 05:25:19', '2015-12-18 05:25:19'),
(7, 7, 7, 'No', 'Yes', 'yes', 'work again', 'Gain more function', 'None', 'No', 'yes', 'no', 'yes', 'no', 'doctor', '2015-12-18 15:05:17', '2015-12-18 15:05:17'),
(8, 8, 8, '', '', '', '', '', '', '', 'no', 'no', 'no', 'no', 'doctor', '2015-12-21 15:10:55', '2015-12-21 15:10:55'),
(9, 10, 10, '', '', '', '', '', '', '', '', '', '', '', '', '2015-12-25 00:20:47', '2015-12-25 00:20:47'),
(10, 10, 10, '', '', '', '', '', '', '', '', '', '', '', '', '2015-12-25 00:20:58', '2015-12-25 00:20:58'),
(11, 8, 12, 'no', 'no', 'no', 'mow lawn', 'none', 'none', 'no', 'no', 'no', 'no', 'no', 'doctor', '2016-01-06 14:40:43', '2016-01-06 14:40:43'),
(12, 13, 13, '', '', '', '', '', '', '', 'no', 'no', 'no', 'no', 'doctor', '2016-01-07 15:54:40', '2016-01-07 15:54:40'),
(13, 14, 14, 'No', 'No', 'No', 'Being normal again', 'Get my life together. ', 'No idea', 'No', 'no', 'no', 'no', 'no', 'doctor', '2016-01-10 15:21:06', '2016-01-10 15:21:06');

-- --------------------------------------------------------

--
-- Table structure for table `PatientMedicalConditions`
--

CREATE TABLE IF NOT EXISTS `PatientMedicalConditions` (
  `patient_medical_conditions_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_date_id` int(11) NOT NULL,
  `medical_condition` text NOT NULL,
  `date_of_diagnosis` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`patient_medical_conditions_id`),
  KEY `patient_id` (`patient_id`),
  KEY `visit_date_id` (`visit_date_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `PatientMedicalConditions`
--

INSERT INTO `PatientMedicalConditions` (`patient_medical_conditions_id`, `patient_id`, `visit_date_id`, `medical_condition`, `date_of_diagnosis`, `created`, `last_modified`) VALUES
(1, 3, 3, 'med cond', 'aaa', '2015-12-18 04:15:29', '2015-12-18 04:15:29'),
(2, 4, 4, 'med condition1', 'april', '2015-12-18 04:31:03', '2015-12-18 04:31:03'),
(3, 4, 4, 'medcondition 2', '', '2015-12-18 04:31:03', '2015-12-18 04:31:03'),
(4, 5, 5, 'condition 1', 'date', '2015-12-18 04:44:11', '2015-12-18 04:44:11'),
(5, 5, 5, 'condition 2', 'date 2', '2015-12-18 04:44:11', '2015-12-18 04:44:11'),
(6, 6, 6, 'med cond 1', 'date 1', '2015-12-18 05:25:19', '2015-12-18 05:25:19'),
(7, 6, 6, 'med cond 2', 'date 2', '2015-12-18 05:25:19', '2015-12-18 05:25:19'),
(8, 7, 7, 'Diabetes', 'Sometime in 2015', '2015-12-18 15:05:16', '2015-12-18 15:05:16'),
(9, 14, 14, 'Hungover', '01/08/16', '2016-01-10 15:21:06', '2016-01-10 15:21:06');

-- --------------------------------------------------------

--
-- Table structure for table `PatientPain`
--

CREATE TABLE IF NOT EXISTS `PatientPain` (
  `patient_pain_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_date_id` int(11) NOT NULL,
  `has_pain_now` char(1) NOT NULL,
  `pain_start_date` date NOT NULL,
  `activity_onset_pain` text,
  `pain_right_now` int(11) NOT NULL,
  `pain_at_worst` int(11) NOT NULL,
  `pain_at_best` int(11) NOT NULL,
  `pain_on_average` int(11) NOT NULL,
  `what_makes_pain_worse` text,
  `what_makes_pain_better` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`patient_pain_id`),
  KEY `patient_id` (`patient_id`),
  KEY `visit_date_id` (`visit_date_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `PatientPain`
--

INSERT INTO `PatientPain` (`patient_pain_id`, `patient_id`, `visit_date_id`, `has_pain_now`, `pain_start_date`, `activity_onset_pain`, `pain_right_now`, `pain_at_worst`, `pain_at_best`, `pain_on_average`, `what_makes_pain_worse`, `what_makes_pain_better`, `created`, `last_modified`) VALUES
(1, 1, 1, 'Y', '2015-02-03', 'Using Microsoft Windows', 6, 10, 4, 6, 'Using anything produced by Microsoft', 'Using Linux Operating System', '2015-12-18 04:13:30', '2015-12-18 04:13:30'),
(2, 3, 3, 'Y', '2015-03-13', '', 5, 5, 5, 5, '', '', '2015-12-18 04:15:00', '2015-12-18 04:15:00'),
(3, 2, 2, 'Y', '2015-04-03', 'Dropped brick on foot', 8, 10, 5, 6, 'Walking', 'Sitting down, icing, and booze.', '2015-12-18 04:22:02', '2015-12-18 04:22:02'),
(4, 4, 4, 'Y', '2015-05-11', '', 10, 10, 10, 10, 'nothing', 'nothing', '2015-12-18 04:30:12', '2015-12-18 04:30:12'),
(5, 5, 5, 'Y', '2015-05-12', 'nothing', 10, 10, 10, 10, 'nothing', 'nothing', '2015-12-18 04:43:14', '2015-12-18 04:43:14'),
(6, 6, 6, 'Y', '2015-03-10', 'nothing', 5, 5, 5, 5, 'nothing', 'nothing', '2015-12-18 05:24:20', '2015-12-18 05:24:20'),
(7, 7, 7, 'Y', '2015-03-09', 'Lifting heavy stuff', 5, 5, 5, 5, 'Lifting more heavy stuff', 'Couches', '2015-12-18 15:02:20', '2015-12-18 15:02:20'),
(8, 8, 8, 'N', '0000-00-00', '', 1, 3, 3, 1, '', '', '2015-12-21 15:10:34', '2015-12-21 15:10:34'),
(9, 10, 10, 'N', '0000-00-00', '', 3, 5, 6, 5, '', '', '2015-12-25 00:19:34', '2015-12-25 00:19:34'),
(10, 8, 12, 'Y', '2016-01-01', 'drinking', 5, 5, 5, 5, 'eating', 'pooping', '2016-01-06 14:39:45', '2016-01-06 14:39:45'),
(11, 13, 13, 'N', '0000-00-00', '', 5, 5, 5, 5, 'a', 'd', '2016-01-07 15:54:23', '2016-01-07 15:54:23'),
(12, 14, 14, 'N', '2016-01-08', 'Hungover', 5, 5, 5, 5, 'Everything', 'Not Moving', '2016-01-10 15:16:07', '2016-01-10 15:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `PatientPainCoordinates`
--

CREATE TABLE IF NOT EXISTS `PatientPainCoordinates` (
  `patient_pain_coordinates_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_pain_id` int(11) NOT NULL,
  `x_coord` double NOT NULL,
  `y_coord` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`patient_pain_coordinates_id`),
  KEY `patient_pain_id` (`patient_pain_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `PatientPainCoordinates`
--

INSERT INTO `PatientPainCoordinates` (`patient_pain_coordinates_id`, `patient_pain_id`, `x_coord`, `y_coord`, `created`, `last_modified`) VALUES
(1, 1, 108, 14, '2015-12-18 04:13:30', '2015-12-18 04:13:30'),
(2, 1, 134, 10, '2015-12-18 04:13:30', '2015-12-18 04:13:30'),
(3, 1, 499, 14, '2015-12-18 04:13:30', '2015-12-18 04:13:30'),
(4, 1, 485, 14, '2015-12-18 04:13:30', '2015-12-18 04:13:30'),
(5, 1, 477, 18, '2015-12-18 04:13:30', '2015-12-18 04:13:30'),
(6, 2, 251, 266, '2015-12-18 04:15:00', '2015-12-18 04:15:00'),
(7, 3, 143, 487, '2015-12-18 04:22:02', '2015-12-18 04:22:02'),
(8, 3, 118, 485, '2015-12-18 04:22:02', '2015-12-18 04:22:02'),
(9, 4, 311, 315, '2015-12-18 04:30:12', '2015-12-18 04:30:12'),
(10, 5, 293, 218, '2015-12-18 04:43:14', '2015-12-18 04:43:14'),
(11, 6, 324, 311, '2015-12-18 05:24:20', '2015-12-18 05:24:20'),
(12, 7, 185, 178, '2015-12-18 15:02:20', '2015-12-18 15:02:20'),
(13, 7, 190, 192, '2015-12-18 15:02:20', '2015-12-18 15:02:20'),
(14, 7, 190, 192, '2015-12-18 15:02:20', '2015-12-18 15:02:20'),
(15, 8, 79, 102, '2015-12-21 15:10:34', '2015-12-21 15:10:34'),
(16, 8, 596, 195, '2015-12-21 15:10:34', '2015-12-21 15:10:34'),
(17, 9, 153, 218, '2015-12-25 00:19:34', '2015-12-25 00:19:34'),
(18, 9, 154, 117, '2015-12-25 00:19:34', '2015-12-25 00:19:34'),
(19, 9, 67, 210, '2015-12-25 00:19:34', '2015-12-25 00:19:34'),
(20, 9, 92, 133, '2015-12-25 00:19:34', '2015-12-25 00:19:34'),
(21, 9, 57, 167, '2015-12-25 00:19:34', '2015-12-25 00:19:34'),
(22, 9, 109, 73, '2015-12-25 00:19:34', '2015-12-25 00:19:34'),
(23, 10, 73, 162, '2016-01-06 14:39:45', '2016-01-06 14:39:45'),
(24, 11, 126, 92, '2016-01-07 15:54:23', '2016-01-07 15:54:23'),
(25, 11, 553, 175, '2016-01-07 15:54:23', '2016-01-07 15:54:23'),
(26, 12, 36, 64, '2016-01-10 15:16:07', '2016-01-10 15:16:07'),
(27, 12, 44, 89, '2016-01-10 15:16:07', '2016-01-10 15:16:07'),
(28, 12, 46, 18, '2016-01-10 15:16:07', '2016-01-10 15:16:07'),
(29, 12, 46, 18, '2016-01-10 15:16:08', '2016-01-10 15:16:08'),
(30, 12, 46, 18, '2016-01-10 15:16:08', '2016-01-10 15:16:08'),
(31, 12, 50, 23, '2016-01-10 15:16:08', '2016-01-10 15:16:08'),
(32, 12, 34, 11, '2016-01-10 15:16:08', '2016-01-10 15:16:08'),
(33, 12, 138, 18, '2016-01-10 15:16:08', '2016-01-10 15:16:08'),
(34, 12, 131, 14, '2016-01-10 15:16:08', '2016-01-10 15:16:08'),
(35, 12, 136, 98, '2016-01-10 15:16:08', '2016-01-10 15:16:08');

-- --------------------------------------------------------

--
-- Table structure for table `PatientPastSurgery`
--

CREATE TABLE IF NOT EXISTS `PatientPastSurgery` (
  `patient_past_surgery_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_date_id` int(11) NOT NULL,
  `type_surgery` text NOT NULL,
  `date_of_surgery` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`patient_past_surgery_id`),
  KEY `patient_id` (`patient_id`),
  KEY `visit_date_id` (`visit_date_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `PatientPastSurgery`
--

INSERT INTO `PatientPastSurgery` (`patient_past_surgery_id`, `patient_id`, `visit_date_id`, `type_surgery`, `date_of_surgery`, `created`, `last_modified`) VALUES
(1, 3, 3, '', '', '2015-12-18 04:15:29', '2015-12-18 04:15:29'),
(2, 4, 4, '', '', '2015-12-18 04:31:03', '2015-12-18 04:31:03'),
(3, 1, 1, 'Brain Surgery', 'Sometime Last Year', '2015-12-18 05:13:18', '2015-12-18 05:13:18'),
(4, 1, 1, 'Gallbladder Surgery', '1950', '2015-12-18 05:13:48', '2015-12-18 05:13:48'),
(5, 6, 6, 'surgery 1 ', 'date 1', '2015-12-18 05:25:19', '2015-12-18 05:25:19'),
(6, 6, 6, 'surgery 3', 'date 3', '2015-12-18 05:25:19', '2015-12-18 05:25:19'),
(7, 7, 7, 'Wisdom Teeth Removal', 'Spring 2010', '2015-12-18 15:05:17', '2015-12-18 15:05:17');

-- --------------------------------------------------------

--
-- Table structure for table `Patients`
--

CREATE TABLE IF NOT EXISTS `Patients` (
  `patient_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `Patients`
--

INSERT INTO `Patients` (`patient_id`, `first_name`, `last_name`, `date_of_birth`, `created`, `last_modified`) VALUES
(1, 'Dennis', 'Ritchie', '1923-12-20', '2015-12-18 04:12:29', '2015-12-18 04:12:29'),
(2, 'Edger', 'Dijkstra', '1927-12-15', '2015-12-18 04:12:45', '2015-12-18 04:12:45'),
(3, 'John', 'Cento', '1983-12-22', '2015-12-18 04:14:24', '2015-12-18 04:14:24'),
(4, 'John', 'C', '1983-12-22', '2015-12-18 04:29:22', '2015-12-18 04:29:22'),
(5, 'Bob', 'Herman', '1983-12-22', '2015-12-18 04:42:40', '2015-12-18 04:42:40'),
(6, 'Joe', 'Johnson', '1983-12-22', '2015-12-18 05:23:57', '2015-12-18 05:23:57'),
(7, 'John', 'Doe', '1983-12-09', '2015-12-18 14:57:37', '2015-12-18 14:57:37'),
(8, 'Rich', 'Cerone', '1994-07-07', '2015-12-21 15:09:27', '2015-12-21 15:09:27'),
(9, 'Test', 'testing', '1990-12-25', '2015-12-25 00:17:36', '2015-12-25 00:17:36'),
(10, 'test', 'testing', '2015-12-24', '2015-12-25 00:18:39', '2015-12-25 00:18:39'),
(11, 'Darrrren', 'Martin', '1993-11-13', '2015-12-31 00:09:02', '2015-12-31 00:09:02'),
(12, 'Richard', 'Cerone', '1994-07-07', '2016-01-06 18:34:45', '2016-01-06 18:34:45'),
(13, 'Richard', 'Cerone', '2016-01-07', '2016-01-07 15:53:54', '2016-01-07 15:53:54'),
(14, 'Darren', 'Martin', '0000-00-00', '2016-01-10 15:13:28', '2016-01-10 15:13:28');

-- --------------------------------------------------------

--
-- Table structure for table `PatientSymptoms`
--

CREATE TABLE IF NOT EXISTS `PatientSymptoms` (
  `patient_symptom_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_date_id` int(11) NOT NULL,
  `patient_symptom` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`patient_symptom_id`),
  KEY `patient_id` (`patient_id`),
  KEY `visit_date_id` (`visit_date_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `PatientSymptoms`
--

INSERT INTO `PatientSymptoms` (`patient_symptom_id`, `patient_id`, `visit_date_id`, `patient_symptom`, `created`, `last_modified`) VALUES
(1, 1, 1, 'Headache', '2015-12-18 04:13:35', '2015-12-18 04:13:35'),
(2, 1, 1, 'Depression', '2015-12-18 04:13:35', '2015-12-18 04:13:35'),
(3, 3, 3, 'Weight Loss', '2015-12-18 04:15:05', '2015-12-18 04:15:05'),
(4, 3, 3, 'Sore Throat', '2015-12-18 04:15:05', '2015-12-18 04:15:05'),
(5, 3, 3, 'Weight Gain', '2015-12-18 04:15:05', '2015-12-18 04:15:05'),
(6, 3, 3, 'Hoarse Voice', '2015-12-18 04:15:05', '2015-12-18 04:15:05'),
(7, 3, 3, 'Tiredness', '2015-12-18 04:15:05', '2015-12-18 04:15:05'),
(8, 2, 2, 'Joint Pain', '2015-12-18 04:22:16', '2015-12-18 04:22:16'),
(9, 2, 2, 'Joint Swelling', '2015-12-18 04:22:16', '2015-12-18 04:22:16'),
(10, 2, 2, 'Chest Pains', '2015-12-18 04:22:16', '2015-12-18 04:22:16'),
(11, 2, 2, 'Fractures', '2015-12-18 04:22:16', '2015-12-18 04:22:16'),
(12, 2, 2, 'Vomiting', '2015-12-18 04:22:16', '2015-12-18 04:22:16'),
(13, 4, 4, 'Weight Loss', '2015-12-18 04:30:17', '2015-12-18 04:30:17'),
(14, 4, 4, 'Weight Gain', '2015-12-18 04:30:17', '2015-12-18 04:30:17'),
(15, 4, 4, 'Tiredness', '2015-12-18 04:30:17', '2015-12-18 04:30:17'),
(16, 4, 4, 'Fever', '2015-12-18 04:30:17', '2015-12-18 04:30:17'),
(17, 5, 5, 'Weight Loss', '2015-12-18 04:43:21', '2015-12-18 04:43:21'),
(18, 5, 5, 'Weight Gain', '2015-12-18 04:43:21', '2015-12-18 04:43:21'),
(19, 5, 5, 'Tiredness', '2015-12-18 04:43:21', '2015-12-18 04:43:21'),
(20, 5, 5, 'Vertigo', '2015-12-18 04:43:21', '2015-12-18 04:43:21'),
(21, 5, 5, 'Diarrhea', '2015-12-18 04:43:21', '2015-12-18 04:43:21'),
(22, 5, 5, 'Nose Bleeds', '2015-12-18 04:43:21', '2015-12-18 04:43:21'),
(23, 5, 5, 'Pain from walking', '2015-12-18 04:43:21', '2015-12-18 04:43:21'),
(24, 6, 6, 'Weight Gain', '2015-12-18 05:24:24', '2015-12-18 05:24:24'),
(25, 6, 6, 'Tiredness', '2015-12-18 05:24:24', '2015-12-18 05:24:24'),
(26, 6, 6, 'Fever', '2015-12-18 05:24:24', '2015-12-18 05:24:24'),
(27, 7, 7, 'Tiredness', '2015-12-18 15:02:49', '2015-12-18 15:02:49'),
(28, 7, 7, 'Heart Palpitations', '2015-12-18 15:02:49', '2015-12-18 15:02:49'),
(29, 7, 7, 'Cough', '2015-12-18 15:02:49', '2015-12-18 15:02:49'),
(30, 7, 7, 'Coughing Up Blood', '2015-12-18 15:02:49', '2015-12-18 15:02:49'),
(31, 8, 8, 'Weight Loss', '2015-12-21 15:10:43', '2015-12-21 15:10:43'),
(32, 8, 8, 'Surgeries', '2015-12-21 15:10:43', '2015-12-21 15:10:43'),
(33, 8, 8, 'Coughing Up Blood', '2015-12-21 15:10:43', '2015-12-21 15:10:43'),
(34, 8, 8, 'Problems Swallowing', '2015-12-21 15:10:43', '2015-12-21 15:10:43'),
(35, 8, 8, 'Decreased Hearing', '2015-12-21 15:10:43', '2015-12-21 15:10:43'),
(36, 8, 8, 'Diarrhea', '2015-12-21 15:10:43', '2015-12-21 15:10:43'),
(37, 8, 8, 'Pain from walking', '2015-12-21 15:10:43', '2015-12-21 15:10:43'),
(38, 10, 10, 'Fainting', '2015-12-25 00:20:32', '2015-12-25 00:20:32'),
(39, 10, 10, 'Cough', '2015-12-25 00:20:32', '2015-12-25 00:20:32'),
(40, 10, 10, 'Anemia', '2015-12-25 00:20:32', '2015-12-25 00:20:32'),
(41, 10, 10, 'Heat/Cold Intolerance', '2015-12-25 00:20:32', '2015-12-25 00:20:32'),
(42, 10, 10, 'Excessive Thirst', '2015-12-25 00:20:32', '2015-12-25 00:20:32'),
(43, 8, 12, 'Weight Loss', '2016-01-06 14:39:57', '2016-01-06 14:39:57'),
(44, 8, 12, 'Chest Pains', '2016-01-06 14:39:57', '2016-01-06 14:39:57'),
(45, 8, 12, 'Unable to Urinate', '2016-01-06 14:39:57', '2016-01-06 14:39:57'),
(46, 13, 13, 'Swollen Glands', '2016-01-07 15:54:27', '2016-01-07 15:54:27'),
(47, 14, 14, 'Seizures', '2016-01-10 15:16:34', '2016-01-10 15:16:34'),
(48, 14, 14, 'Blurred Vision', '2016-01-10 15:16:34', '2016-01-10 15:16:34'),
(49, 14, 14, 'Nausea', '2016-01-10 15:16:34', '2016-01-10 15:16:34');

-- --------------------------------------------------------

--
-- Table structure for table `PatientVideos`
--

CREATE TABLE IF NOT EXISTS `PatientVideos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `description` varchar(512) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `PatientVideos`
--

INSERT INTO `PatientVideos` (`id`, `patient_id`, `url`, `description`, `created`, `last_modified`) VALUES
(20, 8, 'https://www.youtube.com/watch?v=xefD_F0bqOc', 'test', '2015-12-28 15:50:24', '2015-12-28 15:50:24'),
(39, 8, 'https://www.youtube.com/watch?v=27dR_sLaM74', 'video', '2015-12-28 18:39:21', '2015-12-28 18:39:21');

-- --------------------------------------------------------

--
-- Table structure for table `Schedule`
--

CREATE TABLE IF NOT EXISTS `Schedule` (
  `slot_id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduled_patient_id` int(11) DEFAULT NULL,
  `slot_date` date NOT NULL,
  `start_time` datetime NOT NULL,
  `token` text NOT NULL,
  `completed_paperwork` char(1) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`slot_id`),
  KEY `scheduled_patient_id` (`scheduled_patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `Schedule`
--

INSERT INTO `Schedule` (`slot_id`, `scheduled_patient_id`, `slot_date`, `start_time`, `token`, `completed_paperwork`, `created`, `last_modified`) VALUES
(1, 1, '2015-12-17', '2015-12-17 08:30:00', '0pylg', 'Y', '2015-12-18 04:14:19', '2015-12-18 04:12:29'),
(2, 2, '2015-12-17', '2015-12-17 09:00:00', 'i549v', 'Y', '2015-12-18 04:22:52', '2015-12-18 04:12:45'),
(3, 3, '2015-12-17', '2015-12-17 09:30:00', 'or7zo', 'Y', '2015-12-18 04:15:29', '2015-12-18 04:14:24'),
(4, 4, '2015-12-17', '2015-12-17 08:00:00', '5rlsl', 'Y', '2015-12-18 04:31:03', '2015-12-18 04:29:22'),
(5, 5, '2015-12-17', '2015-12-17 08:15:00', 'k8g6w', 'Y', '2015-12-18 04:44:11', '2015-12-18 04:42:40'),
(6, 6, '2015-12-18', '2015-12-18 08:15:00', '9f9u9', 'Y', '2015-12-18 05:25:19', '2015-12-18 05:23:57'),
(7, 7, '2015-12-18', '2015-12-18 09:00:00', 'j7fa2', 'Y', '2015-12-18 15:05:17', '2015-12-18 14:57:37'),
(8, 8, '2015-12-21', '2015-12-21 08:00:00', 'i36wo', 'Y', '2015-12-21 15:10:55', '2015-12-21 15:09:27'),
(9, 9, '2015-12-18', '2015-12-18 10:00:00', 'ac2q1', NULL, '2015-12-25 00:17:36', '2015-12-25 00:17:36'),
(10, 10, '2015-12-24', '2015-12-24 08:30:00', 'p0wep', 'Y', '2015-12-25 00:20:47', '2015-12-25 00:18:39'),
(12, 8, '2016-01-06', '2016-01-06 09:30:00', '536tb', 'Y', '2016-01-06 14:40:43', '2016-01-06 14:39:07'),
(13, 13, '2016-01-07', '2016-01-07 11:00:00', 'ee19m', 'Y', '2016-01-07 15:54:40', '2016-01-07 15:53:54'),
(14, 14, '2016-01-10', '2016-01-10 08:00:00', 'p4rwy', 'Y', '2016-01-10 15:21:06', '2016-01-10 15:13:28');

-- --------------------------------------------------------

--
-- Table structure for table `SurveyBank`
--

CREATE TABLE IF NOT EXISTS `SurveyBank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `SurveyBank`
--

INSERT INTO `SurveyBank` (`id`, `title`, `created`, `last_modified`) VALUES
(32, 'Test', '2015-12-30 18:06:53', '2015-12-30 18:06:53'),
(33, 'Test 2', '2016-01-05 14:15:40', '2016-01-05 14:15:40');

-- --------------------------------------------------------

--
-- Table structure for table `SurveyResponses`
--

CREATE TABLE IF NOT EXISTS `SurveyResponses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `question` varchar(256) NOT NULL,
  `question_number` int(11) NOT NULL,
  `answer` varchar(364) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `SurveyResponses`
--

INSERT INTO `SurveyResponses` (`id`, `survey_id`, `title`, `question`, `question_number`, `answer`, `created`, `last_modified`) VALUES
(15, 32, 'Test', 'Test Question', 1, 'yes', '2016-01-04 16:21:59', '2016-01-04 16:21:59'),
(16, 32, 'Test', 'Test Question2', 2, 'here', '2016-01-04 16:21:59', '2016-01-04 16:21:59'),
(17, 32, 'Test', 'Test Question', 1, 'no', '2016-01-04 16:22:54', '2016-01-04 16:22:54'),
(18, 32, 'Test', 'Test Question2', 2, 'feed back', '2016-01-04 16:22:54', '2016-01-04 16:22:54'),
(19, 32, 'Test', 'Test Question', 1, 'no', '2016-01-05 14:14:22', '2016-01-05 14:14:22'),
(20, 32, 'Test', 'Test Question2', 2, 'hi', '2016-01-05 14:14:22', '2016-01-05 14:14:22'),
(21, 33, 'Test 2', '1', 1, 'yes', '2016-01-05 14:16:11', '2016-01-05 14:16:11'),
(22, 33, 'Test 2', '2', 2, 'yes', '2016-01-05 14:16:11', '2016-01-05 14:16:11'),
(23, 33, 'Test 2', '3', 3, 'no', '2016-01-05 14:16:11', '2016-01-05 14:16:11'),
(24, 33, 'Test 2', '1', 1, 'yes', '2016-01-05 14:16:19', '2016-01-05 14:16:19'),
(25, 33, 'Test 2', '2', 2, 'no', '2016-01-05 14:16:19', '2016-01-05 14:16:19'),
(26, 33, 'Test 2', '3', 3, 'yes', '2016-01-05 14:16:19', '2016-01-05 14:16:19'),
(27, 33, 'Test 2', '1', 1, 'yes', '2016-01-05 14:16:25', '2016-01-05 14:16:25'),
(28, 33, 'Test 2', '2', 2, 'no', '2016-01-05 14:16:25', '2016-01-05 14:16:25'),
(29, 33, 'Test 2', '3', 3, 'no', '2016-01-05 14:16:25', '2016-01-05 14:16:25');

-- --------------------------------------------------------

--
-- Table structure for table `Surveys`
--

CREATE TABLE IF NOT EXISTS `Surveys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `question` varchar(256) NOT NULL,
  `twoAnswers` tinyint(1) NOT NULL,
  `comment` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `Surveys`
--

INSERT INTO `Surveys` (`id`, `survey_id`, `question`, `twoAnswers`, `comment`, `created`, `last_modified`) VALUES
(26, 32, 'Test Question', 1, 0, '2015-12-30 18:06:53', '2015-12-30 18:06:53'),
(27, 32, 'Test Question2', 0, 1, '2015-12-30 18:06:53', '2015-12-30 18:06:53'),
(28, 33, '1', 1, 0, '2016-01-05 14:15:40', '2016-01-05 14:15:40'),
(29, 33, '2', 1, 0, '2016-01-05 14:15:40', '2016-01-05 14:15:40'),
(30, 33, '3', 1, 0, '2016-01-05 14:15:40', '2016-01-05 14:15:40');

-- --------------------------------------------------------

--
-- Table structure for table `SymptomList`
--

CREATE TABLE IF NOT EXISTS `SymptomList` (
  `symptom_id` int(11) NOT NULL AUTO_INCREMENT,
  `symptom_name` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`symptom_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `UserRole`
--

CREATE TABLE IF NOT EXISTS `UserRole` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`role_id`),
  KEY `role_name` (`role_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `UserRole`
--

INSERT INTO `UserRole` (`role_id`, `role_name`, `created`, `last_modified`) VALUES
(1, 'ADMIN', '2015-12-03 01:37:26', '2015-12-03 01:37:26'),
(2, 'DOCTOR', '2015-12-03 01:37:26', '2015-12-03 01:37:26'),
(3, 'MEDSTUDENT', '2015-12-03 01:37:26', '2015-12-03 01:37:26');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Authentication`
--
ALTER TABLE `Authentication`
  ADD CONSTRAINT `Authentication_ibfk_1` FOREIGN KEY (`user_role`) REFERENCES `UserRole` (`role_name`);

--
-- Constraints for table `Log`
--
ALTER TABLE `Log`
  ADD CONSTRAINT `Log_ibfk_1` FOREIGN KEY (`log_user_id`) REFERENCES `Authentication` (`user_id`);

--
-- Constraints for table `OfficeVisitForm`
--
ALTER TABLE `OfficeVisitForm`
  ADD CONSTRAINT `OfficeVisitForm_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `Patients` (`patient_id`),
  ADD CONSTRAINT `OfficeVisitForm_ibfk_2` FOREIGN KEY (`visit_date_id`) REFERENCES `Schedule` (`slot_id`);

--
-- Constraints for table `OfficeVisitFormTreatments`
--
ALTER TABLE `OfficeVisitFormTreatments`
  ADD CONSTRAINT `OfficeVisitFormTreatments_ibfk_1` FOREIGN KEY (`office_visit_id`) REFERENCES `OfficeVisitForm` (`office_visit_form_id`);

--
-- Constraints for table `PatientCurrentMedicine`
--
ALTER TABLE `PatientCurrentMedicine`
  ADD CONSTRAINT `PatientCurrentMedicine_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `Patients` (`patient_id`),
  ADD CONSTRAINT `PatientCurrentMedicine_ibfk_2` FOREIGN KEY (`visit_date_id`) REFERENCES `Schedule` (`slot_id`);

--
-- Constraints for table `PatientDemographics`
--
ALTER TABLE `PatientDemographics`
  ADD CONSTRAINT `PatientDemographics_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `Patients` (`patient_id`);

--
-- Constraints for table `PatientExistingConditions`
--
ALTER TABLE `PatientExistingConditions`
  ADD CONSTRAINT `PatientExistingConditions_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `Patients` (`patient_id`),
  ADD CONSTRAINT `PatientExistingConditions_ibfk_2` FOREIGN KEY (`visit_date_id`) REFERENCES `Schedule` (`slot_id`);

--
-- Constraints for table `PatientMedicalConditions`
--
ALTER TABLE `PatientMedicalConditions`
  ADD CONSTRAINT `PatientMedicalConditions_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `Patients` (`patient_id`),
  ADD CONSTRAINT `PatientMedicalConditions_ibfk_2` FOREIGN KEY (`visit_date_id`) REFERENCES `Schedule` (`slot_id`);

--
-- Constraints for table `PatientPain`
--
ALTER TABLE `PatientPain`
  ADD CONSTRAINT `PatientPain_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `Patients` (`patient_id`),
  ADD CONSTRAINT `PatientPain_ibfk_2` FOREIGN KEY (`visit_date_id`) REFERENCES `Schedule` (`slot_id`);

--
-- Constraints for table `PatientPainCoordinates`
--
ALTER TABLE `PatientPainCoordinates`
  ADD CONSTRAINT `PatientPainCoordinates_ibfk_1` FOREIGN KEY (`patient_pain_id`) REFERENCES `PatientPain` (`patient_pain_id`);

--
-- Constraints for table `PatientPastSurgery`
--
ALTER TABLE `PatientPastSurgery`
  ADD CONSTRAINT `PatientPastSurgery_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `Patients` (`patient_id`),
  ADD CONSTRAINT `PatientPastSurgery_ibfk_2` FOREIGN KEY (`visit_date_id`) REFERENCES `Schedule` (`slot_id`);

--
-- Constraints for table `PatientSymptoms`
--
ALTER TABLE `PatientSymptoms`
  ADD CONSTRAINT `PatientSymptoms_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `Patients` (`patient_id`),
  ADD CONSTRAINT `PatientSymptoms_ibfk_2` FOREIGN KEY (`visit_date_id`) REFERENCES `Schedule` (`slot_id`);

--
-- Constraints for table `Schedule`
--
ALTER TABLE `Schedule`
  ADD CONSTRAINT `Schedule_ibfk_1` FOREIGN KEY (`scheduled_patient_id`) REFERENCES `Patients` (`patient_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
