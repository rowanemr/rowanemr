<?php
	include 'core/init.php';
	if(!logged_in())
		header("Location: index.php");
	if(!get_user_role($_SESSION['id']) === "ADMIN")
		header("Location: index.php");
    include 'templates/header.php';
    include 'templates/navbar.php';
    include 'templates/remove-user-content.php';
    include 'templates/footer.php';
?>