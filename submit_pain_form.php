<?php
include 'core/init.php';
/* For Debugging */
error_reporting(E_ALL);
ini_set('display_errors', 1);
/* Token is propagated through a get variable. We can easily associate patient_id and slot_id with token
   by using the Schedule table
*/
if (!isset($_GET['token']) or !is_valid_token($_GET['token']))
    die("Valid token required");
$token = $_GET['token'];
$patient_id = get_patient_id_from_token($token);
$slot_id = get_slot_id_from_token($token);

/*------------------------------------------------------------------------------
 * Step 1: Collect our POST variables into PHP variables for readability.
 * We will perform data validation with helper functions.
 * Pay careful attention to variables which have bounds
 * As well as date parsing. SQL date convention is: YYYY-MM-DD
 * As well as Yes/No or True/False responses. SQL convention dictates we
 * store such information as CHAR(1): 'Y' or 'N'.
*/
// Convert Yes or No to Y or N
$has_pain_now = convertYesNo($_POST['has_pain']);
$month = clean($_POST['month']);
$day = clean($_POST['day']);
$year = clean($_POST['year']);
$date = sql_friendly_date($month.'/'.$day.'/'.$year); // Give date like 02/28/2015 and convert to SQL friendly date
if (!isReasonableYear($year))
    die("Please enter a reasonable year, {$year} is not reasonable.");
$activity_onset_pain = clean($_POST['activity']);
$pain_right_now = clean($_POST['pain_right_now']);
$pain_at_worst = clean($_POST['pain_at_worst']);
$pain_at_best = clean($_POST['pain_at_best']);
$pain_on_average = clean($_POST['pain_on_average']);
$makes_pain_worse = clean($_POST['makes_pain_worse']);
$makes_pain_better = clean($_POST['makes_pain_better']);
$coords_array = $_POST['coords']; //array of coordinates, e.g $coords_arr[0] = 'X Y'
/*------------------------------------------------------------------------------


/*------------------------------------------------------------------------------
 * Step 2: After preventing MySQL injection with clean() function, we will
 * construct a query to insert into the database.
 *
 */
$query = "INSERT INTO PatientPain ";
$query .= "(patient_id, visit_date_id, has_pain_now, pain_start_date, activity_onset_pain, pain_right_now, pain_at_worst, pain_at_best, pain_on_average, what_makes_pain_worse, what_makes_pain_better, created, last_modified) ";
$query .= "VALUES (?,?,?,?, ?,?,?,?,?,?,?, now(), now())";
//prepare
global $db;
if (!($stmt = $db->prepare($query)))
{
    echo "Prepare failed: (" . $db->errno . ") " . $db->error;
}
//bind
if (!($stmt->bind_param("ddsssddddss", $patient_id, $slot_id, $has_pain_now, $date, $activity_onset_pain, $pain_right_now, $pain_at_worst, $pain_at_best, $pain_on_average, $makes_pain_worse, $makes_pain_better)))
{
    echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
}

//execute
if ($stmt->execute()) {
    // Okay
} else {
    echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
}
$patient_pain_id = $stmt->insert_id; // the pk into PatientPain Table.
$stmt->close();

/*------------------------------------------------------------------------------
 * Step 3: Now we will insert the coordinates into our SQL table
 */

foreach ($coords_array as $coordinate) {
    //$coordinate is in form 'X Y'
    $x = explode(" ", $coordinate)[0]; //get before space
    $y = explode(" ", $coordinate)[1]; //get after space
    $query = "INSERT INTO PatientPainCoordinates ";
    $query .= "(patient_pain_id, x_coord, y_coord, created, last_modified) ";
    $query .= "VALUES (?, ?, ?, now(), now())";
    if (!($stmt = $db->prepare($query))) {
        echo "Prepare failed: (" . $db->errno . ") " . $db->error;
    }
    if (!($stmt->bind_param("ddd", $patient_pain_id, $x, $y))) {
        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    }
    if ($stmt->execute()) {
        // Okay
    } else {
        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
    }
} //endfor

/*
 * ---------------------------------------------------------------
 * Step 4: Cleanup and re-direct to new page
 */
$db->close();
header("Location: patient-symptoms.php?token=$token"); //Re-direct


/**
 * Will convert Yes/No to Y or N. Argument case does not matter.
 * @param string $src Contains Yes or No with any formatting
 * @return string
 */
function convertYesNo($src)
{
    $upper = strtoupper($src);
    if ($upper == 'YES')
        return 'Y';
    else
        return 'N';
}

/**
 * Will test if year is reasonable for patient input or not.
 * Checks 1915 - Present Year
 * @param int $year Year to Checks
 * @return bool
 */
function isReasonableYear($year)
{
    if (!is_numeric($year))
        return false;
    if ($year < 1915 or $year > date("Y"))
        return false;
    return true;
}

/**
 * Will return false if number is invalid (not between 0 and 10)
 * @param int $num The number to Checks
 * @return bool
 *
 */
function isValidPainRange($num)
{
    return $num >= 0 and $num <= 10;
}



?>
