<?php
  require_once 'core/init.php';
  if(!logged_in())
    header("Location: index.php");
  if(get_user_role($_SESSION['id']) === "ADMIN"){ //if user has role 'ADMIN'
  	//load users page
  	include 'templates/header.php';
  	include 'templates/navbar.php';
  	include 'templates/manage-surveys-content.php';
  	include 'templates/footer.php';
  }
  else{ //if user is not an administrator
  	//create an alert informing the user they don't have access and redirect them to the home page
  	echo '<script>'; 
	  echo 'alert("You need to be an administrator to use this functionality");'; 
	  echo 'window.location.replace("index.php");';
	  echo '</script>';
  }
?>