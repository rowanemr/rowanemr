<?php
// This stuff is to fill in the fields we already know
// If the user visits the clinic again, we want to auto-fill these fields out.

if (!isset($_GET['token']) or !is_valid_token($_GET['token']))
    die("Valid token required.");

$token = $_GET['token'];
$patient_id = get_patient_id_from_token($token);
$slot_id = get_slot_id_from_token($token);
// An associate array of fields filled out. Used to echo out values later.
$fields_filled_out = get_patient_info($patient_id); // Store first_name, last_name, date_of_birth in this assoc. array

// Now check to see if we have a previous Demographics input filled out
// There will only be ONE row in the database. If the patient comes again, they will UPDATE the row.

global $db;
$result = $db->query("SELECT  changes_medication_two_weeks, has_allergies, has_physical_trauma, personal_goals, health_expectations, interested_nutritional_topics, other_concerns, knows_about_do, been_do_before, knows_omm, had_omm_before, heard_about_clinic, created, last_modified FROM PatientExistingConditions WHERE patient_id = '$patient_id'");
if ($result->num_rows !== 0) {
    $row = $result->fetch_assoc();
    $fields_filled_out = array_merge($fields_filled_out, $row); //concatenate fields together
}

$medical_conditions_result = $db->query("SELECT medical_condition, date_of_diagnosis FROM PatientMedicalConditions WHERE visit_date_id = '$slot_id' AND patient_id = '$patient_id'");
$medical_conditions = $medical_conditions_result->fetch_all(MYSQLI_ASSOC);

$medications_result = $db->query("SELECT medicine_name FROM PatientCurrentMedicine WHERE visit_date_id = '$slot_id' AND patient_id = '$patient_id' ");
$medications = $medications_result->fetch_all(MYSQLI_ASSOC);

$surgery_result = $db->query("SELECT type_surgery, date_of_surgery FROM PatientPastSurgery WHERE visit_date_id = '$slot_id' AND patient_id = '$patient_id'");
$surgeries = $surgery_result->fetch_all(MYSQLI_ASSOC);


// Called during in-line HTML statements to print stuff out
function print_field($field_name)
{
    global $fields_filled_out;

    // This isset if is required because if it's the first time a patient is filling out this form
    // Then the key in this $fields_filled_out associative array will not exist
    if (isset($fields_filled_out[$field_name]))
        echo $fields_filled_out[$field_name];
}

// Will echo 'checked' if the field name is the field value
// For example print_field_checked('gender', 'male') would output checked for the checkbox if it's true
function print_field_checked($field_name, $field_value)
{
    global $fields_filled_out;
    // This isset if is required because if it's the first time a patient is filling out this form
    // Then the key in this $fields_filled_out associative array will not exist
    if (isset($fields_filled_out[$field_name]))
        if ($fields_filled_out[$field_name] == $field_value)
            echo " checked ";

}

?>


<!-- Script to add and delete fields -->
<script src="scripts/js/add-field.js"></script>


<script>
    //Function used to enable the 'other' text field when the 'other' radio button is selected.
    function enableOtherField() {
        document.getElementById("other-explanation").disabled = false;
    }
    //Function used to disable and clear the 'other' textfield when the 'other' radio button is not selected
    function disableOtherField() {
        document.getElementById("other-explanation").disabled = true;
        document.getElementById("other-explanation").value = "";
    }

    /**
     * Function called when user submits the form. This checks to make sure the user didn't fill out a date
     * without filling out the corresponding input it describes. For instance if the user input a date, but not a surgery
     * name for that date, it would be nonsensical. This stops the submission and alerts the user in such a case.
     **/
    $(function () {
        $("form").submit(function (e) { //Function executes when submit occurs
            $(".date-input").each(function () {
                if ($(this).val() != "") { //Check if a date has been entered
                    var correspondingInput = $(this).parents(".entry").find('input:first').val();

                    if (correspondingInput == "") { //Check if the input the date describes is empty
                        e.preventDefault(); //prevent submit if empty
                        alert("You must have a corresponding input for the date you've entered to complete submission.");
                        return false; //return false to stop the .each() loop
                    }
                }
            })
        });
    });

</script>


<h1 align="center">Existing Medical Conditions and Surgeries</h1>

<br>
<br>

<form action="core/database/add-edit-existing-med-conditions.php?token=<?php echo $token ?>" method="post">
    <!-- Add action to form here -->

    <!-- Input for any existing medical conditions -->
    <!-- The add-field.js script allows user to add another set of fields when add button is clicked.
         The script only creates a new field if the previous field is filled out, date can be excluded.
       -->
    <div class="container" align="center">
        <div class="row">

            <label class="control-label">What medical conditions do you currently have (ex: diabetes, high blood
                pressure)?
                When were you diagnosed?</label>

            <div class="controls">
                <div class="entry input-group">
                    <?php
                        if (count($medical_conditions) == 0)
                        {
                            // Nothing stored for this patient, give them an empty box
                            ?>
                            <div class="col-xs-5">
                                <input class="form-control" name="conditions[]" type="text"
                                       placeholder="Medical Condition"/>

                            </div>

                            <div class="col-xs-5">
                                <input class="form-control date-input" name="diagnosis-dates[]" type="text"
                                       placeholder="Date of Diagnosis"/>
                            </div>

                            <div class="col-xs-2">
                                <button class="btn btn-success btn-add" type="button">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </button>
                            </div>
                <?php
                        } // end if
                    // Otherwise, loop through these. Fill out box with existing med condition/date of diagnosis
                    for ($i = 0; $i < count($medical_conditions); $i++) {
                        $med_condition = $medical_conditions[$i];
                        ?>
                        <div class="entry input-group">

                            <div class="col-xs-5">
                                <input class="form-control" name="conditions[]" type="text"
                                       value="<?php echo $med_condition['medical_condition']; ?>"
                                       placeholder="Medical Condition"/>

                            </div>

                            <div class="col-xs-5">
                                <input class="form-control date-input" name="diagnosis-dates[]" type="text"
                                       value="<?php $med_condition['date_of_diagnosis']; ?>"
                                       placeholder="Date of Diagnosis"/>
                            </div>

                            <div class="col-xs-2">
                                <?php
                                if ($i != count($medical_conditions) - 1) // put remove buttons for all but the last item
                                    echo '<button class="btn btn-danger btn-remove" type="button"><span class="glyphicon glyphicon-minus"></span> </button>';
                                else
                                    echo '<button class="btn btn-success btn-add" type="button"><span class="glyphicon glyphicon-plus"></span> </button>';
                                ?>
                            </div>
                        </div>
                        <?php
                    } // end medical conditions for loop
                    ?>

                </div>
            </div>

        </div>
    </div>


    <!-- Input for medications currently being taken  -->
    <div class="container" align="center">
        <div class="row">
            <div class="control-group" id="medication-fields">
                <label class="control-label">What medications are you currently taking?</label>

                <div class="controls">
                    <?php
                    if (count($medications) == 0) {
                        // Give the user one blank medications if nothing stored
                        ?>
                        <div class="entry input-group">
                            <div class="col-xs-10">
                                <input class="form-control" name="medications[]" type="text"
                                       placeholder="Name of Medication"/>
                            </div>
                            <div class="col-xs-2">
                                <button class="btn btn-success btn-add" type="button">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </button>
                            </div>
                        </div>
                        <?php
                    } // end if for count($medications) == 0
                    // Loop through medications and show input boxes for what we already know
                    for ($i = 0; $i<count($medications); $i++)
                    {
                        ?>
                        <div class="entry input-group">
                            <div class="col-xs-10">
                                <input class="form-control" name="medications[]" type="text"
                                       value="<?php echo $medications[$i]['medicine_name']?>" placeholder="Name of Medication"/>
                            </div>
                            <div class="col-xs-2">
                                <?php
                                if ($i != count($medications) - 1) // put remove buttons for all but the last item
                                    echo '<button class="btn btn-danger btn-remove" type="button"><span class="glyphicon glyphicon-minus"></span> </button>';
                                else
                                    echo '<button class="btn btn-success btn-add" type="button"><span class="glyphicon glyphicon-plus"></span> </button>';
                                ?>
                            </div>
                        </div>
                    <?php
                    } // end $medications for
                    ?>
                </div>
            </div>
        </div>
    </div>


    <!-- Input for past surgeries -->
    <div class="container" align="center">
        <div class="row">

            <label class="control-label">Have you had surgery before? If so, when?</label>

            <div class="controls">
                <?php
                if (count($surgeries) == 0) {
                    ?>
                    <div class="entry input-group">

                        <div class="col-xs-5">
                            <input class="form-control" name="surgeries[]" type="text" placeholder="Type of Surgery"/>
                        </div>

                        <div class="col-xs-5">
                            <input class="form-control date-input" name="surgery-dates[]" type="text"
                                   placeholder="Date of Surgery"/>
                        </div>

                        <div class="col-xs-2">
                            <button class="btn btn-success btn-add" type="button">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                        </div>
                    </div>
                    <?php
                } // end if for count($surgieres)
                for ($i = 0; $i<count($surgeries); $i++)
                {
                    $surgery = $surgeries[$i];
                    ?>
                <div class="entry input-group">

                    <div class="col-xs-5">
                        <input class="form-control" name="surgeries[]" type="text" placeholder="Type of Surgery" value="<?php echo $surgery['type_surgery']?>"/>
                    </div>

                    <div class="col-xs-5">
                        <input class="form-control date-input" name="surgery-dates[]" type="text"
                               placeholder="Date of Surgery" value = "<?php echo $surgery['date_of_surgery']?>"/>
                    </div>

                    <div class="col-xs-2">
                        <?php
                        if ($i != count($surgeries) - 1) // put remove buttons for all but the last item
                            echo '<button class="btn btn-danger btn-remove" type="button"><span class="glyphicon glyphicon-minus"></span> </button>';
                        else
                            echo '<button class="btn btn-success btn-add" type="button"><span class="glyphicon glyphicon-plus"></span> </button>';
                        ?>
                    </div>
                </div>
                <?php
                } //end for for $suergies
                ?>



            </div>

        </div>
    </div>


    <!-- Open ended questions to fill out -->
    <div class="form-group" align="center">
        <label class="control-label">Have there been any changes to your medication in the past two weeks?</label>
        <input type="text" class="form-control" id="medication-changes" name="medication-changes"
               value="<?php print_field('changes_medication_two_weeks') ?>" style="width: 500px;">
    </div>

    <div class="form-group" align="center">
        <label class="control-label">Do you have allergies?</label>
        <input type="text" class="form-control" id="allergies" name="allergies"
               value="<?php print_field('has_allergies') ?>" style="width: 500px;">
    </div>

    <div class="form-group" align="center">
        <label class="control-label">Have you had any falls, car accidents, or other physical trauma?</label>
        <input type="text" class="form-control" id="physical-trauma" name="physical-trauma"
               value="<?php print_field('has_physical_trauma') ?>" style="width: 500px;">
    </div>

    <div class="form-group" align="center">
        <label class="control-label">What are your personal goals upon gaining full function (e.g. play with kids, mow
            the lawn, etc.)?</label>
        <input type="text" class="form-control" id="goals" name="goals" value="<?php print_field('personal_goals') ?>"
               style="width: 500px;">
    </div>

    <div class="form-group" align="center">
        <label class="control-label">What are your health expectations from this appointment?</label>
        <input type="text" class="form-control" id="health-expectations" name="health-expectations"
               value="<?php print_field('health_expectations') ?>" style="width: 500px;">
    </div>

    <div class="form-group" align="center">
        <label class="control-label">What Nutritional topics would you like to learn about today?</label>
        <input type="text" class="form-control" id="nutrional-topics" name="nutritional-topics"
               value="<?php print_field('interested_nutritional_topics') ?>" style="width: 500px;">
    </div>

    <div class="form-group" align="center">
        <label class="control-label">Would you like to discuss any other medical concerns today?</label>
        <input type="text" class="form-control" id="medical-concerns" name="medical-concerns"
               value="<?php print_field('other_concerns') ?>" style="width: 500px;">
    </div>

    <!-- All radio button questions require an answer -->
    <!-- Know what a DO is input  -->
    <div class="form-group" align="center">
        <label class="control-label">Do you know what an Osteopathic Physician (DO) is?</label>

        <label class="radio-inline">
            <input type="radio" name="do-aware" id="yes-do-aware" value="yes"
                   required <?php print_field_checked('knows_about_do', 'yes') ?>> Yes
        </label>
        <label class="radio-inline">
            <input type="radio" name="do-aware" id="no-do-aware" value="no"
                   required <?php print_field_checked('knows_about_do', 'no') ?>> No
        </label>
        <label class="radio-inline">
            <input type="radio" name="do-aware" id="unsure-do-aware" value="unsure"
                   required <?php print_field_checked('knows_about_do', 'unsure') ?>> Unsure
        </label>
    </div>


    <!-- Been to DO before input -->
    <div class="form-group" align="center">
        <label class="control-label">Have you been to a DO before</label>

        <label class="radio-inline">
            <input type="radio" name="do-visited" id="yes-visited" value="yes"
                   required <?php print_field_checked('been_do_before', 'yes') ?>> Yes
        </label>
        <label class="radio-inline">
            <input type="radio" name="do-visited" id="no-visited" value="no"
                   required <?php print_field_checked('been_do_before', 'no') ?>> No
        </label>
        <label class="radio-inline">
            <input type="radio" name="do-visited" id="unsure-visited" value="unsure"
                   required <?php print_field_checked('been_do_before', 'unsure') ?>> Unsure
        </label>
    </div>


    <!-- Know what OMM is input -->
    <div class="form-group" align="center">
        <label class="control-label">Do you know what osteopathic manipulative medicine (OMM) is?</label>

        <label class="radio-inline">
            <input type="radio" name="omm-aware" id="yes-omm-aware" value="yes"
                   required <?php print_field_checked('knows_omm', 'yes') ?>> Yes
        </label>
        <label class="radio-inline">
            <input type="radio" name="omm-aware" id="no-omm-aware" value="no"
                   required <?php print_field_checked('knows_omm', 'no') ?>> No
        </label>
        <label class="radio-inline">
            <input type="radio" name="omm-aware" id="unsure-omm-aware" value="unsure"
                   required <?php print_field_checked('knows_omm', 'unsure') ?>> Unsure
        </label>
    </div>


    <!-- Had OMM performed on you input -->
    <div class="form-group" align="center">
        <label class="control-label">Have you had OMM performed on you?</label>

        <label class="radio-inline">
            <input type="radio" name="omm-performed" id="yes-omm-performed" value="yes"
                   required <?php print_field_checked('had_omm_before', 'yes') ?>> Yes
        </label>
        <label class="radio-inline">
            <input type="radio" name="omm-performed" id="no-omm-performed" value="no"
                   required <?php print_field_checked('had_omm_before', 'no') ?>> No
        </label>
        <label class="radio-inline">
            <input type="radio" name="omm-performed" id="unsure-omm-performed" value="unsure"
                   required <?php print_field_checked('had_omm_before', 'unsure') ?>> Unsure
        </label>
    </div>


    <!-- Hear about us input -->
    <div class="form-group" align="center">
        <label class="control-label">How did you hear about us?</label>

        <label class="radio-inline">
            <input type="radio" name="hear-about" id="flier" value="flier" onclick="disableOtherField()"
                   required <?php print_field_checked('heard_about_clinic', 'flier') ?>> Flier
        </label>
        <label class="radio-inline">
            <input type="radio" name="hear-about" id="family-friend" value="family-friend" onclick="disableOtherField()"
                   required <?php print_field_checked('heard_about_clinic', 'family-friend') ?>> Family/Friend
        </label>
        <label class="radio-inline">
            <input type="radio" name="hear-about" id="doctor" value="doctor" onclick="disableOtherField()"
                   required <?php print_field_checked('heard_about_clinic', 'doctor') ?>> Doctor
        </label>
        <label class="radio-inline">
            <input type="radio" name="hear-about" id="other" value="other" onclick="enableOtherField()"
                   required <?php print_field_checked('heard_about_clinic', 'other') ?>> Other
        </label>

        <input type="text" class="form-control" id="other-explanation" name="other-explanation"
               placeholder="If other, explain"
               style="width: 500px;" disabled <?php print_field('heard_about_clinic') ?>>

    </div>

    <!-- Submit button -->
    <br>

    <div style="text-align:center">
        <button type="submit" class="btn btn-info">Submit</button>
    </div>

</form> <!-- End Form -->

<br>
<br>
<br>
<br>
