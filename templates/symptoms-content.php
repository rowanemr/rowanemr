<?
if (!isset($_GET['token']) or !is_valid_token($_GET['token']))
    die("Valid token required");
?>
<!-- Title -->
<div align="center">
  <h3>Patient Symptoms Intake</h3>
  <p>Have you suffered from any of the following in the last two weeks? Select all that apply.</p>
</div>
<style>
    table.borderless td,table.borderless th{
        border: none !important;
    }
</style>
<!-- Symptom Intake Form Checklist. -->
<form action="submit_symptoms_form.php?token=<?php echo $_GET['token'] ?>" method="post">
                <table class="table borderless" align="center" style="width: auto;">
                <tr>
                <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Weight Loss">Weight Loss</label></td>
                <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Sore Throat">Sore Throat</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Joint Pain">Joint Pain</label></td>
                </tr>
                                    <tr>
                <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Weight Gain">Weight Gain</label></td>
                <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Hoarse Voice">Hoarse Voice</label> </td>
                <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Joint Swelling">Joint Swelling</label></td>
                </tr>

                
                    <tr>
                        <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Tiredness">Tiredness</label></td>
                        <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Swollen Glands">Swollen Glands</label></td>
                        <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Osteoporosis">Osteoporosis</label></td>
                    </tr>

                

                <tr>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Fever">Fever</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Chest Pains">Chest Pains</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Seizures">Seizures</label></td>

                </tr>
                
                <tr>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Chills">Chills</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Heart Palpitations">Heart Palpitations</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Fainting">Fainting</label></td>
                </tr>
                
                    <tr>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Rashes">Rashes</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="High Blood Pressure">High Blood Pressure</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Dizziness">Dizziness</label></td>
                    </tr>
                <tr>
                
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Sores">Sores</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Swelling of arms/legs">Swelling of arms/legs</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Headache">Headache</label></td>
                </tr>
                
                    <tr>

                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Fractures">Fractures</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Cough">Cough</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Anemia">Anemia</label></td>
                        </tr>
                
                    <tr>

                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Surgeries">Surgeries</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Coughing Up Mucus">Coughing Up Mucus</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Heat/Cold Intolerance">Heat/Cold Intolerance</label></td>
                        </tr>
                
                    <tr>

                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Blurred Vision">Blurred Vision</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Coughing Up Blood">Coughing Up Blood</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Excessive Thirst">Excessive Thirst</label></td>
                        </tr>
                
                    <tr>

                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Teary Eyes">Teary Eyes</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Wheezing">Wheezing</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Anxiety">Anxiety</label></td>
                        </tr>
                
                    <tr>

                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Glaucoma">Glaucoma</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Shortness of Breath">Shortness of Breath</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Depression">Depression</label></td>
                        </tr>
                
                    <tr>

                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Cataracts">Cataracts</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Problems Swallowing">Problems Swallowing</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Memory Loss">Memory Loss</label></td>
                        </tr>
                
                    <tr>

                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Decreased Hearing">Decreased Hearing</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Nausea">Nausea</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Ringing of the Ears">Ringing of the Ears</label></td>
                        </tr>
                    <tr>

                    
                    <td> <label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Vomiting">Vomiting</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Vertigo">Vertigo</label></td>
                    <td> <label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Constipation">Constipation</label></td>
                        </tr>
                
                    <tr>

                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Congestion">Congestion</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Diarrhea">Diarrhea</label></td>
                    <td> <label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Running Nose">Running Nose</label></td>
                        </tr>
                
                    <tr>

                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Frequent Urination">Frequent Urination</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Nose Bleeds">Nose Bleeds</label></td>
                    <td> <label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Unable to Urinate">Unable to Urinate</label></td>
                        </tr>
                

                    <tr>

                    <td> <label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Bleeding Gums">Bleeding Gums</label></td>
                    <td><label class="checkbox-inline"><input type="checkbox" name="symptom[]" value="Pain from walking">Pain from walking</label></td>
                    </tr>
                </table>
                
                


    <!-- Submit button -->
    
    <div style="text-align:center">
        <button type="submit" class="btn btn-info">Submit</button>
    </div>

</form> <!-- End Form -->





