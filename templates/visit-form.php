<!DOCTYPE html>
<html>
<?php
  if (!isset($_GET['patient_id']) or !isset($_GET['slot_id']))
  {
    // We require these to work. The patient is is required so we know patient we are talking about.
    // The slot id is required so we know what visit to associate this with
    // Technically we can get the patient id from the slot id, but the less queries we make, the faster the pages loads
    die("Patient ID and Slot ID required.");
  }
$patient_id = clean_up($_GET['patient_id']);
$slot_id = clean_up($_GET['slot_id']); // aka visit_date_id

check_if_locked($patient_id, $slot_id); // if locked, die, or if an admin, print out a warning

$addnew = 1; // assume we are a blank form unless otherwise set

// Check to see if data is already filled out for this form, and if so, put the values into the form.
global $db; //from core/init.php

$office_visit_info = $db->query("SELECT * FROM OfficeVisitForm WHERE patient_id = '$patient_id' AND visit_date_id = '$slot_id'");
$fields_filled_out = $office_visit_info->fetch_assoc() or array();
$omm_fields_filled_out = array(); // get populated later. Declare here so it's in the higher scope.
if ($office_visit_info->num_rows > 0) {
  // If we filled out this form before, let's get these values and put them in the HTML
  $addnew = 0; // not adding new, updating
  $fields_filled_out['height_feet'] = intval(doubleval($fields_filled_out['height']) / 12.0); // height stored in inches
  $fields_filled_out['height_inches'] = intval(doubleval($fields_filled_out['height']) % 12.0); // height stored in inches
  $office_visit_id = $fields_filled_out['office_visit_form_id']; // The ID to join against for OfficeVisitFormTreatments

  // Note in this next query we are using office_visit_id from OfficeVisitForm table's primary key
  // And not a visit_date_id (aka slot_id)
  $omm_exam_info = $db->query("SELECT * FROM OfficeVisitFormTreatments WHERE office_visit_id = '$office_visit_id'");
  $omm_fields_filled_out = $omm_exam_info->fetch_all(MYSQLI_ASSOC) or array(); // there won't be that many rows here
}


// Called during in-line HTML statements to print stuff out
/**
 * Used to print out a field name in the HTML. The inteded usage is inline with the HTML to allow UPDATE.
 * For example, we might have this in HTML somewhere:
 *   <input type="text" value="<?php print_field('height')?>">
 *   This example will print (not return) the height field if it exists.
 * This function makes use of $fields_filled_out associative array, which is formed through a query
 * (I used a global here rather than an argument to avoid more typing with the inline HTML)
 * @param string $field_name The field we want to print. Must exist in table OfficeVisitForm.
 */
function print_field($field_name)
{
  global $fields_filled_out;

  // This isset if is required because if it's the first time a patient is filling out this form
  // Then the key in this $fields_filled_out associative array will not exist
  if (isset($fields_filled_out[$field_name]))
    echo $fields_filled_out[$field_name];
}

// Will echo 'checked' if the field name is the field value
// For example print_field_checked('gender', 'male') would output checked for the checkbox if it's true
// $output can also be 'selected' for HTML Select dropdowns, eg:
//  <select><option selected>1</option>...</select>
/**
 * Will print out checked if the field name given is equal to the field value
 * Used to print out (not return) `checked` with inline HTML to check checkboxes.
 * This function makes use of $fields_filled_out associative array, which is formed through a query
 * You may also change parameter $output to be used for HTML Select dropdows. Eg:
 *      <select><option selected>1</option><option>2</option>...</select>
 * You may also set $field_value if you want to check something other than 'Y'. By convention,
 * our database will store 'Y' where something was checked in a checkbox or is True; and 'N' otherwise.
 * Example Call: print_field_checked('lungs_clear') => would print out `checked`
 * @param string $field_name The field name to print, should exist in OfficeVisitForm table.
 * @param string $field_value The value to compare against, and if true, print out `checked`
 * @param string $output The value to print out. By default, it is `checked`.
 */
function print_field_checked($field_name, $field_value='Y', $output="checked")
{
  global $fields_filled_out;
  // This isset if is required because if it's the first time a patient is filling out this form
  // Then the key in this $fields_filled_out associative array will not exist
  if (isset($fields_filled_out[$field_name]))
    if ($fields_filled_out[$field_name] == $field_value)
      echo " $output ";

}

/// Print checked if a specific region has a specific treatment used
// Example call: print_omm_treatment_checked('Head', 'ME')

/**
 * Print out checked if a specific region has a specific treatment method used
 * Used to print out `checked` for form Updates
 * Example call:
 *     print_omm_treatment_checked(`Head`, `ME`) would print out checked
 *     if ME was used on the Head.
 * @param string $region The region such as `Head`
 * @param string $treatment_method The treatment method used such as `ME`
 */
function print_omm_treatment_checked($region, $treatment_method)
{
  global $omm_fields_filled_out;
  foreach ($omm_fields_filled_out as $row)
  {
    if (isset($row['region']) and isset($row['treatment_method'])) {
      if ($row['region'] == $region and $row['treatment_method'] == $treatment_method) {
        echo " checked";
        return; // if we found it, then leave
      }
    }
  }
}

/**
 * Used to stop executing the page if the form is locked and the user is a non-admin.
 * If the user is an admin, we will let them update the page, but they will be warned.
 * @param int $patient_id The patient ID to check for
 * @param int $slot_id The slot ID to check for
 */
function check_if_locked($patient_id, $slot_id)
{
  $form_status = get_form_status($patient_id, $slot_id);
  $user_role = get_user_role($_SESSION['id']); //get id of Doctor (not patient!)
// Prevent all non-admins from editing a locked form.
  if ($form_status == STATUS_LOCKED and $user_role != 'ADMIN') // Form is locked, sorry, you can't edit it!
    die("Sorry, this form is locked! <a href = 'patient-info.php?patient_id=$patient_id'>Click here for a read-only copy of this patient's data</a>");
  elseif ($form_status == STATUS_LOCKED and $user_role == 'ADMIN') // allow admins to do anything
  {
    echo '<div class="alert alert-danger"><strong>Attention! </strong>';
    echo 'This form is <strong>locked/read-only</strong> to all users, except admins. You may edit this form (as an admin).</div>';
  }
}

?>

<!-- Head tag: Sets title and calls formatting -->
<head>
  <title>Visit Form </title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script>

    function updateBMI() {
      // BMI = (Weight in Pounds * 703) / (Height In Inches ^ 2)
      var weight = $("#weight").val();
      var height_inches = $("#height-inches").val();
      var height_feet = $("#height-feet").val();
      if (weight == 0 || height_feet == 0)
        return;

      var total_height = parseInt((height_feet * 12)) + parseInt(height_inches);
      var bmi = (weight * 703) / (total_height * total_height);
      $("#bmi").val(bmi);

    }

    function toggle(src, name) {
      var checkboxes = document.getElementsByName(name);
      for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = src.checked;
      }
    }

    function uncheck(id)
    {
      var all_normal_checkbox = document.getElementById(id);
      all_normal_checkbox.checked = false;
    }

    function toggle_check_all(src)
    {
      // Mark all checkboxes under physical tab (div) as checked
      var physical_div = document.getElementById('physical');
      var checkboxes = physical_div.getElementsByTagName('input');
      for (var i = 0; i<checkboxes.length; i++)
      {
        if (checkboxes[i].type == 'checkbox')
          checkboxes[i].checked = src.checked;
      }
    }

    function checkHeartRate()
    {
      var rate_num = document.getElementById("heart_rate");
      if(rate_num.value == "" || rate_num.value == null )
      {
        alert("Enter a Heart Rate");
        rate_num.focus();
      }
      if(rate_num.value >= 110)
      {
        if (confirm("Are you sure you want to enter this number?")
          == true) 
        {
          document.getElementById("resp_rate").focus();
        } 
        else 
        {
          rate_num.value = "";
          rate_num.focus();
        }
      }
    }

    function checkRespRate()
    {
      var resp_num = document.getElementById("resp_rate");
      if(resp_num.value == "" || resp_num.value == null )
      {
        alert("Enter a Respiratory Rate");
        resp_num.focus();
      }
      if(resp_num.value >= 23)
      {
        if (confirm("Are you sure you want to enter this number?")
          == true) 
        {
          document.getElementById("systolic_bp").focus();
        } 
        else 
        {
          resp_num.value = "";
          resp_num.focus();
        }
      }
    }

    function checkSystolicBP()
    {
      var systolic_num = document.getElementById("systolic_bp");
      if(systolic_num.value == "" || systolic_num.value == null )
      {
        alert("Enter a Systolic Rate");
        systolic_num.focus();
      }
      if(systolic_num.value >= 160)
      {
        if (confirm("Are you sure you want to enter this number?")
          == true) 
        {
          document.getElementById("diastolic_bp").focus();
        } 
        else 
        {
          systolic_num.value = "";
          systolic_num.focus();
        }
      }
    }

    function checkDiastolicBP()
    {
      var diastolic_num = document.getElementById("diastolic_bp");
      if(diastolic_num.value == "" || diastolic_num.value == null )
      {
        alert("Enter a Diastolic Rate");
        diastolic_num.focus();
      }
      if(diastolic_num.value >= 110)
      {
        if (confirm("Are you sure you want to enter this number?")
          == true) 
        {
          //Wait
        } 
        else 
        {
          diastolic_num.value = "";
          diastolic_num.focus();
        }
      }
    }

  </script>
   <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">

</head>

<!-- Body Tag: Starts body for rest of webpage -->

<body>
  <h1 align="center"> COMMUNITY OMM CLINIC VISIT FORM</h1>

<!-- Creates Tabs -->
<div class="nav-center">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#intake">Intake Paperwork</a></li>
    <li><a data-toggle="tab" href="#complaint">Chief Complaint</a></li>
    <li><a data-toggle="tab" href="#history">History</a></li>
    <li><a data-toggle="tab" href="#vitals">Vitals</a></li>
    <li><a data-toggle="tab" href="#physical">Physical</a></li>
    <li><a data-toggle="tab" href="#omm">OMM Exam</a></li>
    <li><a data-toggle="tab" href="#notes">Notes</a></li>
  </ul>
</div>


  <div class="tab-content" align="center">
<div id = "intake" class="tab-pane active">
  <h2 align="center">Intake Work</h2>
  <?php
    $patient_id = clean_up($_GET['patient_id']); //pass patient ID

    include 'patient-info-content.php';
  ?>
</div>

<!-- Chief Complaint Tab -->
    <div id="complaint" class="tab-pane">

    <form action="submit_visit_form.php?patient_id=<?php echo $patient_id?>&slot_id=<?php echo $slot_id?>&addnew=<?php echo $addnew ?>" method="post">
      <h2 align="center"> Chief Complaint</h2>
      <div class="complaint" align="center">
        <textarea class="form-control" rows="5" name="chief_complaint" placeholder="Chief Complaint" style="width: 500px;"><?php print_field('chief_complaint') ?></textarea>
      </div>

    </div>


<!-- History Tab -->
  <div id="history" class="tab-pane fade">

    <div class="container" align="center">
      <table class="table table-bordered">
        <thead>
          <h2>History of Present Illness</h2>
        </thead>
        <tbody>
          <tr>
            <td width="10%">Location</td>
            <td>
              <textarea class="form-control" rows="1" name="location"><?php print_field('location')?></textarea>
            </td>
          </tr>
          <tr>
            <td>Quality</td>
            <td>
              <textarea class="form-control" rows="1" name="quality"><?php print_field('quality')?></textarea>
            </td>
          </tr>
          <tr>
            <td>Severity</td>
            <td>
              <textarea class="form-control" rows="1" name="severity"><?php print_field('severity')?></textarea>
            </td>
          </tr>
          <tr>
            <td>Duration</td>
            <td>
              <textarea class="form-control" rows="1" name="duration"><?php print_field('duration')?></textarea>
            </td>
          </tr>
          <tr>
            <td>Timing</td>
            <td>
              <textarea class="form-control" rows="1" name="timing"><?php print_field('timing')?></textarea>
            </td>
          </tr>
          <tr>
            <td>Radiation</td>
            <td>
              <textarea class="form-control" rows="1" name="radiation"><?php print_field('radiation')?></textarea>
            </td>
          </tr>
          <tr>
            <td>Context</td>
            <td>
              <textarea class="form-control" rows="1" name="context"><?php print_field('context')?></textarea>
            </td>
          </tr>
          <tr>
            <td>Modifying Factors</td>
            <td>
              <textarea class="form-control" rows="1" name="modifying_factors"><?php print_field('modifying_factors')?></textarea>
            </td>
          </tr>
          <tr>
            <td>Signs/Symptoms</td>
            <td>
              <textarea class="form-control" rows="1" name="signs_symptoms"><?php print_field('associated_signs_symptoms')?></textarea>
            </td>
          </tr>
        </tbody>
      </table>
    </div>


<!-- Medical History Tab -->
    <div class="container" align="center">

      <table class="table table-bordered">
        <tbody>
          <tr>
            <td width="20%">Past Medical History</td>
            <td width="30%">
              <label>
                <input type="checkbox" name="reviewed_medical_history" value="1"> Reviewed with patient from intake sheet</label>
            </td>
            <td>
              <textarea class="form-control" rows="1" name="medical_history"><?php print_field('past_medical_history')?></textarea>
            </td>
          </tr>
          <tr>
            <td>Past Surgical History</td>
            <td width="30%">
              <label>
                <input type="checkbox" name="reviewed_surgical_history" value="1"> Reviewed with patient from intake sheet</label>
            </td>
            <td>
              <textarea class="form-control" rows="1" name="surgical_history"><?php print_field('past_surgical_history')?></textarea>
            </td>
          </tr>
          <tr>
            <td>Medications</td>
            <td width="30%">
              <label>
                <input type="checkbox" name="reviewed_medications" value="1"> Reviewed with patient from intake sheet</label>
            </td>
            <td>
              <textarea class="form-control" rows="1" name="medications"><?php print_field('medications')?></textarea>
            </td>
          </tr>
          <tr>
            <td>Allergies</td>
            <td width="30%">
              <label>
                <input type="checkbox" name="reviewed_allergies" value="1"> Reviewed with patient from intake sheet</label>
            </td>
            <td>
              <textarea class="form-control" rows="1" name="allergies"><?php print_field('allergies')?></textarea>
            </td>
          </tr>
          <tr>
            <td>Social History</td>
            <td width="30%">
              <label>
                <input type="checkbox" name="reviewed_social_history" value="1"> Reviewed with patient from intake sheet</label>
            </td>
            <td>
              <textarea class="form-control" rows="1" name="social_history"><?php print_field('social_history')?></textarea>
            </td>
          </tr>
          <tr>
            <td>Review of Symptoms</td>
            <td width="30%">
              <label>
                <input type="checkbox" name="reviewed_symptoms" value="1"> Reviewed with patient from intake sheet</label>
            </td>
            <td>
              <textarea class="form-control" rows="1" name="review_of_symptoms"><?php print_field('review_of_symptoms')?></textarea>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>


<!-- Vital Signs Tab -->
  <div id="vitals" class="tab-pane fade">
    <div class="container" align="center">

      <table class="table table-bordered">
        <thead>
          <h2>Vital Signs</h2>
        </thead>
        <tbody>
          <tr>
            <td width="10%">Heart Rate:
              <input type="text" class="form-control" id="heart_rate" name="heart_rate" placeholder="0" style="width: 50px;"
                     value="<?php print_field('heart_rate') ?>" onblur="checkHeartRate()">
            </td>
            <td width="10%">Respiratory Rate:
              <input type="text" class="form-control" id="resp_rate" name="resp_rate" placeholder="0" style="width: 50px;"
                     value="<?php print_field('resp_rate') ?>"
                     onblur="checkRespRate()">
            </td>
            <td width="10%">Systolic BP:
              <input type="text" class="form-control" id="systolic_bp" name="systolic_bp" placeholder="0" style="width: 50px;"
                     value="<?php print_field('systolic_bp') ?>"
                     onblur="checkSystolicBP()">
            </td>
            <td width="10%">Diastolic BP:
              <input type="text" class="form-control" id="diastolic_bp" name="diastolic_bp" placeholder="0" style="width: 50px;"
                     value="<?php print_field('diastolic_bp') ?>"
                     onblur="checkDiastolicBP()">
            </td>
          </tr>
          <tr>
            <td>Height:
              <div id="feetlabel">Feet:</div>
              <select id="height-feet" name="height_feet" onchange="updateBMI();">
                <option value="0" <?php print_field_checked('height_feet', 0, 'selected')?> >0</option>
                <option value="1" <?php print_field_checked('height_feet', 1, 'selected')?>>1</option>
                <option value="2" <?php print_field_checked('height_feet', 2, 'selected')?>>2</option>
                <option value="3" <?php print_field_checked('height_feet', 3, 'selected')?>>3</option>
                <option value="4" <?php print_field_checked('height_feet', 4, 'selected')?>>4</option>
                <option value="5" <?php print_field_checked('height_feet', 5, 'selected')?>>5</option>
                <option value="6" <?php print_field_checked('height_feet', 6, 'selected')?>>6</option>
                <option value="7" <?php print_field_checked('height_feet', 7, 'selected')?>>7</option>
              </select>
              <div id="inchlabel">Inches:</div>
              <select id="height-inches" name="height_inches" onchange="updateBMI();">
                <option value="0" <?php print_field_checked('height_inches', 0, 'selected')?>>0</option>
                <option value="1" <?php print_field_checked('height_inches', 1, 'selected')?>>1</option>
                <option value="2" <?php print_field_checked('height_inches', 2, 'selected')?>>2</option>
                <option value="3" <?php print_field_checked('height_inches', 3, 'selected')?>>3</option>
                <option value="4" <?php print_field_checked('height_inches', 4, 'selected')?>>4</option>
                <option value="5" <?php print_field_checked('height_inches', 5, 'selected')?>>5</option>
                <option value="6" <?php print_field_checked('height_inches', 6, 'selected')?>>6</option>
                <option value="7" <?php print_field_checked('height_inches', 7, 'selected')?>>7</option>
                <option value="8" <?php print_field_checked('height_inches', 8, 'selected')?>>8</option>
                <option value="9" <?php print_field_checked('height_inches', 9, 'selected')?>>9</option>
                <option value="10" <?php print_field_checked('height_inches', 10, 'selected')?>>10</option>
                <option value="11" <?php print_field_checked('height_inches', 11, 'selected')?>>11</option>
              </select>
            </td>
            <td>Weight:
              <input type="text" class="form-control" id = "weight" name="weight" onblur="updateBMI();" placeholder="0" style="width: 100px;"
                     value="<?php print_field('weight') ?>">
            </td>
            <td>Calculated BMI
            <input type = "text" class="form-control" id="bmi" placeholder="0" style="width: 100px;" disabled>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>


<!-- Physical Exam Tab -->
  <div id="physical" class="tab-pane fade">
    <div class="container" align="center">

      <table class="table table-bordered">
        <thead>
          <h3>Physical Exam</h3>
          <h4>Check if normal, write abnormalities</h4>
          <h5><label><input type="checkbox" value="normal" onClick="toggle_check_all(this)"> Check if all normal </label></h5>
        </thead>

        <tbody>
          <tr>
            <!-- Physical Exam -->

            <!--- When the All normal checkbox is clicked, it will call toggle on the name (gen_exam[])
                  When the other checkboxes are clicked, it will uncheck the All normal checkbox (by id gen_checkall)
                  Note the difference in that toggle() checks for name, uncheck() uses id
                  This is somewhat hacky, but it works and I think makes sense for our purpose.
             -->
            <td width="40%"> <h4>Gen: <label><input type="checkbox" id="gen_checkall" onClick="toggle(this, 'gen_exam[]')"> All normal</label> </h4>
              <label><input type="checkbox" name="gen_exam[]" value="awake" onClick="uncheck('gen_checkall')" <?php print_field_checked('gen_awake_alert')?>> Awake Alert and oriented x 3</label>
              <label><input type="checkbox" name="gen_exam[]" value="noacutedistress" onClick="uncheck('gen_checkall')" <?php print_field_checked('gen_no_acute_distress')?>> No acute distress</label>
            </td>
            <td>
              <h6> Write abnormalities </h6>
              <textarea class="form-control" rows="2" name="gen_abnormalities"><?php print_field('gen_abnormal')?></textarea>
            </td>
          </tr>
          <tr>
            <td width="40%"> <h4>HEENT: <label><input type="checkbox" id="heent_checkall" onClick="toggle(this, 'heent_exam[]')"> All normal</label> </h4>
              <h4>Ears:</h4>
              <label><input type="checkbox" name="heent_exam[]" value="tmclear" onClick="uncheck('heent_checkall')" <?php print_field_checked('heent_ears_clear_intact')?>> TM clear and intact</label>
              <label><input type="checkbox" name="heent_exam[]" value="canalsclear" onClick="uncheck('heent_checkall')" <?php print_field_checked('heent_ears_canals_clear')?>> Canals clear</label>

              <h4>Neck:</h4>
              <label><input type="checkbox" name="heent_exam[]" value="supple" onClick="uncheck('heent_checkall')" <?php print_field_checked('heent_neck_supple_nontender')?>> Supple, nontender</label>
              <label><input type="checkbox" name="heent_exam[]" value="nothyromegaly" onClick="uncheck('heent_checkall') <?php print_field_checked('heent_neck_no_thyromegaly')?>"> No thyromegaly or lymphadenopathy</label>

              <h4>Mouth:</h4>
              <label><input type="checkbox" name="heent_exam[]" value="mucous" onClick="uncheck('heent_checkall')" <?php print_field_checked('heent_mouth_mucous_membranes_moist')?>> Mucous membranes moist</label>
              <label><input type="checkbox" name="heent_exam[]" value="noerythema" onClick="uncheck('heent_checkall')" <?php print_field_checked('heent_mouth_no_erythema')?>> No erythema, exudates or tonsillar enlargement</label>

            </td>
            <td>
              <h6> Write abnormalities </h6>
              <textarea class="form-control" rows="10" name="heent_abnormalities"><?php print_field('heent_abnormal')?></textarea>
            </td>
          </tr>
          <tr>
            <td width="40%"> <h4>Cardiac: <label><input type="checkbox" id="cardiac_checkall" onClick="toggle(this, 'cardiac_exam[]')"> All normal</label> </h4>

              <label><input type="checkbox" name="cardiac_exam[]" value="regularrate" onClick="uncheck('cardiac_checkall')" <?php print_field_checked('cardiac_regular_rate')?>> Regular rate and rhythm; Normal S1, S2</label>
              <label><input type="checkbox" name="cardiac_exam[]" value="nomurmurs" onClick="uncheck('cardiac_checkall')" <?php print_field_checked('cardiac_murmurs')?>> No murmurs, rubs, gallops</label>
            </td>
            <td>
              <h6> Write abnormalities </h6>
              <textarea class="form-control" rows="2" name="cardiac_abnormalities"><?php print_field('cardiac_abnormal')?></textarea>
            </td>
          </tr>
          <tr>
            <td width="40%"> <h4>Lungs: <label><input type="checkbox" name="lung_exam[]" value="clearauscultation" <?php print_field_checked('lungs_clear')?>> All normal</label> </h4>
              Clear to auscultation, no wheezes, rales, or rhonci
             </td>
            <td>
              <h6> Write abnormalities </h6>
              <textarea class="form-control" rows="2" name="lung_abnormalities"><?php print_field('lungs_abnormal')?></textarea>
            </td>
          </tr>
          <tr>
            <td width="40%"> <h4>GI: <label><input type="checkbox" id="gi_checkall" onClick="toggle(this, 'gi_exam[]')"> All normal</label> </h4>
              <label><input type="checkbox" name="gi_exam[]" value="abdomensoft" onClick="uncheck('gi_checkall')" <?php print_field_checked('gi_abdomen_soft')?>> Abdomen soft, non-tender</label>
              <label><input type="checkbox" name="gi_exam[]" value="nomasses" onClick="uncheck('gi_checkall')" <?php print_field_checked('gi_no_masses')?>> No masses, splenomegaly or hepatomegaly</label>
            </td>
            <td>
              <h6> Write abnormalities </h6>
              <textarea class="form-control" rows="2" name="gi_abnormalities"><?php print_field('gi_abnormal')?></textarea>
            </td>
          </tr>
          <tr>
            <td width="40%">
              <!-- Ext category only has one checkbox, so no checkall is needed -->
              <h4>Ext: <label><input type="checkbox" name="ext_exam[]" value="noedema"<?php print_field_checked('ext_no_edema')?>> All normal</label> </h4>
               No edema, cyanosis, clubbing
             </td>
            <td>
              <h6> Write abnormalities </h6>
              <textarea class="form-control" rows="2" name="ext_abnormalities"><?php print_field('ext_abnormal')?></textarea>
            </td>
          </tr>
          <tr>
            <td width="40%"><h4>MSK: <label><input type="checkbox" id="msk_checkall" onClick="toggle(this, 'msk_exam[]')"> All normal</label> </h4>
              <label><input type="checkbox" name="msk_exam[]" value="fullmotion" onClick="uncheck('msk_checkall')" <?php print_field_checked('msk_full_range_motion')?>> Full range of motion</label>
              <label><input type="checkbox" name="msk_exam[]" value="nojoint" onClick="uncheck('msk_checkall')" <?php print_field_checked('msk_no_joint_deformity')?>> No joint deformity</label>
              <label><input type="checkbox" name="msk_exam[]" value="nomuscle" onClick="uncheck('msk_checkall')" <?php print_field_checked('msk_no_muscle_hypertonicity')?>> No muscle hypertonicity</label>
            </td>
            <td>
              <h6> Write abnormalities </h6>
              <textarea class="form-control" rows="2" name="msk_abnormalities"><?php print_field('msk_abnormal')?></textarea>
            </td>
          </tr>
          <tr>
            <td width="40%"> <h4>Neuro: <label><input type="checkbox" id="neuro_checkall" onClick="toggle(this, 'neuro_exam[]')"> All normal</label> </h4>
              <label><input type="checkbox" name="neuro_exam[]" value="cranial" onClick="uncheck('neuro_checkall')" <?php print_field_checked('neuro_cranial_nerves_intact')?>> Cranial nerves II through XII intact</label>
              <label><input type="checkbox" name="neuro_exam[]" value="musclestrength" onClick="uncheck('neuro_checkall')" <?php print_field_checked('neuro_muscle_strength')?>> Muscle strength 5/5: upper and lower extremities</label>
              <label><input type="checkbox" name="neuro_exam[]" value="sensational" onClick="uncheck('neuro_checkall')" <?php print_field_checked('neuro_sensation_intact')?>> Sensation intact to light touch: upper and lower extremities</label>
              <label><input type="checkbox" name="neuro_exam[]" value="deeptendon" onClick="uncheck('neuro_checkall')" <?php print_field_checked('neuro_deep_tendon_reflexes')?>> Deep Tendon Reflexes 2/4: upper and lower extremities</label>
            </td>
            <td>
              <h6> Write abnormalities </h6>
              <textarea class="form-control" rows="5" name="neuro_abnormalities"><?php print_field('neuro_abnormal')?></textarea>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

<!-- OMM EXAM Tab -->
   <div id="omm" class="tab-pane fade">
    <div class="container">
      <h3 align="center">OMM EXAM, DIAGNOSIS AND TREATMENT</h3>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Region</th>
            <th>Diagnosis/Somatic Dysfunction</th>
            <th>OMM</th>
            <th>Treatment Method</th>
            <th>Result</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Head</td>
            <td>
              <textarea class="form-control" rows="1" name="head"><?php print_field('head_diagnosis')?></textarea>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="headomm" value="Y" <?php print_field_checked('head_omm')?>>Yes</label>
                <label>
                  <input type="radio" name="headomm" value="N">No <?php print_field_checked('head_omm', 'N')?></label>
              </div>
            </td>
            <td>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="headtreatmentmethod[]" value="ME" <?php print_omm_treatment_checked('Head', 'ME')?>>ME</label>
                <label>
                  <input type="checkbox" name="headtreatmentmethod[]" value="MFR" <?php print_omm_treatment_checked('Head', 'MFR')?>>MFR</label>
                <label>
                  <input type="checkbox" name="headtreatmentmethod[]" value="HVLA" <?php print_omm_treatment_checked('Head', 'HVLA')?>>HVLA</label>
                <label>
                  <input type="checkbox" name="headtreatmentmethod[]" value="ART" <?php print_omm_treatment_checked('Head', 'ART')?>>ART</label>
                <label>
                  <input type="checkbox" name="headtreatmentmethod[]" value="ST" <?php print_omm_treatment_checked('Head', 'ST')?>>ST</label>
                <label>
                  <input type="checkbox" name="headtreatmentmethod[]" value="FPR" <?php print_omm_treatment_checked('Head', 'FPR')?>>FPR</label>
                <label>
                  <input type="checkbox" name="headtreatmentmethod[]" value="CS" <?php print_omm_treatment_checked('Head', 'CS')?>>CS</label>
                <label>
                  <input type="checkbox" name="headtreatmentmethod[]" value="BLT" <?php print_omm_treatment_checked('Head', 'BLT')?>>BLT</label>
                <label>
                  <input type="checkbox" name="headtreatmentmethod[]" value="OCMM" <?php print_omm_treatment_checked('Head', 'OCMM')?>>OCMM</label>
              </div>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="headresult" value="Resolved" <?php print_field_checked('head_result', 'Resolved')?> >Resolved</label>
                <label>
                  <input type="radio" name="headresult" value="Improved" <?php print_field_checked('head_result', 'Improved')?> >Improved</label>
                <label>
                  <input type="radio" name="headresult" value="Unchanged" <?php print_field_checked('head_result', 'Unchanged')?> >Unchanged</label>
                <label>
                  <input type="radio" name="headresult" value="Worsened" <?php print_field_checked('head_result', 'Worsened')?> >Worsened</label>
              </div>
            </td>
          </tr>
          <tr>
            <td>Cervical</td>
            <td>
              <textarea class="form-control" rows="1" name="cervical"><?php print_field('cervical_diagnosis')?></textarea>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="cervicalomm" value="Y" <?php print_field_checked('cervical_omm')?>>Yes</label>
                <label>
                  <input type="radio" name="cervicalomm" value="N" <?php print_field_checked('cervical_omm', 'N')?>>No</label>
              </div>
            </td>
            <td>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="cervicaltreatmentmethod[]" value="ME" <?php print_omm_treatment_checked('Cervical', 'ME')?>>ME</label>
                <label>
                  <input type="checkbox" name="cervicaltreatmentmethod[]" value="MFR" <?php print_omm_treatment_checked('Cervical', 'MFR')?>>MFR</label>
                <label>
                  <input type="checkbox" name="cervicaltreatmentmethod[]" value="HVLA" <?php print_omm_treatment_checked('Cervical', 'HVLA')?>>HVLA</label>
                <label>
                  <input type="checkbox" name="cervicaltreatmentmethod[]" value="ART" <?php print_omm_treatment_checked('Cervical', 'ART')?>>ART</label>
                <label>
                  <input type="checkbox" name="cervicaltreatmentmethod[]" value="ST" <?php print_omm_treatment_checked('Cervical', 'ST')?>>ST</label>
                <label>
                  <input type="checkbox" name="cervicaltreatmentmethod[]" value="FPR" <?php print_omm_treatment_checked('Cervical', 'FPR')?>>FPR</label>
                <label>
                  <input type="checkbox" name="cervicaltreatmentmethod[]" value="CS" <?php print_omm_treatment_checked('Cervical', 'CS')?>>CS</label>
                <label>
                  <input type="checkbox" name="cervicaltreatmentmethod[]" value="BLT" <?php print_omm_treatment_checked('Cervical', 'BLT')?>>BLT</label>
                <label>
                  <input type="checkbox" name="cervicaltreatmentmethod[]" value="OCMM" <?php print_omm_treatment_checked('Cervical', 'OCMM')?>>OCMM</label>
              </div>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="cervicalresult" value="Resolved" <?php print_field_checked('cervical_result', 'Resolved')?>>Resolved</label>
                <label>
                  <input type="radio" name="cervicalresult" value="Improved" <?php print_field_checked('cervical_result', 'Improved')?>>Improved</label>
                <label>
                  <input type="radio" name="cerviaclresult" value="Unchanged" <?php print_field_checked('cervical_result', 'Unchanged')?>>Unchanged</label>
                <label>
                  <input type="radio" name="cervicalresult" value="Worsened" <?php print_field_checked('cervical_result', 'Worsened')?>>Worsened</label>
              </div>
            </td>
          </tr>
          <tr>
            <td>Thoracic</td>
            <td>
              <textarea class="form-control" rows="1" name="thoracic"><?php print_field('thoracic_diagnosis')?></textarea>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="thoracicomm" value="Y" <?php print_field_checked('thoracic_omm')?>>Yes</label>
                <label>
                  <input type="radio" name="thoracicomm" value="N" <?php print_field_checked('thoracic_omm', 'N')?>>No</label>
              </div>
            </td>
            <td>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="thoracictreatmentmethod[]" value="ME" <?php print_omm_treatment_checked('Thoracic', 'ME')?>>ME</label>
                <label>
                  <input type="checkbox" name="thoracictreatmentmethod[]" value="MFR" <?php print_omm_treatment_checked('Thoracic', 'MFR')?>>MFR</label>
                <label>
                  <input type="checkbox" name="thoracictreatmentmethod[]" value="HVLA" <?php print_omm_treatment_checked('Thoracic', 'HVLA')?>>HVLA</label>
                <label>
                  <input type="checkbox" name="thoracictreatmentmethod[]" value="ART" <?php print_omm_treatment_checked('Thoracic', 'ART')?>>ART</label>
                <label>
                  <input type="checkbox" name="thoracictreatmentmethod[]" value="ST" <?php print_omm_treatment_checked('Thoracic', 'ST')?>>ST</label>
                <label>
                  <input type="checkbox" name="thoracictreatmentmethod[]" value="FPR" <?php print_omm_treatment_checked('Thoracic', 'FPR')?>>FPR</label>
                <label>
                  <input type="checkbox" name="thoracictreatmentmethod[]" value="CS" <?php print_omm_treatment_checked('Thoracic', 'CS')?>>CS</label>
                <label>
                  <input type="checkbox" name="thoracictreatmentmethod[]" value="BLT" <?php print_omm_treatment_checked('Thoracic', 'BLT')?>>BLT</label>
                <label>
                  <input type="checkbox" name="thoracictreatmentmethod[]" value="OCMM" <?php print_omm_treatment_checked('Thoracic', 'OCMM')?>>OCMM</label>
              </div>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="thoracicresult" value="Resolved" <?php print_field_checked('thoracic_result', 'Resolved')?>>Resolved</label>
                <label>
                  <input type="radio" name="thoracicresult" value="Improved" <?php print_field_checked('thoracic_result', 'Improved')?>>Improved</label>
                <label>
                  <input type="radio" name="thoracicresult" value="Unchanged" <?php print_field_checked('thoracic_result', 'Unchanged')?>>Unchanged</label>
                <label>
                  <input type="radio" name="thoracicresult" value="Worsened" <?php print_field_checked('thoracic_result', 'Worsened')?>>Worsened</label>
              </div>
            </td>
          </tr>
          <tr>
            <td>Lumbar</td>
            <td>
              <textarea class="form-control" rows="1" name="lumbar"><?php print_field('lumbar_diagnosis')?></textarea>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="lumbaromm" value="Y" <?php print_field_checked('lumbar_omm')?>>Yes</label>
                <label>
                  <input type="radio" name="lumbaromm" value="N" <?php print_field_checked('lumbar_omm', 'N')?>>No</label>
              </div>
            </td>
            <td>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="lumbartreatmentmethod[]" value="ME" <?php print_omm_treatment_checked('Lumbar', 'ME')?>>ME</label>
                <label>
                  <input type="checkbox" name="lumbartreatmentmethod[]" value="MFR" <?php print_omm_treatment_checked('Lumbar', 'MFR')?>>MFR</label>
                <label>
                  <input type="checkbox" name="lumbartreatmentmethod[]" value="HVLA" <?php print_omm_treatment_checked('Lumbar', 'HVLA')?>>HVLA</label>
                <label>
                  <input type="checkbox" name="lumbartreatmentmethod[]" value="ART" <?php print_omm_treatment_checked('Lumbar', 'ART')?>>ART</label>
                <label>
                  <input type="checkbox" name="lumbartreatmentmethod[]" value="ST" <?php print_omm_treatment_checked('Lumbar', 'ST')?>>ST</label>
                <label>
                  <input type="checkbox" name="lumbartreatmentmethod[]" value="FPR" <?php print_omm_treatment_checked('Lumbar', 'FPR')?>>FPR</label>
                <label>
                  <input type="checkbox" name="lumbartreatmentmethod[]" value="CS"<?php print_omm_treatment_checked('Lumbar', 'CS')?>>CS</label>
                <label>
                  <input type="checkbox" name="lumbartreatmentmethod[]" value="BLT" <?php print_omm_treatment_checked('Lumbar', 'BLT')?>>BLT</label>
                <label>
                  <input type="checkbox" name="lumbartreatmentmethod[]" value="OCMM" <?php print_omm_treatment_checked('Lumbar', 'OCMM')?>>OCMM</label>
              </div>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="lumbarresult" value="Resolved" <?php print_field_checked('lumbar_result', 'Resolved')?>>Resolved</label>
                <label>
                  <input type="radio" name="lumbarresult" value="Improved" <?php print_field_checked('lumbar_result', 'Improved')?>>Improved</label>
                <label>
                  <input type="radio" name="lumbarrresult" value="Unchanged" <?php print_field_checked('lumbar_result', 'Unchanged')?>>Unchanged</label>
                <label>
                  <input type="radio" name="lumbarresult" value="Worsened" <?php print_field_checked('lumbar_result', 'Worsened')?>>Worsened</label>
              </div>
            </td>
          </tr>
          <tr>
            <td>Sacrum</td>
            <td>
              <textarea class="form-control" rows="1" name="sacrum"><?php print_field('sacrum_diagnosis')?></textarea>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="sacrumomm" value="Y" <?php print_field_checked('sacrum_omm')?>>Yes</label>
                <label>
                  <input type="radio" name="sacrumomm" value="N" <?php print_field_checked('sacrum_omm', 'N')?>>No</label>
              </div>
            </td>
            <td>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="sacrumtreatmentmethod[]" value="ME" <?php print_omm_treatment_checked('Sacrum', 'ME')?>>ME</label>
                <label>
                  <input type="checkbox" name="sacrumtreatmentmethod[]" value="MFR" <?php print_omm_treatment_checked('Sacrum', 'MFR')?>>MFR</label>
                <label>
                  <input type="checkbox" name="sacrumtreatmentmethod[]" value="HVLA" <?php print_omm_treatment_checked('Sacrum', 'HVLA')?>>HVLA</label>
                <label>
                  <input type="checkbox" name="sacrumtreatmentmethod[]" value="ART" <?php print_omm_treatment_checked('Sacrum', 'ART')?>>ART</label>
                <label>
                  <input type="checkbox" name="sacrumtreatmentmethod[]" value="ST" <?php print_omm_treatment_checked('Sacrum', 'ST')?>>ST</label>
                <label>
                  <input type="checkbox" name="sacrumtreatmentmethod[]" value="FPR" <?php print_omm_treatment_checked('Sacrum', 'FPR')?>>FPR</label>
                <label>
                  <input type="checkbox" name="sacrumtreatmentmethod[]" value="CS" <?php print_omm_treatment_checked('Sacrum', 'CS')?>>CS</label>
                <label>
                  <input type="checkbox" name="sacrumtreatmentmethod[]" value="BLT" <?php print_omm_treatment_checked('Sacrum', 'BLT')?>>BLT</label>
                <label>
                  <input type="checkbox" name="sacrumtreatmentmethod[]" value="OCMM" <?php print_omm_treatment_checked('Sacrum', 'OCMM')?>>OCMM</label>
              </div>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="sacrumresult" value="Resolved" <?php print_field_checked('sacrum_result', 'Resolved')?>>Resolved</label>
                <label>
                  <input type="radio" name="sacrumresult" value="Improved" <?php print_field_checked('sacrum_result', 'Improved')?>>Improved</label>
                <label>
                  <input type="radio" name="sacrumresult" value="Unchanged" <?php print_field_checked('sacrum_result', 'Unchanged')?>>Unchanged</label>
                <label>
                  <input type="radio" name="sacrumresult" value="Worsened" <?php print_field_checked('sacrum_result', 'Worsened')?>>Worsened</label>
              </div>
            </td>
          </tr>
          <tr>
            <td>Pelvis/Innominates</td>
            <td>
              <textarea class="form-control" rows="1" name="pelvis"><?php print_field('pelvis_diagnosis')?></textarea>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="pelvisomm" value="Y" <?php print_field_checked('pelvis_omm')?>>Yes</label>
                <label>
                  <input type="radio" name="pelvisomm" value="N" <?php print_field_checked('head_omm', 'N')?>>No</label>
              </div>
            </td>
            <td>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="pelvistreatmentmethod[]" value="ME"  <?php print_omm_treatment_checked('Pelvis/Innominates', 'ME')?>>ME</label>
                <label>
                  <input type="checkbox" name="pelvistreatmentmethod[]" value="MFR" <?php print_omm_treatment_checked('Pelvis/Innominates', 'MFR')?>>MFR</label>
                <label>
                  <input type="checkbox" name="pelvistreatmentmethod[]" value="HVLA" <?php print_omm_treatment_checked('Pelvis/Innominates', 'HVLA')?>>HVLA</label>
                <label>
                  <input type="checkbox" name="pelvistreatmentmethod[]" value="ART" <?php print_omm_treatment_checked('Pelvis/Innominates', 'ART')?>>ART</label>
                <label>
                  <input type="checkbox" name="pelvistreatmentmethod[]" value="ST" <?php print_omm_treatment_checked('Pelvis/Innominates', 'ST')?>>ST</label>
                <label>
                  <input type="checkbox" name="pelvistreatmentmethod[]" value="FPR" <?php print_omm_treatment_checked('Pelvis/Innominates', 'FPR')?>>FPR</label>
                <label>
                  <input type="checkbox" name="pelvistreatmentmethod[]" value="CS" <?php print_omm_treatment_checked('Pelvis/Innominates', 'CS')?>>CS</label>
                <label>
                  <input type="checkbox" name="pelvistreatmentmethod[]" value="BLT" <?php print_omm_treatment_checked('Pelvis/Innominates', 'BLT')?>>BLT</label>
                <label>
                  <input type="checkbox" name="pelvistreatmentmethod[]" value="OCMM" <?php print_omm_treatment_checked('Pelvis/Innominates', 'OCMM')?>>OCMM</label>
              </div>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="pelvisresult" value="Resolved" <?php print_field_checked('pelvis_result', 'Resolved')?>>Resolved</label>
                <label>
                  <input type="radio" name="pelvisresult" value="Improved" <?php print_field_checked('pelvis_result', 'Improved')?>>Improved</label>
                <label>
                  <input type="radio" name="pelvisresult" value="Unchanged" <?php print_field_checked('pelvis_result', 'Unchanged')?>>Unchanged</label>
                <label>
                  <input type="radio" name="pelvisresult" value="Worsened" <?php print_field_checked('pelvis_result', 'Worsened')?>>Worsened</label>
              </div>
            </td>
          </tr>
          <tr>
            <td>Ribcage</td>
            <td>
              <textarea class="form-control" rows="1" name="ribcage"><?php print_field('ribcage_diagnosis')?></textarea>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="ribcageomm" value="Y" <?php print_field_checked('ribcage_omm')?>>Yes</label>
                <label>
                  <input type="radio" name="ribcageomm" value="N"  <?php print_field_checked('ribcage_omm', 'N')?>>No</label>
              </div>
            </td>
            <td>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="ribcagetreatmentmethod[]" value="ME" <?php print_omm_treatment_checked('Ribcage', 'ME')?>>ME</label>
                <label>
                  <input type="checkbox" name="ribcagetreatmentmethod[]" value="MFR"  <?php print_omm_treatment_checked('Ribcage', 'MFR')?>>MFR</label>
                <label>
                  <input type="checkbox" name="ribcagetreatmentmethod[]" value="HVLA"  <?php print_omm_treatment_checked('Ribcage', 'HVLA')?>>HVLA</label>
                <label>
                  <input type="checkbox" name="ribcagetreatmentmethod[]" value="ART" <?php print_omm_treatment_checked('Ribcage', 'ART')?>>ART</label>
                <label>
                  <input type="checkbox" name="ribcagetreatmentmethod[]" value="ST"  <?php print_omm_treatment_checked('Ribcage', 'ST')?>>ST</label>
                <label>
                  <input type="checkbox" name="ribcagetreatmentmethod[]" value="FPR"  <?php print_omm_treatment_checked('Ribcage', 'FPR')?>>FPR</label>
                <label>
                  <input type="checkbox" name="ribcagetreatmentmethod[]" value="CS"  <?php print_omm_treatment_checked('Ribcage', 'CS')?>>CS</label>
                <label>
                  <input type="checkbox" name="ribcagetreatmentmethod[]" value="BLT"  <?php print_omm_treatment_checked('Ribcage', 'BLT')?>>BLT</label>
                <label>
                  <input type="checkbox" name="ribcagetreatmentmethod[]" value="OCMM"  <?php print_omm_treatment_checked('Ribcage', 'OCMM')?>>OCMM</label>
              </div>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="ribcageresult" value="Resolved" <?php print_field_checked('ribcage_result', 'Resolved')?> >Resolved</label>
                <label>
                  <input type="radio" name="ribcageresult" value="Improved" <?php print_field_checked('ribcage_result', 'Improved')?> >Improved</label>
                <label>
                  <input type="radio" name="ribcageresult" value="Unchanged" <?php print_field_checked('ribcage_result', 'Unchanged')?> >Unchanged</label>
                <label>
                  <input type="radio" name="ribcageresult" value="Worsened" <?php print_field_checked('ribcage_result', 'Worsened')?>>Worsened</label>
              </div>
            </td>
          </tr>

          <tr>
            <td>Abdomen</td>
            <td>
              <textarea class="form-control" rows="1" name="abdomen"><?php print_field('abdomen_diagnosis')?></textarea>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="abdomenomm" value="Y" <?php print_field_checked('abdomen_omm')?>>Yes</label>
                <label>
                  <input type="radio" name="abdomenomm" value="N" <?php print_field_checked('abdomen_omm', 'N')?>>No</label>
              </div>
            </td>
            <td>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="abdomentreatmentmethod[]" value="ME" <?php print_omm_treatment_checked('Abdomen', 'ME')?>>ME</label>
                <label>
                  <input type="checkbox" name="abdomentreatmentmethod[]" value="MFR" <?php print_omm_treatment_checked('Abdomen', 'MFR')?>>MFR</label>
                <label>
                  <input type="checkbox" name="abdomentreatmentmethod[]" value="HVLA" <?php print_omm_treatment_checked('Abdomen', 'HVLA')?>>HVLA</label>
                <label>
                  <input type="checkbox" name="abdomentreatmentmethod[]" value="ART" <?php print_omm_treatment_checked('Abdomen', 'ART')?>>ART</label>
                <label>
                  <input type="checkbox" name="abdomentreatmentmethod[]" value="ST" <?php print_omm_treatment_checked('Abdomen', 'ST')?>>ST</label>
                <label>
                  <input type="checkbox" name="abdomentreatmentmethod[]" value="FPR" <?php print_omm_treatment_checked('Abdomen', 'FPR')?>>FPR</label>
                <label>
                  <input type="checkbox" name="abdomentreatmentmethod[]" value="CS" <?php print_omm_treatment_checked('Abdomen', 'CS')?>>CS</label>
                <label>
                  <input type="checkbox" name="abdomentreatmentmethod[]" value="BLT" <?php print_omm_treatment_checked('Abdomen', 'BLT')?>>BLT</label>
                <label>
                  <input type="checkbox" name="abdomentreatmentmethod[]" value="OCMM" <?php print_omm_treatment_checked('Abdomen', 'OCMM')?>>OCMM</label>
              </div>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="abdomenresult" value="Resolved" <?php print_field_checked('abdomen_result', 'Resolved')?>>Resolved</label>
                <label>
                  <input type="radio" name="abdomenresult" value="Improved" <?php print_field_checked('abdomen_result', 'Improved')?>>Improved</label>
                <label>
                  <input type="radio" name="abdomentresult" value="Unchanged" <?php print_field_checked('abdomen_result', 'Unchanged')?>>Unchanged</label>
                <label>
                  <input type="radio" name="abdomenresult" value="Worsened" <?php print_field_checked('abdomen_result', 'Worsened')?>>Worsened</label>
              </div>
            </td>
          </tr>
          <tr>
            <td>Upper Extremity</td>
            <td>
              <textarea class="form-control" rows="1" name="upperextremity"><?php print_field('upper_diagnosis')?></textarea>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="upperextremityomm" value="Y" <?php print_field_checked('upper_omm')?>>Yes</label>
                <label>
                  <input type="radio" name="upperextremityomm" value="N" <?php print_field_checked('upper_omm', 'N')?>>No</label>
              </div>
            </td>
            <td>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="upperextremitytreatmentmethod[]" value="ME" <?php print_omm_treatment_checked('Upper Extremity', 'ME')?>>ME</label>
                <label>
                  <input type="checkbox" name="upperextremitytreatmentmethod[]" value="MFR" <?php print_omm_treatment_checked('Upper Extremity', 'MFR')?>>MFR</label>
                <label>
                  <input type="checkbox" name="upperextremitytreatmentmethod[]" value="HVLA" <?php print_omm_treatment_checked('Upper Extremity', 'HVLA')?>>HVLA</label>
                <label>
                  <input type="checkbox" name="upperextremitytreatmentmethod[]" value="ART" <?php print_omm_treatment_checked('Upper Extremity', 'ART')?>>ART</label>
                <label>
                  <input type="checkbox" name="upperextremitytreatmentmethod[]" value="ST" <?php print_omm_treatment_checked('Upper Extremity', 'ST')?>>ST</label>
                <label>
                  <input type="checkbox" name="upperextremitytreatmentmethod[]" value="FPR" <?php print_omm_treatment_checked('Upper Extremity', 'FPR')?>>FPR</label>
                <label>
                  <input type="checkbox" name="upperextremitytreatmentmethod[]" value="CS" <?php print_omm_treatment_checked('Upper Extremity', 'CS')?>>CS</label>
                <label>
                  <input type="checkbox" name="upperextremitytreatmentmethod[]" value="BLT" <?php print_omm_treatment_checked('Upper Extremity', 'BLT')?>>BLT</label>
                <label>
                  <input type="checkbox" name="upperextremitytreatmentmethod[]" value="OCMM" <?php print_omm_treatment_checked('Upper Extremity', 'OCMM')?>>OCMM</label>
              </div>
            </td>
            <td>
              <div class="radio">
                <label>
                  <input type="radio" name="upperextremityresult" value="Resolved" <?php print_field_checked('upper_result', 'Resolved')?>>Resolved</label>
                <label>
                  <input type="radio" name="upperextremityresult" value="Improved" <?php print_field_checked('upper_result', 'Improved')?>>Improved</label>
                <label>
                  <input type="radio" name="upperextremityresult" value="Unchanged" <?php print_field_checked('upper_result', 'Unchanged')?>>Unchanged</label>
                <label>
                  <input type="radio" name="upperextremityresult" value="Worsened" <?php print_field_checked('upper_result', 'Worsened')?>>Worsened</label>
              </div>
            </td>
            <tr>
              <td>Lower Extremity</td>
              <td>
                <textarea class="form-control" rows="1" name="lowerextremity"><?php print_field('lower_diagnosis')?></textarea>
              </td>
              <td>
                <div class="radio">
                  <label>
                    <input type="radio" name="lowerextremityomm" value="Y" <?php print_field_checked('lower_omm')?>>Yes</label>
                  <label>
                    <input type="radio" name="lowerextremityomm" value="N" <?php print_field_checked('head_omm', 'N')?>>No</label>
                </div>
              </td>
              <td>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="lowerextremitytreatmentmethod[]" value="ME" <?php print_omm_treatment_checked('Lower Extremity', 'ME')?> >ME</label>
                  <label>
                    <input type="checkbox" name="lowerextremitytreatmentmethod[]" value="MFR"  <?php print_omm_treatment_checked('Lower Extremity', 'MFR')?>>MFR</label>
                  <label>
                    <input type="checkbox" name="lowerextremitytreatmentmethod[]" value="HVLA"  <?php print_omm_treatment_checked('Lower Extremity', 'HVLA')?>>HVLA</label>
                  <label>
                    <input type="checkbox" name="lowerextremitytreatmentmethod[]" value="ART"  <?php print_omm_treatment_checked('Lower Extremity', 'ART')?>>ART</label>
                  <label>
                    <input type="checkbox" name="lowerextremitytreatmentmethod[]" value="ST" <?php print_omm_treatment_checked('Lower Extremity', 'ST')?>>ST</label>
                  <label>
                    <input type="checkbox" name="lowerextremitytreatmentmethod[]" value="FPR" <?php print_omm_treatment_checked('Lower Extremity', 'FPR')?>>FPR</label>
                  <label>
                    <input type="checkbox" name="lowerextremitytreatmentmethod[]" value="CS" <?php print_omm_treatment_checked('Lower Extremity', 'CS')?>>CS</label>
                  <label>
                    <input type="checkbox" name="lowerextremitytreatmentmethod[]" value="BLT" <?php print_omm_treatment_checked('Lower Extremity', 'BLT')?>>BLT</label>
                  <label>
                    <input type="checkbox" name="lowerextremitytreatmentmethod[]" value="OCMM" <?php print_omm_treatment_checked('Lower Extremity', 'OCMM')?>>OCMM</label>
                </div>
              </td>
              <td>
                <div class="radio">
                  <label>
                    <input type="radio" name="lowerextremityresult" value="Resolved" <?php print_field_checked('lower_result', 'Resolved')?>>Resolved</label>
                  <label>
                    <input type="radio" name="lowerextremityresult" value="Improved" <?php print_field_checked('lower_result', 'Improved')?>>Improved</label>
                  <label>
                    <input type="radio" name="lowerextremityresult" value="Unchanged" <?php print_field_checked('lower_result', 'Unchanged')?>>Unchanged</label>
                  <label>
                    <input type="radio" name="lowerextremityresult" value="Worsened" <?php print_field_checked('lower_result', 'Worsened')?>>Worsened</label>
                </div>
              </td>
            </tr>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

<!-- Notes Tab -->
  <div id="notes" class="tab-pane fade">
    <div class="form-group" align="center">
      <label for="notes">NOTES:</label>
      <textarea class="form-control" rows="5" name="notes"><?php print_field('notes')?></textarea>
    </div>

    <div class="form-group" align="center">
      <label for="assessment">ASSESSMENT:</label>
      <textarea class="form-control" rows="5" name="assessment"><?php print_field('assessment')?></textarea>
    </div>

    <div class="form-group" align="center">
      <label for="plan">PLAN:</label>
      <textarea class="form-control" rows="5" name="plan"><?php print_field('plan')?></textarea>
    </div>

    <?php
    if (get_user_role($_SESSION['id']) == "MEDSTUDENT") { // show student signature
      ?>
      <div class="form-group" align="center">
        <label for="primary">Primary Student Doctor Signature:</label>
        <input type="text" class="form-control" name="student_signature" style="width: 500px;">
      </div>
      <div align="center">
        Date:
        <input type="text" name="primarydate" value="<?php echo date('m/d/Y', time()) ?>">
      </div>
      <?php
    } else { //show physician signature
      ?>

      <div class="form-group" align="center">
        <label for="physician">Physician's Signature:</label>
        <input type="text" class="form-control" name="physician_signature" style="width: 500px;" required>
      </div>

      <div align="center">
        Date:
        <input type="text" name="physiciandate" value="<?php echo date('m/d/Y', time()) ?>">
      </div>
      <?php
    } // end else
    ?>

    <div style="text-align:center">
      <input type="submit" class="btn btn-info" value="Submit">
    </div>
  </div>
  </form>
</div>
</body>

</html>
