<link href="../css/index-logged-out.css" rel="stylesheet">
<link href="../css/login.css" rel="stylesheet">

<header id="top" class="header">
    <!-- JQuery import -->
    <script src="../scripts/js/jquery.js"></script>
    <!-- Bootstrap CSS import -->
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"</link>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"</link>

<link href="css/index-logged-out.css" rel="stylesheet">
<link href="css/login.css" rel="stylesheet">

<header id="top" class="header">
    <!-- JQuery import -->
    <script src="scripts/js/jquery.js"></script>
    <!-- Bootstrap CSS import -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"> </link>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> </link>

    <div class="text-vertical-center">
        <div class="container">
            <h1><strong>Patient Account Creation</strong></h1>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <p>Enter your first name, last name, date of birth, and a password to create an account:</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-lock"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" method="post" class="login-form"  action="core/database/create-patient-account.php">
                            <div class="form-group">
                                <label class="sr-only" for="form-firstname">First name: </label>
                                <input type="text" name="firstname" placeholder="First name..." class="form-firstname form-control" id="form-firstname">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-lastname">Last name: </label>
                                <input type="text" name="lastname" placeholder="Last name..." class="form-lastname form-control" id="form-lastname">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-dob">Date of Birth: </label>
                                <input type="text" name="dateofbirth" placeholder="Date of birth..." class="form-dateofbirth form-control" id="form-dateofbirth">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-dob">Password: </label>
                                <input type="password" name="password" placeholder="Password..." class="form-dateofbirth form-control" id="form-password">
                            </div>
                            <button type="submit" class="btn btn-dark btn-lg">Create Account!</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>