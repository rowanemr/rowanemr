<?php
  // This stuff is to fill in the fields we already know
  // If the user visits the clinic again, we want to auto-fill these fields out.

  if (!isset($_GET['token']) or !is_valid_token($_GET['token']))
      die("Valid token required.");

  $token = $_GET['token'];
  $patient_id = get_patient_id_from_token($token);
  // An associate array of fields filled out. Used to echo out values later.
  $fields_filled_out = get_patient_info($patient_id); // Store first_name, last_name, date_of_birth in this assoc. array

  // Now check to see if we have a previous Demographics input filled out
  // There will only be ONE row in the database. If the patient comes again, they will UPDATE the row.

    global $db;
    $result = $db->query("SELECT preferred_name, gender, daily_work_performed, completed_education, has_history_tobacco, alcohol_usage, ethnicity FROM PatientDemographics WHERE patient_id = '$patient_id'");
    if ($result->num_rows !== 0)
    {
        $row = $result->fetch_assoc();
        $fields_filled_out = array_merge($fields_filled_out, $row); //concatenate fields together
    }

    // Called during in-line HTML statements to print stuff out
    function print_field($field_name)
    {
        global $fields_filled_out;

        // This isset if is required because if it's the first time a patient is filling out this form
        // Then the key in this $fields_filled_out associative array will not exist
        if (isset($fields_filled_out[$field_name]))
            echo $fields_filled_out[$field_name];
    }

    // Will echo 'checked' if the field name is the field value
    // For example print_field_checked('gender', 'male') would output checked for the checkbox if it's true
    function print_field_checked($field_name, $field_value)
    {
        global $fields_filled_out;
        // This isset if is required because if it's the first time a patient is filling out this form
        // Then the key in this $fields_filled_out associative array will not exist
        if (isset($fields_filled_out[$field_name]))
            if ($fields_filled_out[$field_name] == $field_value)
                echo " checked ";

    }

?>
<!-- Needed for jQUERY CALENDAR UI widget -->
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <h1 align="center">Intake Form</h1>

    <form action="core/database/add-edit-demographic-form.php?token=<?php echo $token ?>" method="post">
        <!-- Name and Today's Date input -->
        <div class="form-group" align="center">
           <label for="first-name">First Name</label>
           <input type="text" class="form-control" id="first-name" name="first-name" style="width: 500px;" value="<?php print_field('first_name') ?>" required>
           <label for="last-name">Last Name</label>
           <input type="text" class="form-control" id="last-name" name="last-name" style="width: 500px;" value="<?php print_field('last_name') ?>" required>
           <label for="preferred-name">Preferred Name</label>
           <input type="text" class="form-control" id="preferred-name" name="preferred-name" style="width: 500px;" value="<?php print_field('preferred_name')?>">
           <label for="today-date">Today's Date</label>
            <!-- Note today-date is filled in with JavaScript at end of this file -->
            <input type="text" class="form-control" id="today-date" name="today-date" style="width: 150px;" required>
            <script>
                $(function() {
                    $( "#today-date" ).datepicker({
                        dateFormat: "mm/dd/yy",
                        defaultDate: 0,
                        setDate: new Date(),
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#today-date").datepicker('setDate', new Date());
                });
            </script>
        </div>

       <!-- Demographic info section -->
       <h1 align="center">Demographic Information</h1>

       <!-- Date of Birth input -->
       <div class="form-group" align="center">
        <label for="birth-date">Date of Birth</label>
        <input type="text" class="form-control" id="birth-date" name="birth-date" style="width: 150px;" required>
           <script>
               $(function() {
                   $( "#birth-date" ).datepicker({
                       dateFormat: "mm/dd/yy",
                       changeMonth: true,
                       changeYear: true
                   });
                   // Set birth date
                   // Note the date is echoed from PHP
                   $("#birth-date").datepicker('setDate', "<?php print_field('date_of_birth') ?>");
               });
           </script>
    </div>


    <!-- Gender input -->
    <div class="form-group" align="center">
        <label class="control-label">Gender</label>

        <label class="radio-inline">
            <input type="radio" name="gender-options" id="male" value="male" required <?php print_field_checked('gender', 'male') ?>> Male
        </label>
        <label class="radio-inline">
            <input type="radio" name="gender-options" id="female" value="female" required <?php print_field_checked('gender', 'female') ?>> Female
        </label>
        <label class="radio-inline">
            <input type="radio" name="gender-options" id="other" value="other" required <?php print_field_checked('gender', 'other') ?>> Other

        </label>
    </div>
    <br>


    <!-- Daily work performed -->
    <div class="form-group" align="center">
        <label for="today-date">Daily Work Performed:</label>
        <input type="text" class="form-control" name="daily-work" id="daily-work" placeholder="Type of daily work performed" style="width: 500px;" value="<?php print_field('daily_work_performed') ?> ">
        <br>
    </div>


    <!-- Completed education input -->
    <div class="form-group" align="center">
        <label class="control-label">Completed Education</label>

        <label class="radio-inline">
            <input type="radio" name="education-options" id="none" value="NO EDUCATION" <?php print_field_checked('completed_education', 'NO EDUCATION') ?>> None
        </label>
        <label class="radio-inline">
            <input type="radio" name="education-options" id="high-school" value="HIGH SCHOOL" <?php print_field_checked('completed_education', 'HIGH SCHOOL') ?> > High School
        </label>
        <label class="radio-inline">
            <input type="radio" name="education-options" id="college" value="COLLEGE" <?php print_field_checked('completed_education', 'COLLEGE') ?>> College
        </label>
        <label class="radio-inline">
            <input type="radio" name="education-options" id="graduate" value="GRADUATE" <?php print_field_checked('completed_education', 'GRADUATE') ?>> Graduate/Profession School
        </label>
    </div>


    <!-- Tobacco Use input -->
    <div class="form-group" align="center">
        <label class="control-label">History of Tobacco Use</label>

        <label class="radio-inline">
            <input type="radio" name="tobacco-options" id="yes-tobacco" value="Y" <?php print_field_checked('has_history_tobacco', 'Y') ?>> Yes
        </label>
        <label class="radio-inline">
            <input type="radio" name="tobacco-options" id="no-tobacco" value="N" <?php print_field_checked('has_history_tobacco', 'N') ?>> No
        </label>
    </div>


    <!-- Alcohol Use input -->
    <div class="form-group" align="center">
        <label class="control-label">Alcohol Usage</label>

        <label class="radio-inline">
            <input type="radio" name="alcohol-options" id="none-alcohol" value="NONE" <?php print_field_checked('alcohol_usage', 'NONE') ?>> None
        </label>
        <label class="radio-inline">
            <input type="radio" name="alcohol-options" id="rarely-alohol" value="RARELY" <?php print_field_checked('alcohol_usage', 'RARELY') ?>> Rarely
        </label>
        <label class="radio-inline">
            <input type="radio" name="alcohol-options" id="socially-alcohol" value="SOCIALLY" <?php print_field_checked('alcohol_usage', 'SOCIALLY') ?>> Socially
        </label>
        <label class="radio-inline">
            <input type="radio" name="alcohol-options" id="daily-alcohol" value="DAILY" <?php print_field_checked('alcohol_usage', 'DAILY') ?>> Daily
        </label>
    </div>


    <!-- Ethnicity input -->
    <div class="form-group" align="center">
        <label class="control-label">Ethnicity (Optional)</label>
    </div>
    <div class="form-group" align="center">
        <label class="radio-inline">
            <input type="radio" name="ethnicity-options" id="caucasian" value="CAUCASIAN" <?php print_field_checked('ethnicity', 'CAUCASIAN') ?> > Caucasian
        </label>
        <label class="radio-inline">
            <input type="radio" name="ethnicity-options" id="native-american" value="NATIVE AMERICAN" <?php print_field_checked('ethnicity', 'NATIVE AMERICAN') ?> > American Indian / Alaska Native
        </label>
        <label class="radio-inline">
            <input type="radio" name="ethnicity-options" id="black" value="BLACK" <?php print_field_checked('ethnicity', 'BLACK') ?> > Black / African American
        </label>
        <label class="radio-inline">
            <input type="radio" name="ethnicity-options" id="latino" value="LATINO" <?php print_field_checked('ethnicity', 'LATINO') ?> > Hispanic / Latino
        </label>
        <label class="radio-inline">
            <input type="radio" name="ethnicity-options" id="asian" value="ASIAN" <?php print_field_checked('ethnicity', 'ASIAN') ?> > Asian / Pacific Islander
        </label>
    </div>

    <!-- Submit button -->
    <br>
    <div style="text-align:center">
        <button type="submit" class="btn btn-info">Submit</button>
    </div>

</form> <!-- End form -->

<br>
<br>
<br>
<br>


