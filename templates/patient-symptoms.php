<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>
 
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
 
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   <div class="jumbotron">
  <h1>Patient Symptoms Intake Form</h1>
  <p>Have you suffered from any of the following in the last two weeks? Select all that apply.</p>
</div>

<!-- Symptom Intake Form Checklist. -->
<br><label class="checkbox-inline"><input type="checkbox" value="">Weight Loss</label>
<label class="checkbox-inline"><input type="checkbox" value="">Sore Throat</label>
<label class="checkbox-inline"><input type="checkbox" value="">Joint Pain</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Weight Gain</label>
<label class="checkbox-inline"><input type="checkbox" value="">Hoarse Voice</label>
<label class="checkbox-inline"><input type="checkbox" value="">Joint Swelling</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Tiredness</label>
<label class="checkbox-inline"><input type="checkbox" value="">Swollen Glands</label>
<label class="checkbox-inline"><input type="checkbox" value="">Osteoporosis</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Fever</label>
<label class="checkbox-inline"><input type="checkbox" value="">Chest Pains</label>
<label class="checkbox-inline"><input type="checkbox" value="">Seizures</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Chills</label>
<label class="checkbox-inline"><input type="checkbox" value="">Heart Palpitations</label>
<label class="checkbox-inline"><input type="checkbox" value="">Fainting</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Rashes</label>
<label class="checkbox-inline"><input type="checkbox" value="">High Blood PRessure</label>
<label class="checkbox-inline"><input type="checkbox" value="">Dizziness</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Sores</label>
<label class="checkbox-inline"><input type="checkbox" value="">Swelling of arms/legs</label>
<label class="checkbox-inline"><input type="checkbox" value="">Headache</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Fractures</label>
<label class="checkbox-inline"><input type="checkbox" value="">Cough</label>
<label class="checkbox-inline"><input type="checkbox" value="">Anemia</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Surgeries</label>
<label class="checkbox-inline"><input type="checkbox" value="">Coughing Up Mucus</label>
<label class="checkbox-inline"><input type="checkbox" value="">Heat/Cold Intolerance</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Blurred Vision</label>
<label class="checkbox-inline"><input type="checkbox" value="">Coughing Up Blood</label>
<label class="checkbox-inline"><input type="checkbox" value="">Excessive Thirst</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Teary Eyes</label>
<label class="checkbox-inline"><input type="checkbox" value="">Wheezing</label>
<label class="checkbox-inline"><input type="checkbox" value="">Anxiety</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Glaucoma</label>
<label class="checkbox-inline"><input type="checkbox" value="">Shortness of Breath</label>
<label class="checkbox-inline"><input type="checkbox" value="">Depression</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Cataracts</label>
<label class="checkbox-inline"><input type="checkbox" value="">Problems Swallowing</label>
<label class="checkbox-inline"><input type="checkbox" value="">Memory Loss</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Decreased Hearing</label>
<label class="checkbox-inline"><input type="checkbox" value="">Nausea</label>
<label class="checkbox-inline"><input type="checkbox" value="">Ringing of the Ears</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Vomitting</label>
<label class="checkbox-inline"><input type="checkbox" value="">Vertigo</label>
<label class="checkbox-inline"><input type="checkbox" value="">Constipation</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Congestion</label>
<label class="checkbox-inline"><input type="checkbox" value="">Diarrhea</label>
<label class="checkbox-inline"><input type="checkbox" value="">Running Nose</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Frequent Urination</label>
<label class="checkbox-inline"><input type="checkbox" value="">Nose Bleeds</label>
<label class="checkbox-inline"><input type="checkbox" value="">Unable to Urinate</label>
<br>
<br>
<label class="checkbox-inline"><input type="checkbox" value="">Bleeding Gums</label>
<label class="checkbox-inline"><input type="checkbox" value="">Pain from walking</label>
<br>
<br>
<input type="submit" value="Submit">
</form>

 
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
