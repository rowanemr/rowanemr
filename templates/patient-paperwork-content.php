<?php
/**
 * The entrypoint for patient intake paperwork (where the patient inserts information into the iPad).
 */

// Note isset is a language construct, not a function, which is why it won't error if token isn't an index $_GET[]
// This is one of many many bad design choices PHP makes
$token = isset($_GET['token']) ? $_GET['token'] : '';
$current_file_name = basename($_SERVER['PHP_SELF']);

if ($token == '') {
    no_token_provided();
}
elseif (!is_valid_token($token)) {
    invalid_token();
}


function invalid_token()
{
    $current_file_name = basename($_SERVER['PHP_SELF']);
    echo "<center><h1>Invalid Token</h1>Token is invalid. ";
    die("<a href = '$current_file_name'>Please click here to re-enter it.</a>");
}


function no_token_provided()
{
    $current_file_name = basename($_SERVER['PHP_SELF']);
    // Echo using heredoc syntax. Echo's everything between the EOD keywords
    echo <<< EOD

<style>body {
        font-size: 20px;
    input { }
    </style>
<center><h1>Token Entry</h1>
<body>
<strong>Please enter in the token provided to you to start entering in Office paperwork:<br><br></strong>
<form action="$current_file_name" method="get">
    <label for="token">Token: </label>
    <input type="text" name="token" id="token" size="5" maxlength="5" required>
    <input class="btn btn-large btn-success" type="submit">
</center></form></body>
EOD;

}

?>