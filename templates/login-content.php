<link href="css/index-logged-out.css" rel="stylesheet">
<link href="css/login.css" rel="stylesheet">

<header id="top" class="header">

    <div class="text-vertical-center">
        <div class="container">
            <h1><strong>RowanEMR</strong> Login</h1>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <p>Enter your username and password to log on:</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-lock"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" method="post" class="login-form"  action="core/database/login.php">
                            <div class="form-group">
                                <label class="sr-only" for="form-username">Username</label>
                                <input type="text" name="username" placeholder="Username..." class="form-username form-control" id="form-username">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-password">Password</label>
                                <input type="password" name="password" placeholder="Password..." class="form-password form-control" id="form-password">
                            </div>
                            <button type="submit" class="btn btn-dark btn-lg">Sign in!</button>
                        </form>
                        <a href="templates/patient-login-content.php">Are you a patient? Please click here...</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>