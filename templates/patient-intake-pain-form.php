<?php
  if (!isset($_GET['token']) or !is_valid_token($_GET['token']))
	  die("Valid token required");
  $token = $_GET['token'];
?>

<head>
	<title>Pain Form </title>
<!-- http://jsfiddle.net/K6Xnk/ -->
<script>
	$( document ).ready(function() {
		$("#body-img").click(function (ev) {
			mouseX = ev.pageX;
			mouseY = ev.pageY;
			var color = '#FF0000';
			var size = '12px';
			$("body").append(
					$('<div></div>')
							.css('position', 'absolute')
							.css('top', mouseY + 'px')
							.css('left', mouseX + 'px')
							.css('width', size)
							.css('height', size)
							.css('background-color', color)
			);
		});
	});

</script>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>

</head>

<body>
	<form action="submit_pain_form.php?token=<?php echo $token ?>" method="post">
	<h1 align="center">PAIN FORM</h1>

	<div class="current-pain" style="text-align:center">

		<label for="current-pain">Currently experiencing pain?</label>
		<div class="radio" style="text-align:center">
			<label>
				<input type="radio" name="has_pain" value="Yes">Yes</label>
		</div>
		<div class="radio" style="text-align:center">
			<label>
				<input type="radio" name="has_pain" value="No">No</label>
		</div>

	</div>

	<div class="date" style="text-align:center">

		<label for="date-starting-pain">Date of Starting Pain</label>

		<select name="month">
			<option>- Month -</option>
			<option value="1">January</option>
			<option value="2">February</option>
			<option value="3">March</option>
			<option value="4">April</option>
			<option value="5">May</option>
			<option value="6">June</option>
			<option value="7">July</option>
			<option value="8">August</option>
			<option value="9">September</option>
			<option value="10">October</option>
			<option value="11">November</option>
			<option value="12">December</option>
		</select>

		<select name="day">
			<option> - Day - </option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
			<option value="9">9</option>
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12">12</option>
			<option value="13">13</option>
			<option value="14">14</option>
			<option value="15">15</option>
			<option value="16">16</option>
			<option value="17">17</option>
			<option value="18">18</option>
			<option value="19">19</option>
			<option value="20">20</option>
			<option value="21">21</option>
			<option value="22">22</option>
			<option value="23">23</option>
			<option value="24">24</option>
			<option value="25">25</option>
			<option value="26">26</option>
			<option value="27">27</option>
			<option value="28">28</option>
			<option value="29">29</option>
			<option value="30">30</option>
			<option value="31">31</option>
		</select>

		<input type="text" class="input-block-level" placeholder="Year" name="year" id="year" maxlength="4">
		<script>document.getElementById('year').value = new Date().getFullYear(); // fill in this year</script>
	</div>

	<div class="activity-during-onset" align="center">
		<label for="activity">Activity during onset of pain:</label>
		<input type="text" class="form-control" name="activity" id="activity" style="width: 500px;">
	</div>

	<table align="center">
		<tr>
			<td style="padding:0 25px 0 25px;">
				<div class="pain-right-now">
					<label for="pain_right_now">Pain right now:</label>
					<input type="range" name="pain_right_now" id="pain-right-now" value="5" min="0" max="10">
				</div>
			</td>
			<td style="padding:0 25px 0 25px;">
				<div class="pain-at-worst">
					<label for="pain_at_worst">Pain at its worst:</label>
					<input type="range" name="pain_at_worst" id="pain-at-worst" value="5" min="0" max="10">
				</div>
			</td>
		</tr>
	</table>

	<table align="center">
		<tr>
			<td  style="padding:0 25px 0 25px;">
				<div class="pain-at-best" style="text-align:center">
					<label for="pain_at_best">Pain at its best:</label>
					<input type="range" name="pain_at_best" id="pain-at-best" value="5" min="0" max="10">
				</div>
			</td>
			<td  style="padding:0 25px 0 25px;">
				<div class="pain-on-average" style="text-align:center">
					<label for="pain_on_average">Pain on average:</label>
					<input type="range" name="pain_on_average" id="pain-on-average" value="5" min="0" max="10">
				</div>
			</td>
		</tr>
	</table>
	<div class="makes-pain-worse" align="center">
		<label for="pain-worse">What makes the pain worse? </label>
		<input type="text" class="form-control" name="makes_pain_worse" id="pain-worse" style="width: 500px;">
	</div>

	<div class="makes-pain-better" align="center">
		<label for="pain-better">What makes the pain better? </label>
		<input type="text" class="form-control" name="makes_pain_better" id="pain-better" style="width: 500px;">

	</div>

		<div style="text-align:center">
			<h2>Please mark the location of your pain on the figure below</h2>

			<div id="body-container">
				<canvas id="body-canvas" width="618" height="515"></canvas>
				<script>
				var canvas = document.getElementById('body-canvas');
				var context = canvas.getContext('2d');
				var imageObj = new Image();

				canvas.onclick = function (e)
				{
					var x = e.pageX - this.offsetLeft;
					var y = e.pageY - this.offsetTop;
					context.fillStyle = 'red';
					context.fillRect(x, y, 20, 20);
					// Add a hidden input field to send these points to the backend
					$('<input>').attr({
						type: 'hidden',
						name: 'coords[]',
						value: x + ' ' + y // space separate these points
					}).appendTo('form');
				};

				imageObj.onload = function() {
					context.drawImage(imageObj, 0, 0);
				};
				imageObj.src = '/res/image/body.jpeg';
				</script>
			</div>
		</div>

	<div style="text-align:center">
		<input type="submit" class="btn btn-info" value="Submit">
	</div>
	</form>
</body>


</html>
