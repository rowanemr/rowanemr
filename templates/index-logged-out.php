
<!-- Custom CSS -->
<link href="css/index-logged-out.css" rel="stylesheet">
<link href="css/login.css" rel="stylesheet">

<!-- Header -->
<header id="top" class="header">
    <div class="text-vertical-center">
        <h1>Rowan Clinic</h1>
        <h3>Electronic Medical Record Sytem</h3>
        <br>
        <a href="login.php" class="btn btn-dark btn-lg">Login Here</a>
    </div>
</header>



