<?php
require_once dirname(__DIR__) . '/core/init.php';
$results = get_user_name($_SESSION['id']);
$row = $results->fetch_assoc();
?>

<!-- Sets a scroll view for the table -->
<link href="css/table-style.css" rel="stylesheet">

<!-- Needed for datepicker widget -->
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
<script src="scripts/js/moment.js"></script>
<script src="scripts/js/bootstrap-datetimepicker.js"></script>

<!-- Needed for jQUERY CALENDAR UI widget (not boostrap one) -->
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<link href="css/index-logged-out.css" rel="stylesheet">
<link href="css/login.css" rel="stylesheet">
<link href="css/chat.css" rel="stylesheet">

<style>
    button.btn
    {
        margin-right: 5px;
    }
</style>

<!-- ADD Modal: Pop up dialog for adding a new patient. Created when add button is clicked -->
<div class="modal fade" id="add-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="add-modal-label">Add an Appointment to this Time Slot</h4>
            </div>

            <form id="new-appointment-form" method="post">
                <div class="modal-body">
                    <div id = "timeslot-to-show">PLACEHOLDER</div>
                    <label for="patient-first-name">Patient's First Name</label>
                    <input class="form-control" name="patient-first-name" id="patient-first-name" type="text" placeholder="Name" required />
                    <label for="patient-last-name">Patient's Last Name</label>
                    <input class="form-control" name="patient-last-name" id="patient-last-name" type="text" placeholder="Name" required />
                    <label for="birth-date">Patient's Date of Birth</label>
                    <input class="form-control" name="birth-date" id="birth-date" type="text" placeholder="Date of Birth" required/>
                    <script>
                        $(function () {
                            $("#birth-date").datepicker({
                                dateFormat: "mm/dd/yy",
                                yearRange: "-100:+0", // this is the option you're looking for
                                setDate: new Date(),
                                changeMonth: true,
                                changeYear: true
                            });
                        });
                    </script>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="add-submit" class="btn btn-info" data-dismiss="modal" >Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>


<!-- DELETE Modal: Pop up dialog for deleting a new patient. Created when a patient name button is clicked -->
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="edit-modal-label">Would you like to delete this appointment?</h4>
                <div id="edit-modal-body">PLACEHOLDER</div>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="delete-btn" class="btn btn-danger">Delete</button>
            </div>

        </div>
    </div>
</div>



<!-- Title -->
<h3 align="center">Schedule Daily View</h3>


<!-- Bootstrap Datepicker widget from eonasdan.github page (MIT license) -->
<div class="container">
    <div class="row">
        <div class='col-xs-4'></div>
        <div class='col-xs-4'>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' id="schedule-date" name="schedule-date" class="form-control" readonly/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <!-- Search Button -->
        <div class='col-xs-4'>
            <button class="btn btn-info btn-lg" id="look-up">Look Up</button>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                    defaultDate: Date.now(), //today's date as default
                    format: 'MM/DD/YYYY',
                    ignoreReadonly: true
                });
            });
        </script>
    </div>
</div>


<!-- Schedule Daily View Table -->
<div class="container" id="table-scroll">
    <table class="table table-bordered">
        <!-- top row of table -->
        <thead>
            <tr>
                <th class="start-column" style="text-align:center">Appointment Start Time</th>
                <th style="text-align:center">Patient Name</th>
            </tr>
        </thead>

        <!-- Body of table -->
        <tbody>
            <?php
            //Let us dynamically generate time slots 7:00AM - end time
            // PHP will echo these out directly to the web browser
            $start_time = strtotime("8:00");
            $end = 46800; //seconds from 8AM - 9PM = 46,800 seconds
            $step = 15 * 60; //15 minute steps in seconds
            $mins = range(0, $end, $step); //5 minute steps

            $time = "7:45AM"; //just to test
            // just to test
            /* echo "<tr>\n<td>$time</td>\n<td id=\"$time\">
              <button class=\"btn btn-info\" type=\"button\" data-toggle=\"modal\" data-target=\"#delete-modal\" data-firstname=\"Robert\" data-lastname=\"Herman\" data-patientId=\"1\">Robert Herman</button>
              <button class=\"btn btn-success pull-right\" type=\"button\" data-toggle=\"modal\" data-target=\"#add-modal\" data-time=\"$time\">
              <span class=\"glyphicon glyphicon-plus\"></span>
              </button></td></tr>";
             */

            foreach ($mins as $min)
            {
                $time = date('h:iA', $start_time + $min); //h = hour in 12 hour format, i for minutes, A for AM/PM
                echo "<tr>\n<td>$time</td>\n<td id=\"$time\"><a class=\"btn btn-lg btn-success pull-right\" type=\"button\" data-toggle=\"modal\" data-target=\"#add-modal\" data-time=\"$time\"><span class=\"glyphicon glyphicon-plus\"></span></a></td>\n</tr>\n\n";
            }
            ?>


            <!-- Example
        <tr>
          <td>8:00AM</td>
          <td> 
           <button class="btn btn-info" type="button" data-toggle="modal" data-target="#delete-modal" data-personName="Robert Herman">Robert Herman</button>
           <button class="btn btn-info">William Gates</button> 
           <button class="btn btn-success pull-right" type="button" data-toggle="modal" data-target="#add-modal">
            <span class="glyphicon glyphicon-plus"></span>
          </button>
        </td>
        </tr>
            -->

        </tbody>

    </table>
</div>

<br>
<br>
<br>
<br>

<div class="chat-box">
    <input type="checkbox" />
    <label data-expanded="Close Chatbox" data-collapsed="Rowan EMR Chat"></label>
    <div id="prefetch" class="text-center">
        <input class="no-clear typeahead formcontrol" id="typeahead" type="text" placeholder="Search..." style="width: 150px; height 15px">
    </div>
    <!--        <div>
                <select id='select'>
                    <option value="default">--SELECT A USER--</option>
                    <option value="group">Rowan EMR Group Chat</option>
    <?php
    $list = get_user_list();
    if ($list)
    {
        $i = 0;
        while ($item = $list->fetch_assoc())
        {
            echo '<option id=user', $i, ' value="', $item['name'], '">', $item['name'], '</option>';
            ++$i;
        }
    }
    ?>
                </select>
            </div>-->
    <div class="chat-box-content">
        <div id="box" style="height:120px; width:170px; border:1px; overflow:auto;"></div>

        <input type="text" id="input" name="text_name" placeholder="Type..." style="width: 150px; height 15px;"/>

        <input id="chat" type="submit" value="Send">

        <div id="serverRes"></div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://cdn.pubnub.com/pubnub-3.7.18.min.js"></script>
<script src="scripts/js/typeahead.js"></script>
<script src="scripts/js/search-user.js"></script>

<script>
       var username = "<?php echo $row['name'] ?>";
       var pubnub = PUBNUB.init({
           subscribe_key: 'sub-c-a2afaf94-ae42-11e5-9510-02ee2ddab7fe',
           publish_key: 'pub-c-e2695edd-441b-4aaf-872f-fac79f6a1b12'
       });
       var input = document.getElementById('input').value;
       var channel;
       var id;
       var chan;
       var box;

       document.getElementById('chat').addEventListener("click", function () {
           publish();
       });

       document.getElementById('typeahead').addEventListener("change", function () {
           pubnub.unsubscribe({
               channel: channel
           });
           document.getElementById('box').innerHTML = "";
           privateChat();
           subscribe();
       });

       function privateChat() {
           id = pubnub.uuid;
           chan = 'rowanemr-' + id;
           box = pubnub.$('box'), input = pubnub.$('input'), channel = 'private';
       }

//    function groupChat() {
//        box = pubnub.$('box'), input = pubnub.$('input'), channel = 'rowanemr_channel';
//    }

       function publish() {
           pubnub.publish({
               channel: channel,
               message: {
                   text: input.value,
                   uuid: username
               }
           });
       }

       function subscribe() {
           console.log("Subscribing...");
           pubnub.subscribe({
               channel: channel,
               message: function (data) {
                   box.innerHTML = ('' + data.uuid + ": " + data.text).replace(/[<>]/g, '') + '<br>' + box.innerHTML;
                   input.value = '';
               },
               connect: pub
           });

           function pub() {
               console.log("Publishing...");
               pubnub.publish({
                   channel: channel,
                   message: {
                       text: " entered the RowanEMR chat",
                       uuid: username
                   },
                   callback: function (m) {
                       console.log(m);
                   }
               });
           }
       }
</script>

<script>
    $('#delete-modal').on('show.bs.modal', function (e) {
        /* http://stackoverflow.com/questions/24323895/send-parameter-to-bootstrap-modal-window
         * Basically, we can add a data-yourparameter attribute to a Bootstrap HTML element
         * And it will get passed along. This is easy for PHP to do.
         * Example:
         * <button class="btn btn-info" type="button" data-toggle="modal" data-target="#delete-modal"
         * data-personName="Robert Herman" data-patientId="1">Robert Herman</button>
         * PHP can dynamically generate the data-personName and data-patientId attributes
         */
        var firstname = e.relatedTarget.dataset.firstname;
        var lastname = e.relatedTarget.dataset.lastname;
        var patientid = e.relatedTarget.dataset.patientid;
        var slotid = e.relatedTarget.dataset.slotid;
        $('#edit-modal-body').text("Patient Name: " + firstname + " " + lastname);
        //alert("DELETE PATIENT " + personname + "WITH ID OF " + patientid);
        var button_to_remove = $(e.relatedTarget); //the button we want to remove
        $('#delete-btn').click(function (e) {
            // Dynamically on every popup change what the delete button does
            button_to_remove.remove();
            $('#delete-modal').modal('hide');
            deleteAppointmentSlot(slotid);
        });
    });

    /*
     * Delete Appointment with slotid (slot_id in Schedule table)
     */
    function deleteAppointmentSlot(slot_id)
    {
        var request = $.ajax({
            url: "scheduling-ajax.php",
            type: "POST",
            data: {
                "action": "DELETE_APPOINTMENT",
                "slot_id": slot_id
            },
            success: function (response) {
                if (response == "OK")
                    console.log("DELETING " + slot_id + " A SUCCESS");
                else
                    alert("Error in deletion: " + response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Error " + textStatus); //throw some error
            }
        });
    }

    /**
     * Called when the add-modal dialog is shown
     */
    $('#add-modal').on('show.bs.modal', function (e) {
        var timeslot = e.relatedTarget.dataset.time;
        $('#timeslot-to-show').text(timeslot);
    });

    $('#add-submit').click(function (e) {
        e.preventDefault();
        var first_name = $('#patient-first-name').val();
        var last_name = $('#patient-last-name').val();
        var patient_name = first_name + " " + last_name;
        var birth_day = $('#birth-date').val();
        var timeslot = $("#timeslot-to-show").text().trim();
        /* Something like 8:00AM which is also the ID of the <td> we want to add to */
        var date = document.getElementById("schedule-date").value; //get value like 12/02/2015
        // Clear the fields for future use
        $('#patient-first-name').val('');
        $('#patient-last-name').val('');
        $('#birth-date').val('');
        //alert("SUBMIT " + patient_name + " " + birth_day + " " + timeslot);
        // Perform AJAX call
        var request = $.ajax({
            url: "scheduling-ajax.php",
            type: "POST",
            data: {
                "action": "NEW_APPOINTMENT",
                "first_name": first_name,
                "last_name": last_name,
                "birth_day": birth_day,
                "timeslot": timeslot,
                "date": date
            },
            success: function (response, textStatus, jqXHR) {
                console.log("RESPONSE" + response);
                var json_reply = $.parseJSON(response);
                var slot_id = json_reply.slot_id; // The slot_id column in Schedule that we added to (used for delete)
                var patient_id = json_reply.patient_id; //The patient id. NULL if this is a new patient (no id yet)
                addNewPerson(timeslot, first_name, last_name, patient_id, slot_id);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Error " + textStatus); //throw some error
            }
        });

        /* Must get the patient ID from AJAX and put that here
         */

    });

    $('#look-up').click(function (e) {
        loadData();
        $(this).blur(); //remove blur

    });

    /*
     * Call when document is loaded
     */
    $(document).ready(function () {
        loadData();
    });

    // Returns Current Date in String in MM/DD/YYYY format
    function getDateString() {
        var currentDt = new Date();
        var mm = currentDt.getMonth() + 1;
        var dd = currentDt.getDate();
        var yyyy = currentDt.getFullYear();
        return mm + '/' + dd + '/' + yyyy;
    }

    // Clear the table
    // We store all our buttons with the patient's name as name=personbutton
    // So let's just request them and remove them
    function clearTable()
    {
        $("[name=personbutton]").remove();
    }

    function addNewPerson(start_time, first_name, last_name, patient_id, slot_id)
    {
        // start_time = String like 8:00AM, which are the ids of our <td> entries
        // The ID's are in the DOM as something like this: <td id = "06:30PM">
        // We can't do something like $("#06:30PM") because : (colon) character is special
        // So we will do this: $("[id='06:30PM']") which will work
        // We will use .append() rather than manually making an HTMl string and appending it to innerHTML
        console.log("ADD NEW PERSON: " + first_name + " " + last_name);
        var request = $("[id='" + start_time + "']").append(
                $('<button/>', {
                    'class': 'btn btn-lg btn-info',
                    'type': 'button',
                    'name': 'personbutton',
                    value: first_name + ' ' + last_name,
                    'data-toggle': 'modal',
                    'data-target': '#delete-modal',
                    'data-firstname': first_name,
                    'data-lastname': last_name,
                    'data-patientid': patient_id,
                    'data-slotid': slot_id
                }).text(first_name + ' ' + last_name));
    }

    function loadDataForDay(response) {
        clearTable();
        var parsed = $.parseJSON(response);
        $.each(parsed, function (i, item) {
            // Find the ID, and append this button to it
            addNewPerson(item.start_time, item.first_name, item.last_name, item.patient_id, item.slot_id);

        }); // close $.each()
    }

    function loadData() {
        //var d = document.getElementById("schedule-date").value; //get value like 12/02/2015
        $("#look-up").prop("disabled", true); //disable button until we get results back
        var d = $("#schedule-date").val();
        if (d == "" || !d)
            d = getDateString(); //Default to current
        console.log("REQUESTING FOR DATE= " + d);
        var request = $.ajax({
            url: "scheduling-ajax.php",
            type: "POST",
            data: {"action": "GET_PATIENTS_FOR_DAY", "date": d},
            success: function (response) {
                console.log("RESPONSE" + response);
                loadDataForDay(response);
                $("#look-up").prop("disabled", false);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Error " + textStatus);
            }
        });
        //var d = document.getElementById("schedule-date").value; //get value like 12/02/2015

    }

    // Source: http://stackoverflow.com/questions/22405259/use-jquery-datepicker-in-bootstrap-3-modal
    // This line is required to get the jQuery DatePicker in the Scheduler to work right in Firefox
    // The issue is because jQuery Modals and Boostrap modals don't play nice
    // And the Bootstrap datepicker isn't feature rich enough for our needs, so we're using the jQuery one
    $.fn.modal.Constructor.prototype.enforceFocus = function () {
    };
</script>