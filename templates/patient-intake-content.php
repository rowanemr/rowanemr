<?php
require_once dirname(__DIR__) . '/core/init.php';
$results = get_user_name($_SESSION['id']);
$row = $results->fetch_assoc();
?>

<?php

/* We are going to show a list of patients scheduled for today's date.
 * We will give them (a person who works in the clinic) the options to EITHER:
 * 1. Use one tablet for the whole practice. In this scenario, the clinic worker will click on one of these links
      to click on. This link will logout the office worker, and prompt the patient to fill out office related paperwork
      (patient intake means patient fills it out themselves!)

 * 2. Use multiple tablets for the practice. I envision one tablet (or more) dedicated to patient paperwork.
      In this case, the patient will enter in a token on the tablet (on a special page) given to them by an office
      worker.
      The token is given to the office worker on this page. On this patient-only tablet, no logins will occur.
      The token is used to `authenticate` users (they are who they say they are). Note that patients as of this
      writing DO NOT get logins. The token will expire after one-use.

*/

// We will echo out links (in case we are handing the tablet directly to the patient), as well as tokens
// (for the office worker to read to the patient to enter into a different page).
// Only show for the current date
?>
<link href="css/index-logged-out.css" rel="stylesheet">
<link href="css/login.css" rel="stylesheet">
<link href="css/chat.css" rel="stylesheet">

<style>
    .content {
        font-size: 16px;
    }
    ol
    {
        text-align: center; list-style-position:inside;
    }
</style>
<body>
<div class = "content" align="center">
    <h1>Patient Intake</h1>

    Here are a list of patients scheduled for today. Some of them still require patient intake paperwork, others may require an initial office visit from
    a medical student, and others may require approval from a doctor.<br>
    For Patient Intake, you can either:
<ol>
    <li>Click on the links and give the tablet to the patient or</li>
    <li>Read the token to the patient to fill out the digital paperwork</li>
</ol>
<?php
    global $db;
    $date_str = date('m/d/Y');
    $user_role = get_user_role($_SESSION['id']);
    echo "<h2>Scheduled Patients for today: <i>$date_str</i></h2>";
    echo "<ol>";
    $query = "SELECT first_name, last_name, token, DATE_FORMAT(start_time, '%h:%i%p') AS start_time, Patients.patient_id, slot_id, completed_paperwork, status ";
    $query .= "FROM Patients ";
    $query .= "JOIN Schedule ON Patients.patient_id = Schedule.scheduled_patient_id ";
    $query .= "LEFT JOIN OfficeVisitForm On Schedule.slot_id = OfficeVisitForm.visit_date_id "; // LEFT JOIN, because if it's not filled out yet, that's okay, $status will be NULL
    $query .= "WHERE Schedule.slot_date = CAST(NOW() AS DATE) "; // get patients for today
    $query .= "ORDER BY slot_date ASC";
    $stmt = $db->prepare($query);
    $stmt->bind_result($first_name, $last_name, $token, $start_time, $current_patient_id, $current_slot_id, $completed_paperwork, $status); // bind the parameters to a result. Very useful!
    $stmt->execute();
    // Fetch all results and generate links to them
    $has_data = false;
    while ($stmt->fetch())
    {
        $has_data = true;
        if ($completed_paperwork === 'Y' and ($status == STATUS_EMPTY or $status == ''))
        {
            // If patient paperwork is done and office visit form is empty, allow them to see this patient in the exam room
            echo "<li>See this patient now: <a href = \"show_visit_form.php?patient_id=$current_patient_id&slot_id=$current_slot_id\">$first_name $last_name see in office</a></li>";
        }
        else if ($completed_paperwork === 'Y' and $status == STATUS_AWAITING_APPROVAL and ($user_role == 'DOCTOR' or $user_role == 'ADMIN'))
        {
            // Let the Doctor approve the paperwork if patient has completed paperwork and med student has completed paperwork (STATUS_AWAITING_APPROVAL)
            echo "<li>Sign off on this chart: <a href = \"show_visit_form.php?patient_id=$current_patient_id&slot_id=$current_slot_id\">$first_name $last_name (approve chart)</a></li>";
        }
        else if ($completed_paperwork != 'Y') // Office paperwork not completed; let them enter it in.
            echo "<li>Click and give to patient: <a href = \"patient-paperwork.php?token=$token\"> $first_name $last_name at $start_time.</a><br>Token: <strong>$token</strong></li>";
        else
            echo "<li><del>$first_name $last_name (complete)</del></li>";
    }
    if (!$has_data)
        echo "No patients scheduled for today.";
    echo "</ol>"; //end list
?>
</div>

<div class="chat-box">
    <input type="checkbox" />
    <label data-expanded="Close Chatbox" data-collapsed="Rowan EMR Chat"></label>
    <div id="prefetch" class="text-center">
        <input class="no-clear typeahead formcontrol" id="typeahead" type="text" placeholder="Search..." style="width: 150px; height 15px">
    </div>
    <!--        <div>
                <select id='select'>
                    <option value="default">--SELECT A USER--</option>
                    <option value="group">Rowan EMR Group Chat</option>
    <?php
    $list = get_user_list();
    if ($list)
    {
        $i = 0;
        while ($item = $list->fetch_assoc())
        {
            echo '<option id=user', $i, ' value="', $item['name'], '">', $item['name'], '</option>';
            ++$i;
        }
    }
    ?>
                </select>
            </div>-->
    <div class="chat-box-content">
        <div id="box" style="height:120px; width:170px; border:1px; overflow:auto;"></div>

        <input type="text" id="input" name="text_name" placeholder="Type..." style="width: 150px; height 15px;"/>

        <input id="chat" type="submit" value="Send">

        <div id="serverRes"></div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://cdn.pubnub.com/pubnub-3.7.18.min.js"></script>
<script src="scripts/js/typeahead.js"></script>
<script src="scripts/js/search-user.js"></script>
<script src="scripts/js/create-fields.js"></script>
<script src="scripts/js/create-survey.js"></script>


<script>
    var username = "<?php echo $row['name'] ?>";
    var pubnub = PUBNUB.init({
        subscribe_key: 'sub-c-a2afaf94-ae42-11e5-9510-02ee2ddab7fe',
        publish_key: 'pub-c-e2695edd-441b-4aaf-872f-fac79f6a1b12'
    });
    var input = document.getElementById('input').value;
    var channel;
    var id;
    var chan;
    var box;

    document.getElementById('chat').addEventListener("click", function () {
        publish();
    });

    document.getElementById('typeahead').addEventListener("change", function () {
        pubnub.unsubscribe({
            channel: channel
        });
        document.getElementById('box').innerHTML = "";
        privateChat();
        subscribe();
    });

    function privateChat() {
        id = pubnub.uuid;
        chan = 'rowanemr-' + id;
        box = pubnub.$('box'), input = pubnub.$('input'), channel = 'private';
    }

//    function groupChat() {
//        box = pubnub.$('box'), input = pubnub.$('input'), channel = 'rowanemr_channel';
//    }

    function publish() {
        pubnub.publish({
            channel: channel,
            message: {
                text: input.value,
                uuid: username
            }
        });
    }

    function subscribe() {
        console.log("Subscribing...");
        pubnub.subscribe({
            channel: channel,
            message: function (data) {
                box.innerHTML = ('' + data.uuid + ": " + data.text).replace(/[<>]/g, '') + '<br>' + box.innerHTML;
                input.value = '';
            },
            connect: pub
        });

        function pub() {
            console.log("Publishing...");
            pubnub.publish({
                channel: channel,
                message: {
                    text: " entered the RowanEMR chat",
                    uuid: username
                },
                callback: function (m) {
                    console.log(m);
                }
            });
        }
    }
</script>
</body>

