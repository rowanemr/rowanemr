<html>
    <head>
        <!-- Import charts JavaScript package. -->
        <script src="scripts/js/Chart.js"></script>
    </head>
    
    <style>
        .chartLegend li span
        {
            display: inline-block;
            width: 12px;
            height: 12px;
            margin-right: 5px;
        }
    </style>
    
    <body>
        <div id="content" align="center">
            <!-- We will show yes/no answers via bar graph. -->
            <canvas id="results" width="500" height="500"></canvas>
            <div id="legend" align="left" class="chartLegend col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4"></div>
            <br><br>
            <div id="table"></div>
        </div>
    </body>
    
    <script src="scripts/js/load-survey-response-data.js"></script>
    
</html>