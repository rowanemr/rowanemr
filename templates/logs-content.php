<?php
require_once dirname(__DIR__) . '/core/init.php';
$results = get_user_name($_SESSION['id']);
$row = $results->fetch_assoc();
?>

<!-- Custom CSS -->
<link href="css/index-logged-out.css" rel="stylesheet">
<link href="css/login.css" rel="stylesheet">
<link href="css/chat.css" rel="stylesheet">
<!-- Sets a scroll view for the table -->
<link href="css/table-style.css" rel="stylesheet">

<!-- Needed for datepicker widget -->
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
<script src="scripts/js/moment.js"></script>
<script src="scripts/js/bootstrap-datetimepicker.js"></script>


<!-- Title -->
<h1 align="center">Logs</h1>


<div align="center">
    <label for="datetimepicker1">Select the date for the logs you would like to see</label>
</div>

<form method="post"> <!-- No action specified so the php code on this page will execute upon submit -->
    <!-- Bootstrap Datepicker widget from eonasdan.github page (MIT license) -->
    <div class="container">
        <div class="row">
            <div class='col-xs-4'></div>
            <div class='col-xs-4'>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' name="log-date" class="form-control" readonly value=<?php
                        //The php here will keep the date the user submitted as the value, instead of clearing it on page load
                        if (isset($_POST['log-date']))
                        {
                            echo $_POST['log-date'];
                        }
                        else
                            echo '""' //keep it blank if no date looked up 
                            ?>
                               /> <!-- end date input -->
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <!-- Search Button -->
            <div class='col-xs-4'>
                <button type="submit" class="btn btn-info">Look Up</button>
            </div>
            <script type="text/javascript">
                $(function () {
                    $('#datetimepicker1').datetimepicker({
                        format: 'MM/DD/YYYY',
                        ignoreReadonly: true
                    });
                });
            </script>
        </div>
    </div>
</form>


<!-- Table to show logs for the specified day -->
<div class="container" id="table-scroll">
    <table class="table table-bordered">
        <!-- top row of table -->
        <thead>
            <tr>
                <th style="text-align:center">Time of Action</th>
                <th style="text-align:center">Type of Action</th>
                <th style="text-align:center">User Name</th>
            </tr>
        </thead>

    <!-- Body of table 1st <td> is time, 2nd is action, 3rd is user name -->
        <tbody>
            <?php
            if (isset($_POST['log-date']) && $_POST['log-date'] !== "")
            { //if user has looked up a date
                //get log-date and convert it to a sql-friendly date
                $log_date = clean($_POST['log-date']);
                $log_date = sql_friendly_date($log_date);

                global $db;
                //DATE() function is used in sql query to ignore the time part of 'created' field.
                $result = $db->query("SELECT log_user_id, action, created FROM Log WHERE DATE(created) = '$log_date'");
                while ($row = $result->fetch_assoc())
                {
                    $name_result = get_user_name($row['log_user_id']); //function we had in users.php returns result, not name itself
                    $name_row = $name_result->fetch_assoc(); //so we must fetch the array
                    echo '<tr>';
                    echo '<td style="text-align:center">' . $row['created'] . '</td>';
                    echo '<td style="text-align:center">' . $row['action'] . '</td>';
                    echo '<td style="text-align:center">' . $name_row['name'] . '</td>';
                    echo '</tr>';
                }
            }
            ?>
        </tbody>

    </table>
</div>




<br>
<br>
<br>
<br>
<div class="chat-box">
    <input type="checkbox" />
    <label data-expanded="Close Chatbox" data-collapsed="Rowan EMR Chat"></label>
    <div id="prefetch" class="text-center">
        <input class="no-clear typeahead formcontrol" id="typeahead" type="text" placeholder="Search..." style="width: 150px; height 15px">
    </div>
    <!--        <div>
                <select id='select'>
                    <option value="default">--SELECT A USER--</option>
                    <option value="group">Rowan EMR Group Chat</option>
    <?php
    $list = get_user_list();
    if ($list)
    {
        $i = 0;
        while ($item = $list->fetch_assoc())
        {
            echo '<option id=user', $i, ' value="', $item['name'], '">', $item['name'], '</option>';
            ++$i;
        }
    }
    ?>
                </select>
            </div>-->
    <div class="chat-box-content">
        <div id="box" style="height:120px; width:170px; border:1px; overflow:auto;"></div>

        <input type="text" id="input" name="text_name" placeholder="Type..." style="width: 150px; height 15px;"/>

        <input id="chat" type="submit" value="Send">

        <div id="serverRes"></div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://cdn.pubnub.com/pubnub-3.7.18.min.js"></script>
<script src="scripts/js/typeahead.js"></script>
<script src="scripts/js/search-user.js"></script>
<script src="scripts/js/get-surveys.js"></script>


<script>
     var username = "<?php echo $row['name'] ?>";
     var pubnub = PUBNUB.init({
         subscribe_key: 'sub-c-a2afaf94-ae42-11e5-9510-02ee2ddab7fe',
         publish_key: 'pub-c-e2695edd-441b-4aaf-872f-fac79f6a1b12'
     });
     var input = document.getElementById('input').value;
     var channel;
     var id;
     var chan;
     var box;

     document.getElementById('chat').addEventListener("click", function () {
         publish();
     });

     document.getElementById('typeahead').addEventListener("change", function () {
         pubnub.unsubscribe({
             channel: channel
         });
         document.getElementById('box').innerHTML = "";
         privateChat();
         subscribe();
     });

     function privateChat() {
         id = pubnub.uuid;
         chan = 'rowanemr-' + id;
         box = pubnub.$('box'), input = pubnub.$('input'), channel = 'private';
     }

     //    function groupChat() {
     //        box = pubnub.$('box'), input = pubnub.$('input'), channel = 'rowanemr_channel';
     //    }

     function publish() {
         pubnub.publish({
             channel: channel,
             message: {
                 text: input.value,
                 uuid: username
             }
         });
     }

     function subscribe() {
         console.log("Subscribing...");
         pubnub.subscribe({
             channel: channel,
             message: function (data) {
                 box.innerHTML = ('' + data.uuid + ": " + data.text).replace(/[<>]/g, '') + '<br>' + box.innerHTML;
                 input.value = '';
             },
             connect: pub
         });

         function pub() {
             console.log("Publishing...");
             pubnub.publish({
                 channel: channel,
                 message: {
                     text: " entered the RowanEMR chat",
                     uuid: username
                 },
                 callback: function (m) {
                     console.log(m);
                 }
             });
         }
     }
</script>
