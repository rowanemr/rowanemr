<?php
require_once dirname(__DIR__) . '/core/init.php';
$results = get_user_name($_SESSION['id']);
$row = $results->fetch_assoc();
?>

<!-- Custom CSS -->
<link href="css/index-logged-out.css" rel="stylesheet">
<link href="css/login.css" rel="stylesheet">
<link href="css/chat.css" rel="stylesheet">

<header id="top" class="header">
    <div class="text-vertical-center">
        <h1>Welcome to Rowan EMR</h1>
        <h3>Logged in as: <?php echo $row['name'] ?></h3>
        <h4>(<?php echo get_user_role($_SESSION['id']); ?>)</h4>
        <div class="row">
            <!-- <a href="patient-demographic.php?patient=new"> -->
            <a href = "templates/patient-login-content.php">
                <img border="25" src="res/image/Patient.png" width="150" height="150">
            </a>
            <a href = "generate-survey.php">
                <img border="25" src="res/image/Surveys.png" width="150" height="150">
            </a>
            <a href = "patient-intake.php">
                <img border="25" src="res/image/New-Patient-Intake.png" width="150" height="150">
            </a>
            <a href="scheduling.php">
                <img border="25" src="res/image/Scheduling.png" width="150" height="150">
            </a>
            <a href="reports.php">
                <img border="25" src="res/image/Data-Export.png" width="150" height="150">
            </a>
        </div>
        <div class="row">
            <a href="search.php">
                <img border="25" src="res/image/Search-For-Patient.png" width="150" height="150">
            </a>
            <a href="manage-surveys.php">
                <img border="25" src="res/image/ManageSurveys.png" width="150" height="150">
            </a>
            <a href="logs.php">
                <img border="25" src="res/image/logs.png" width="150" height="150">
            </a>
            <a href="users.php">
                <img border="25" src="res/image/users.png" width="150" height="150">
            </a>
            <a href="core/database/logout.php">
                <img border="25" src="res/image/Logout.png" width="150" height="150">
            </a>
        </div>
    </div>
    <div class="chat-box">
        <input type="checkbox" />
        <label data-expanded="Close Chatbox" data-collapsed="Rowan EMR Chat"></label>
        <div id="prefetch" class="text-center">
            <input class="no-clear typeahead formcontrol" id="typeahead" type="text" placeholder="Search..." style="width: 150px; height 15px">
        </div>
        <!--        <div>
                    <select id='select'>
                        <option value="default">--SELECT A USER--</option>
                        <option value="group">Rowan EMR Group Chat</option>
                <?php
                $list = get_user_list();
                if ($list)
                {
                    $i = 0;
                    while ($item = $list->fetch_assoc())
                    {
                        echo '<option id=user', $i, ' value="', $item['name'], '">', $item['name'], '</option>';
                        ++$i;
                    }
                }
                ?>
                    </select>
                </div>-->
        <div class="chat-box-content">
            <div id="box" style="height:120px; width:170px; border:1px; overflow:auto;"></div>

            <input type="text" id="input" name="text_name" placeholder="Type..." style="width: 150px; height 15px;"/>

            <input id="chat" type="submit" value="Send">

            <div id="serverRes"></div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.pubnub.com/pubnub-3.7.18.min.js"></script>
    <script src="scripts/js/typeahead.js"></script>
    <script src="scripts/js/search-user.js"></script>
</header>

<script>
    var username = "<?php echo $row['name'] ?>";
    var pubnub = PUBNUB.init({
        subscribe_key: 'sub-c-a2afaf94-ae42-11e5-9510-02ee2ddab7fe',
        publish_key: 'pub-c-e2695edd-441b-4aaf-872f-fac79f6a1b12'
    });
    var input = document.getElementById('input').value;
    var channel;
    var id;
    var chan;
    var box;

    document.getElementById('chat').addEventListener("click", function () {
        publish();
    });

    document.getElementById('typeahead').addEventListener("change", function () {
        pubnub.unsubscribe({
            channel: channel
        });
            document.getElementById('box').innerHTML = "";
            privateChat();
            subscribe();
    });

    function privateChat() {
        id = pubnub.uuid;
        chan = 'rowanemr-' + id;
        box = pubnub.$('box'), input = pubnub.$('input'), channel = 'private';
    }

//    function groupChat() {
//        box = pubnub.$('box'), input = pubnub.$('input'), channel = 'rowanemr_channel';
//    }

    function publish() {
        pubnub.publish({
            channel: channel,
            message: {
                text: input.value,
                uuid: username
            }
        });
    }

    function subscribe() {
        console.log("Subscribing...");
        pubnub.subscribe({
            channel: channel,
            message: function (data) {
                box.innerHTML = ('' + data.uuid + ": " + data.text).replace(/[<>]/g, '') + '<br>' + box.innerHTML;
                input.value = '';
            },
            connect: pub
        });

        function pub() {
            console.log("Publishing...");
            pubnub.publish({
                channel: channel,
                message: {
                    text: " entered the RowanEMR chat",
                    uuid: username
                },
                callback: function (m) {
                    console.log(m);
                }
            });
        }
    }
</script>