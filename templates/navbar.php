<!-- Static navbar -->

<script>
  // Add functionality to mark a link in the navbar as active (make it a darker color to indicate current page).
  // This is cleaner than a PHP solution.
  $(document).ready(function() {

    // get the full path to this URL
    var path_name = this.location.pathname;
    // get the name of this PHP script, eg: patient-intake.php
    var script_name = path_name.substr(path_name.lastIndexOf('/') + 1);
    //mark this anchor tag as active (have a darker gray color in navbar to denote current page)
    $('a[href="' + script_name + '"]').parent().addClass('active');

  });
</script>
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">Rowan OMM Clinic</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="core/database/logout.php">Logout</a></li>
       <li><a href="scheduling.php">Schedule</a></li>
        <li><a href="patient-intake.php">Patient Intake</a></li>
        <li><a href="reports.php">Data Export</a></li>
        <li><a href="search.php">Search Patient</a></li>
        <li><a href="generate-survey.php">Create Survey</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administrator <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="logs.php">Logs</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="users.php">Users</a></li>
            <li role="separator" class="divider"></li>
             <li><a href="manage-surveys.php">Manage Surveys</a></li>
          </ul>
        </li>
      </ul>

    </div><!--/.nav-collapse -->
  </div>
</nav>

