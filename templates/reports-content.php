<?php
require_once dirname(__DIR__) . '/core/init.php';
$results = get_user_name($_SESSION['id']);
$row = $results->fetch_assoc();
?>

<!-- Custom CSS -->
<link href="css/index-logged-out.css" rel="stylesheet">
<link href="css/login.css" rel="stylesheet">
<link href="css/chat.css" rel="stylesheet">
<h3 align="center"> Data Export </h3>
<h4 align="center"> Select Data to Export </h3>
<div class = "text-center" align="center"style='padding: 10px;'>

    Select field by left clicking.<br>
    Select a range by shift+clicking one field to another.<br>
    Select multiple fields holding ctrl (cmd on Mac) and clicking multiple fields.<br>
    <br>
    Click "Download" when finished selecting.


    <form action="core/database/generate-report.php" class="form-horizontal" align="center" method="post">
        <fieldset>
            <div align="center">
                <select id="selectmultiple" name="field[]" class="form-control" size="21" style='height: 100%; width:250px;' multiple="multiple">
                    <option value="1">Gender</option>
                    <option value="2">Alcohol Usage</option>
                    <option value="3">Has History Tobacco</option>
                    <option value="4">Has Pain Right Now</option>
                    <option value="5">Activity Onset of Pain</option>
                    <option value="6">Pain at Worst</option>
                    <option value="7">Pain at Best</option>
                    <option value="8">Pain on Average</option>
                    <option value="9">Has Allergies</option>
                    <option value="10">Has Physical Trauma</option>
                    <option value="11">Knows About DO</option>
                    <option value="12">Been to DO Before</option>
                    <option value="13">Knows OMM</option>
                    <option value="14">Had OMM Before</option>
                    <option value="15">Heard About Clinic</option>
                    <option value="16">Heart Rate</option>
                    <option value="17">Respiratory Rate</option>
                    <option value="18">Height</option>
                    <option value="19">Weight</option>
                    <option value="20">Blood Pressure</option>
                </select>
            </div>
        </fieldset>
        <div class="text-center" style='padding:20px;'>
            <input type="submit" class="btn btn-primary" value="Download">
        </div>
</div>
</form>
<div class="chat-box">
    <input type="checkbox" />
    <label data-expanded="Close Chatbox" data-collapsed="Rowan EMR Chat"></label>
    <div id="prefetch" class="text-center">
        <input class="no-clear typeahead formcontrol" id="typeahead" type="text" placeholder="Search..." style="width: 150px; height 15px">
    </div>
    <!--        <div>
                <select id='select'>
                    <option value="default">--SELECT A USER--</option>
                    <option value="group">Rowan EMR Group Chat</option>
    <?php
    $list = get_user_list();
    if ($list)
    {
        $i = 0;
        while ($item = $list->fetch_assoc())
        {
            echo '<option id=user', $i, ' value="', $item['name'], '">', $item['name'], '</option>';
            ++$i;
        }
    }
    ?>
                </select>
            </div>-->
    <div class="chat-box-content">
        <div id="box" style="height:120px; width:170px; border:1px; overflow:auto;"></div>

        <input type="text" id="input" name="text_name" placeholder="Type..." style="width: 150px; height 15px;"/>

        <input id="chat" type="submit" value="Send">

        <div id="serverRes"></div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://cdn.pubnub.com/pubnub-3.7.18.min.js"></script>
<script src="scripts/js/typeahead.js"></script>
<script src="scripts/js/search-user.js"></script>

<script>
    var username = "<?php echo $row['name'] ?>";
    var pubnub = PUBNUB.init({
        subscribe_key: 'sub-c-a2afaf94-ae42-11e5-9510-02ee2ddab7fe',
        publish_key: 'pub-c-e2695edd-441b-4aaf-872f-fac79f6a1b12'
    });
    var input = document.getElementById('input').value;
    var channel;
    var id;
    var chan;
    var box;

    document.getElementById('chat').addEventListener("click", function () {
        publish();
    });

    document.getElementById('typeahead').addEventListener("change", function () {
        pubnub.unsubscribe({
            channel: channel
        });
        document.getElementById('box').innerHTML = "";
        privateChat();
        subscribe();
    });

    function privateChat() {
        id = pubnub.uuid;
        chan = 'rowanemr-' + id;
        box = pubnub.$('box'), input = pubnub.$('input'), channel = 'private';
    }

//    function groupChat() {
//        box = pubnub.$('box'), input = pubnub.$('input'), channel = 'rowanemr_channel';
//    }

    function publish() {
        pubnub.publish({
            channel: channel,
            message: {
                text: input.value,
                uuid: username
            }
        });
    }

    function subscribe() {
        console.log("Subscribing...");
        pubnub.subscribe({
            channel: channel,
            message: function (data) {
                box.innerHTML = ('' + data.uuid + ": " + data.text).replace(/[<>]/g, '') + '<br>' + box.innerHTML;
                input.value = '';
            },
            connect: pub
        });

        function pub() {
            console.log("Publishing...");
            pubnub.publish({
                channel: channel,
                message: {
                    text: " entered the RowanEMR chat",
                    uuid: username
                },
                callback: function (m) {
                    console.log(m);
                }
            });
        }
    }
</script>

