<?php
require_once dirname(__DIR__) . '/core/init.php';
$results = get_user_name($_SESSION['id']);
$row = $results->fetch_assoc();
?>

<!-- Styling for text fields -->
<style>
    .typeahead,
    .tt-query,
    .tt-hint, .formcontrol {
        width: 800px;
        height: 50px;
        padding: 8px 12px;
        font-size: 16px;
        line-height: 30px;
        border: 2px solid #ccc;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        border-radius: 8px;
        outline: none;
    }

    .typeahead {
        background-color: #fff;
    }

    .typeahead:focus {
        border: 2px solid #0097cf;
    }

    .tt-query {
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    }

    .tt-hint {
        color: #999
    }

    .tt-menu {
        width: 422px;
        margin: 12px 0;
        padding: 8px 0;
        background-color: #fff;
        border: 1px solid #ccc;
        border: 1px solid rgba(0, 0, 0, 0.2);
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        border-radius: 8px;
        -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
        -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
        box-shadow: 0 5px 10px rgba(0,0,0,.2);
    }

    .tt-suggestion {
        padding: 3px 20px;
        font-size: 16px;
        line-height: 24px;
    }

    .tt-suggestion:hover {
        cursor: pointer;
    }

    .tt-suggestion.tt-cursor {
        color: #fff;
        background-color: #0097cf;

    }

    .tt-suggestion p {
        margin: 0;
    }
</style>

<link href="css/index-logged-out.css" rel="stylesheet">
<link href="css/login.css" rel="stylesheet">
<link href="css/chat.css" rel="stylesheet">

<h3 align="center">Create A Survey</h3>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-md-offset-5">
            <select id="numOfFields" class='form-control'>
                <option id='1' value="1">1</option>
                <option id='2' value="2">2</option>
                <option id='3' value="3">3</option>
                <option id='4' value="4">4</option>
                <option id='5' value="5">5</option>
                <option id='6' value="6">6</option>
                <option id='7' value="7">7</option>
                <option id='8' value="8">8</option>
                <option id='9' value="9">9</option>
                <option id='10' value="10">10</option>
            </select>
            <br>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div align="center" id="content"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 col-md-offset-5">
            <input type="text" id="title" placeholder="Survey Title" class="form-control">
        </div>
    </div>
    <br>

    <div class="row" align="center">
        <div class="col-md-2 col-md-offset-5">
            <button id="create" class="btn btn-primary">Create Survey</button>
        </div>
    </div>
</div>

<div class="chat-box">
    <input type="checkbox" />
    <label data-expanded="Close Chatbox" data-collapsed="Rowan EMR Chat"></label>
    <div id="prefetch" class="text-center">
        <input class="no-clear typeahead formcontrol" id="typeahead" type="text" placeholder="Search..." style="width: 150px; height 15px">
    </div>
    <!--        <div>
                <select id='select'>
                    <option value="default">--SELECT A USER--</option>
                    <option value="group">Rowan EMR Group Chat</option>
    <?php
    $list = get_user_list();
    if ($list)
    {
        $i = 0;
        while ($item = $list->fetch_assoc())
        {
            echo '<option id=user', $i, ' value="', $item['name'], '">', $item['name'], '</option>';
            ++$i;
        }
    }
    ?>
                </select>
            </div>-->
    <div class="chat-box-content">
        <div id="box" style="height:120px; width:170px; border:1px; overflow:auto;"></div>

        <input type="text" id="input" name="text_name" placeholder="Type..." style="width: 150px; height 15px;"/>

        <input id="chat" type="submit" value="Send">

        <div id="serverRes"></div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://cdn.pubnub.com/pubnub-3.7.18.min.js"></script>
<script src="scripts/js/typeahead.js"></script>
<script src="scripts/js/search-user.js"></script>
<script src="scripts/js/create-fields.js"></script>
<script src="scripts/js/create-survey.js"></script>


<script>
    var username = "<?php echo $row['name'] ?>";
    var pubnub = PUBNUB.init({
        subscribe_key: 'sub-c-a2afaf94-ae42-11e5-9510-02ee2ddab7fe',
        publish_key: 'pub-c-e2695edd-441b-4aaf-872f-fac79f6a1b12'
    });
    var input = document.getElementById('input').value;
    var channel;
    var id;
    var chan;
    var box;

    document.getElementById('chat').addEventListener("click", function () {
        publish();
    });

    document.getElementById('typeahead').addEventListener("change", function () {
        pubnub.unsubscribe({
            channel: channel
        });
        document.getElementById('box').innerHTML = "";
        privateChat();
        subscribe();
    });

    function privateChat() {
        id = pubnub.uuid;
        chan = 'rowanemr-' + id;
        box = pubnub.$('box'), input = pubnub.$('input'), channel = 'private';
    }

//    function groupChat() {
//        box = pubnub.$('box'), input = pubnub.$('input'), channel = 'rowanemr_channel';
//    }

    function publish() {
        pubnub.publish({
            channel: channel,
            message: {
                text: input.value,
                uuid: username
            }
        });
    }

    function subscribe() {
        console.log("Subscribing...");
        pubnub.subscribe({
            channel: channel,
            message: function (data) {
                box.innerHTML = ('' + data.uuid + ": " + data.text).replace(/[<>]/g, '') + '<br>' + box.innerHTML;
                input.value = '';
            },
            connect: pub
        });

        function pub() {
            console.log("Publishing...");
            pubnub.publish({
                channel: channel,
                message: {
                    text: " entered the RowanEMR chat",
                    uuid: username
                },
                callback: function (m) {
                    console.log(m);
                }
            });
        }
    }
</script>