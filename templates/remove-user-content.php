<?php
	if(isset($_GET['id'])) {
		$id = clean_up($_GET['id']);
	}
	else {
    header("Location: users.php");
	}
?>
<div class="container">

  <div style="margin-top:10px;" class="mainbox col-md-12 text-center"> 

    	<legend><div class="row text-center">Delete User</div></legend>
      
      <h5>Pressing "Delete" will remove the user.</h5>
      <h5>This cannot be undone. Would you like to continue?</h5>

      <br><br><br><br><br><br><br><br><br><br><br>
       <div class="row text-center">
         <a type="button" class="btn btn-primary" href="core/database/remove-user.php?id=<?php echo $id; ?>">Delete</a>
         <a type="button" class="btn btn-primary" href="users.php">Back</a>
      </div>

  </div>
</div>