<?php
$mytabs = array('#demographics' => 'demographics', '#pain' => 'pain', '#conditions_medications_surgery' => 'conditions_medications_surgery', '#symptoms' => 'symptoms', '#previous_office_visits' => 'previous_office_visits', '#send_videos' => 'send_videos');
if (!isset($tab)) //for use with include this file
    $tab = 'demographics';
// $patient_id may be defined in an existing file already
// eg: this file patient-info-content.php is included inside show_visit_form.php for Intake tab
if (isset($_GET['patient_id']))
    $patient_id = clean_up($_GET['patient_id']);
elseif (!isset($patient_id)) //check if defined in another file (i.e. included here)
    die("No patient id provided.");

function print_patient_info()
{
    global $patient_id;
    $patient_info = get_patient_info($patient_id); // get important information
    $first_name = $patient_info['first_name'];
    $last_name = $patient_info['last_name'];
    $dob = $patient_info['date_of_birth'];
    echo "<strong>Name:</strong> $first_name $last_name";
    echo "<br><strong>DOB:</strong> $dob";
}
?>

<div style="margin-top:20px;" class="mainbox col-md-12 text-center">

    <div class="row">

        <!-- Simple jQuery calls to switch out divs-->
        <a type="button" class="btn btn-primary" onClick="<?php make_swap_code('#demographics', $mytabs); ?>">Patient
            Demographics</a>
        <a type="button" class="btn btn-primary"
           onClick="<?php make_swap_code("#conditions_medications_surgery", $mytabs) ?>">Existing Conditions</a>
        <a type="button" class="btn btn-primary" onClick="<?php make_swap_code('#pain', $mytabs); ?>">Patient Pain</a>
        <a type="button" class="btn btn-primary" onClick="<?php make_swap_code("#symptoms", $mytabs) ?>">Symptoms</a>
        <a type="button" class="btn btn-primary" onClick="<?php make_swap_code("#previous_office_visits", $mytabs) ?>">Previous
            Office Visits</a>
        <a type="button" class="btn btn-primary" onClick="<?php make_swap_code("#send_videos", $mytabs) ?>">Relay Videos</a>

        <!-- Same exact format, change the value of first param in make_swap_code() to whatever your div id is-->

    </div>

    <!-- DEMOGRAPHICS TAB SECTION -->
    <div <?php
    $my_id = "demographics";
    make_div_line_code($my_id, $tab === $my_id);
    ?> class="table-responsive">
        <h3>Patient Demographics</h3>
        <h4><?php print_patient_info(); ?></h4>
        <div class="text-center">
            <table class="table table-striped text-center">
                <?php
                global $db;
                $fields = array("preferred_name", "date_entered", "date_of_birth", "gender", "daily_work_performed", "completed_education", "has_history_tobacco", "alcohol_usage", "ethnicity", "last_modified");
                $field_print_names = array("Preferred Name", "Appointment Date", "Date of Birth", "Gender", "Daily Work Performed", "Completed Education", "Has History of Tobacco", "Alcohol Usage", "Ethnicity", "Last Updated");
                $result = $db->query("SELECT * FROM PatientDemographics WHERE patient_id =  '$patient_id'");
                $row = $result->fetch_assoc();
                //output student table.
                // table headers
                echo '<thead><tr>';
                echo '
                        <th><div class="text-center">Field</div></th>
                        <th><div class="text-center">Value</div></th>
                       
                        ';
                echo '</tr></thead>';
                echo '<tbody>';
                for ($i = 0; $i < count($fields); $i++)
                {
                    $current_field = $fields[$i]; // get the SQL field name like preferred_name
                    $current_value = $row[$current_field]; // get the value from $row (what the preferred_name really is)
                    $current_pretty_message = $field_print_names[$i]; // get the pretty name like Preferred Name
                    echo "<tr><td>$current_pretty_message</td>";
                    echo "<td>$current_value</td></tr>";
                    echo '</tbody>';
                }
                ?>
            </table>
        </div>
    </div>


    <!-- PAIN TAB SECTION -->
    <div <?php
    $my_id = "pain";
    make_div_line_code($my_id, $tab === $my_id);
    ?> class="table-responsive">
        <h3>Patient Pain</h3>
        <h4> <?php print_patient_info(); ?> </h4>
        <div class="text-center">
            <?php
            global $db;
            $fields = array("has_pain_now", "pain_start_date", "activity_onset_pain", "pain_right_now", "pain_at_worst", "pain_at_best", "pain_on_average", "what_makes_pain_worse", "what_makes_pain_better");
            $field_print_names = array("Has Pain Now", "Pain Start Date", "Activity Onset", "Pain Right Now", "Pain At Worse", "Pain At Best", "Pain On Average", "What Makes Pain Worse?", "What Makes Pain Better");
            $query = "SELECT * FROM PatientPain ";
            $query .= "JOIN Schedule ON PatientPain.visit_date_id = Schedule.slot_id ";
            $query .= "WHERE patient_id = '$patient_id' ";
            $query .= "ORDER BY PatientPain.last_modified DESC";
            $result = $db->query($query);
            // We must get all of the rows this user filled out
            // This is because for multiple office visits, they will have filled out multiple Pain forms
            // This is in contrast to the Demographic form, which they filled out once, but UPDATE multiple times.
            // We will present them in descending order (thanks, SQL query!)
            $rows = $result->fetch_all(MYSQLI_ASSOC);
            $counter = 0;
            foreach ($rows as $row)
            {
                echo '<table class="table table-striped text-center">';
                $start_time = $row['start_time']; // when we started this appointment
                echo "<thead>";
                echo "For Appointment Date: <strong>$start_time</strong>";
                echo '<tr><th> <div class="text-center">Field</div></th><th><div class="text-center">Value</div></th>';
                echo '</tr></thead>';
                echo '<tbody>';
                for ($i = 0; $i < count($fields); $i++)
                {
                    $current_field = $fields[$i]; // get the SQL field name like preferred_name
                    $current_value = $row[$current_field]; // get the value from $row (what the preferred_name really is)
                    $current_pretty_message = $field_print_names[$i]; // get the pretty name like Preferred Name
                    echo "<tr><td><div class=\"text-center\">$current_pretty_message</div></td>";
                    echo "<td><div class = \"text-center\">$current_value</div></td></tr>";
                }

                // Now, get information for the Pain Figure
                $query = "SELECT x_coord AS x, y_coord AS y FROM PatientPainCoordinates WHERE patient_pain_id = {$row['patient_pain_id']}";
                $coordinates_result = $db->query($query);
                $coordinate_rows = $coordinates_result->fetch_all(MYSQLI_ASSOC);
                $json_coords = json_encode($coordinate_rows);
                $counter = $counter + 1;
                echo '</tbody></table><br><br>';
                echo <<<EOD
            <div id="body-container$counter">
            <canvas id="body-canvas$counter" width="618" height="515"></canvas>
            <script>
                // counter here to prevent collisions. Hacky but it works.
                var canvas$counter = document.getElementById('body-canvas$counter');
                var context$counter = canvas$counter.getContext('2d');
                var imageObj$counter = new Image();
                var json_coords$counter = $.parseJSON('$json_coords'); // JavaScript parses the JSON coords from PHP
                imageObj$counter.onload = function() {
                    context$counter.drawImage(imageObj$counter, 0, 0);
                    for (var i = 0; i<(json_coords$counter).length; i++)
                    {
                        var c = json_coords$counter;
                        context$counter.fillStyle = 'red';
                        context$counter.fillRect(c[i].x, c[i].y, 20, 20);
                        }

                };
                imageObj$counter.src = 'res/image/body.jpeg';
            </script>
        </div>
EOD;
            }
            ?>

        </div>
    </div>


    <!-- This tab will contain 3 tables. A table for each: medical conditions, current medications, past surgeries-->
    <div <?php
    $my_id = "conditions_medications_surgery";
    make_div_line_code($my_id, $tab === $my_id);
    ?> class="table-responsive">
        <h3>Conditions, Medications, Surgeries</h3>
        <h4><?php print_patient_info(); ?> </h4>
        <!-- This table is for the patient's existing medical conditions -->
        <div class="text-center">
            <table class="table table-striped text-center">
                <?php
                $fields = array("changes_medication_two_weeks", "has_allergies", "has_physical_trauma", "personal_goals", "health_expectations", "interested_nutritional_topics", "other_concerns", "knows_about_do", "been_do_before", "knows_omm", "had_omm_before", "heard_about_clinic", "last_modified");
                $field_print_names = array("Changes In Medication Past Two Weeks", "Has Allergies", "Has Physical Trauma", "Personal Goals", "Health Expectations", "Interested In Nutritional Topics", "Other Concerns", "Knows about DO", "Been DO Before", "Knows about OMM", "Had OMM Before", "Head About Clinic", "Last Modified");
                $result = $db->query("SELECT * FROM PatientExistingConditions WHERE patient_id =  '$patient_id'");
                $row = $result->fetch_assoc(); // Only one row per patient unlike pain form
                echo '<thead><tr>';
                echo '
                        <th><div class="text-center">Field</div></th>
                        <th><div class="text-center">Value</div></th>

                        ';
                echo '</tr></thead>';
                echo '<tbody>';
                for ($i = 0; $i < count($fields); $i++)
                {
                    $current_field = $fields[$i]; // get the SQL field name like preferred_name
                    $current_value = $row[$current_field]; // get the value from $row (what the preferred_name really is)
                    $current_pretty_message = $field_print_names[$i]; // get the pretty name like Preferred Name
                    echo "<tr><td><div class=\"text-center\">$current_pretty_message</div></td>";
                    echo "<td><div class = \"text-center\">$current_value</div></td></tr>";
                    echo '</tbody>';
                }
                ?>
            </table>
        </div>

        <!-- Medical Conditions Table -->
        <div class="text-center">
            <table class="table table-striped text-center">
                <?php
                global $db;
                $fields = array("medical_condition", "date_of_diagnosis");
                $field_print_names = array("Medical Condition", "Date Of Diagnosis");
                $result = $db->query("SELECT * FROM PatientMedicalConditions WHERE patient_id =  '$patient_id'");
                $rows = $result->fetch_all(MYSQLI_ASSOC);

                if ($result->num_rows === 0)
                    echo '<th><div class="text-center">No Medical Conditions.</div></th>';
                else
                {
                    echo '<thead>';
                    echo '<th><div class="text-center">Medical Condition</div></th>
                        <th><div class="text-center">Date of Diagnosis</div></th>';
                    echo '</tr></thead>';
                    echo '<tbody>';
                    foreach ($rows as $row)
                    {
                        echo "<tr><td>{$row['medical_condition']}</td>";
                        echo "<td>{$row['date_of_diagnosis']}</td></tr>";
                    }
                    echo '</tbody>';
                }
                ?>
            </table>
        </div>

        <!-- Medications Table -->
        <div class="text-center">
            <table class="table table-striped text-center">
                <?php
                //table header
                echo '<thead><tr>';
                echo '<th><div class="text-center">Current Medications</div></th>';
                echo '</tr></thead>';
                //table body
                echo '<tbody>';
                $result = $db->query("SELECT medicine_name FROM PatientCurrentMedicine WHERE patient_id =  '$patient_id'");
                $rows = $result->fetch_all(MYSQLI_ASSOC);
                if ($result->num_rows === 0)
                    echo "<tr><td>No medications.</td></tr>";
                else
                {
                    foreach ($rows as $row)
                    {
                        //create a new row for each medication
                        echo "<tr><td><div class=\"text-center\">{$row["medicine_name"]}</td></tr>";
                    }
                }
                ?>
                </tbody>
            </table>
        </div>

        <!-- Surgeries Table -->
        <div class="text-center">
            <table class="table table-striped text-center">
                <?php
                global $db;
                $fields = array("type_surgery", "date_of_surgery");
                $field_print_names = array("Type Of Surgery", "Date Of Surgery");
                $result = $db->query("SELECT * FROM PatientPastSurgery WHERE patient_id =  '$patient_id'");
                $rows = $result->fetch_all(MYSQLI_ASSOC);

                if ($result->num_rows === 0)
                    echo '<th><div class="text-center">No Past Surgeries.</div></th>';
                else
                {
                    echo '<thead>';
                    echo '<th><div class="text-center">Type Of Surgery</div></th>
                        <th><div class="text-center">Date Of Surgery</div></th>';
                    echo '</tr></thead>';
                    echo '<tbody>';
                    foreach ($rows as $row)
                    {
                        echo "<tr><td>{$row['type_surgery']}</td>";
                        echo "<td>{$row['date_of_surgery']}</td></tr>";
                    }
                    echo '</tbody>';
                }
                ?>
            </table>
        </div>


    </div> <!-- Div for existing med conditions tab (which has medications and surgeries tables) -->


    <!-- SYMPTOMS TAB SECTION -->
    <div <?php
    $my_id = "symptoms";
    make_div_line_code($my_id, $tab === $my_id);
    ?> class="table-responsive">
        <h3>Patient Symptoms</h3>
        <h4> <?php print_patient_info(); ?> </h4>

        <div class="text-center">
            <?php
            global $db;
            // Symptoms is a special case
            // For each visit, we have multiple rows (checkboxes).
            // We need to process them a bit differently if we want to group them by visit_date_id
            // This query will give us all the symptoms for a particular patient on a particular date
            // Note that in the DB each symptom is a different row, but GROUP_CONCAT lets us easily combine them
            $query = "SELECT patient_id, visit_date_id, start_time, GROUP_CONCAT(patient_symptom) AS symptoms_comma_delimited ";
            $query .= "FROM PatientSymptoms ";
            $query .= "JOIN Schedule ON PatientSymptoms.visit_date_id = Schedule.slot_id ";
            $query .= "WHERE patient_id = '$patient_id' ";
            $query .= "GROUP BY patient_id, visit_date_id, start_time ";
            $query .= "ORDER BY PatientSymptoms.last_modified DESC"; // put most recent first
            $result = $db->query($query);
            $rows = $result->fetch_all(MYSQLI_ASSOC);
            if ($result->num_rows === 0)
            {
                echo '<tr><td>No Symptoms.</td></tr>';
            }
            else
            {
                foreach ($rows as $row)
                {
                    echo '<table class="table table-striped text-center">';
                    $start_time = $row['start_time']; // when we started this appointment
                    echo "<thead>";
                    echo "For Appointment Date: <strong>$start_time</strong>";
                    echo '<tr><th> <div class="text-center">Symptom Name</div></th></tr>';
                    echo '</thead>';
                    echo '<tbody>';
                    $symptoms_comma_delimited = explode(",", $row['symptoms_comma_delimited']); //array of symptoms
                    foreach ($symptoms_comma_delimited as $symptom)
                        echo "<td><div class = \"text-center\">{$symptom}</div></td></tr>";
                }
                echo '</tbody></table><br><br>';
            }
            ?>
        </div>
    </div>

    <div <?php
    $my_id = "previous_office_visits";
    make_div_line_code($my_id, $tab === $my_id);
    ?> class="table-responsive">
        <h3>Previous Office Visits</h3>
        <h4> <?php print_patient_info(); ?> </h4>

        <div class="text-center">
            <?php
            // Here we define a mapping of the SQL field names to pretty names to print out.
            // We want the values printed out in this exact order, which we can do.
            // Note that PHP Associative Arrays are an ORDERED MAP unlike say a HashMap in Java (has no ordering) !
            $mapping = array('chief_complaint' => 'Chief Complaint', 'location' => 'Location', 'quality' => 'Quality', 'severity' => 'Severity', 'duration' => 'Duration', 'timing' => 'Timing', 'radiation' => 'Radiation', 'context' => 'Context', 'modifying_factors' => 'Modifying Factors', 'associated_signs_symptoms' => 'Associated Signs&Symptoms', 'past_medical_history' => 'Past Medical History', 'past_surgical_history' => 'Past Surgical History', 'medications' => 'Medications', 'allergies' => 'Allergies', 'social_history' => 'Social History', 'review_of_symptoms' => 'Review Of Symptoms', 'heart_rate' => 'Heart Rate', 'resp_rate' => 'Resp. Rate', 'systolic_bp' => 'Systolic BP', 'diastolic_bp' => 'Diastolic BP', 'height' => 'Height (Inches)', 'weight' => 'Weight', 'gen_awake_alert' => 'Awake, Alerted', 'gen_no_acute_distress' => 'No Acute Distress', 'gen_abnormal' => 'Gen Abnormal', 'heent_ears_clear_intact' => 'TM Clear&Intact', 'heent_ears_canals_clear' => 'Ear Canals Clear', 'heent_neck_supple_nontender' => 'Neck supple, nontender', 'heent_neck_no_thyromegaly' => 'No thyromegaly', 'heent_mouth_mucous_membranes_moist' => 'Mucous Membranes Moist', 'heent_mouth_no_erythema' => 'No erythema; exudates', 'heent_abnormal' => 'Heent Abnormal', 'cardiac_regular_rate' => 'Regular Cardiac Rate', 'cardiac_murmurs' => 'No murmurs, rubs, gaps', 'cardiac_abnormal' => 'Cardiac Abnormal', 'lungs_clear' => 'Lungs Clear', 'lungs_abnormal' => 'Lungs Abnormal', 'gi_abdomen_soft' => 'Abdomen soft, non-tender', 'gi_no_masses' => 'No masses, splenomegaly, hepatomegaly', 'gi_abnormal' => 'GI Abnormal', 'ext_no_edema' => 'No edema, cyanosis', 'ext_abnormal' => 'EXT Abnormal', 'msk_full_range_motion' => 'Full range of motion', 'msk_no_joint_deformity' => 'No joint deformity', 'msk_no_muscle_hypertonicity' => 'No muscle hypertonicity', 'msk_abnormal' => 'MSK Abnormal', 'neuro_cranial_nerves_intact' => 'Cranial Nervies II through XII Intact', 'neuro_muscle_strength' => 'Muscle strength 5/5', 'neuro_sensation_intact' => 'Sensation intact', 'neuro_deep_tendon_reflexes' => 'Deep Tendon Reflexes 2/4', 'neuro_abnormal' => 'Neuro Abnormalities', 'notes' => 'Notes', 'assessment' => 'Assessment', 'plan' => 'Plan');
            $query = "SELECT * FROM OfficeVisitForm JOIN Schedule ON OfficeVisitForm.visit_date_id = Schedule.slot_id ";
            $query .= "WHERE OfficeVisitForm.patient_id = '$patient_id' ";
            $query .= "ORDER BY OfficeVisitForm.last_updated DESC";
            $result = $db->query($query);
            $rows = $result->fetch_all(MYSQLI_ASSOC); // can have multiple visits, so multiple rows
            if ($result->num_rows === 0)
            {
                echo '<tr><td>No Previous Office Visits</td></tr>';
            }
            else
            {
                foreach ($rows as $row)
                {
                    // Query: Get the treatment methods for this office visit form
                    // Use GROUP_CONCAT to get easy to read list such as: Abdomen | ME MFR HVLA ART...
                    $query = "SELECT region, GROUP_CONCAT(treatment_method SEPARATOR ' ') AS treatment_methods FROM OfficeVisitFormTreatments ";
                    $query .= " WHERE OfficeVisitFormTreatments.office_visit_id = {$row['office_visit_form_id']} ";
                    $query .= "GROUP BY region";
                    $treatment_result = $db->query($query);
                    $treatment_rows = $treatment_result->fetch_all(MYSQLI_ASSOC); // can be multiple rows (Abdomen, Head) for each row in OfficeVisitForm
                    echo '<table class="table table-striped text-center">';
                    $start_time = $row['start_time']; // when we started this appointment
                    echo "<thead>";
                    echo "For Appointment Date: <strong>$start_time</strong>";
                    echo "<pre><strong>Summary:</strong> Patient presented with `" . $row['chief_complaint'] . '`<br>Treated with:';
                    foreach ($treatment_rows as $treatment_row)
                        echo "<br><strong>{$treatment_row['region']}</strong>: {$treatment_row['treatment_methods']}";
                    echo '</pre>';
                    echo '<th><div class="text-center">Field</div></th>
                       <th><div class="text-center">Value</div></th>';
                    echo '</tr></thead>';
                    echo '<tbody>';
                    // Loop through our fields to print out, and echo them in a table
                    foreach ($mapping as $current_field => $current_pretty_message)
                    {
                        // $current_field is SQL field name like chief_complaint
                        // $current_pretty_message is pretty name like Chief Complaint
                        $current_value = trim($row[$current_field]); // get the value from $row (what the complaint really is)
                        if (!isset($current_value) or $current_value == "" or strtoupper($current_value) == 'Y')
                            continue; //field empty or is normal ($current_val == 'Y'), so go to next value
                        echo "<tr><td><div class=\"text-center\">$current_pretty_message</div></td>";
                        echo "<td><div class = \"text-center\">$current_value</div></td></tr>";
                    }
                }
                echo '</tbody></table><br><br>';
            } // else
            ?>
        </div>
    </div>


    <div <?php
    $my_id = "send_videos";
    make_div_line_code($my_id, $tab === $my_id);
    ?> class="table-responsive">
        <h3>Relay Videos</h3>
        <h4> <?php print_patient_info(); ?> </h4>
        <a type="button" class="btn btn-info" onClick="addBoxes()">Add Video</a>

        <div class="text-center"  id="vid-input">

        </div>
    </div>

</div> <!-- For topmost div -->

<script>
    function addBoxes()
    {
        var div = document.getElementById("vid-input");
        if (div.children.length === 0) {
            var boxes = document.createElement("div");
            boxes.innerHTML = ('<br><textarea rows="3" cols="50" id="desc" placeholder="Description" maxlength="512"></textarea>');
            boxes.innerHTML += ('<br><textarea rows="1" cols="50" id="vidUrl" placeholder="URL"></textarea>');
            boxes.innerHTML += ('<br><br><a type="button" class="btn btn-success" onClick="checkBoxes()">Submit Video</a>');
            div.appendChild(boxes);
        }
    }

    function checkBoxes() {
        var desc = document.getElementById('desc').value;
        var vidUrl = document.getElementById('vidUrl').value;
        if (desc !== "") {
            if (vidUrl !== "") {
                if (window.location.href.indexOf("patient_id=") > -1) {
                    var url = document.URL;
                    var valid = validateUrl();
                    var videoID = getVideoId(vidUrl);
                    id = url.substring(url.lastIndexOf("=") + 1); // get the patient id from the url
                    var posting = {patient_id: id, url: vidUrl, description: desc, id: videoID};
                    if (valid) {
                        $.post('/rowanemr/rowanemr/core/database/add-video.php', {posting}, function (data) {
                            alert(data);
                            if (data !== "VIDEO DOES NOT EXIST") {
                                while (document.getElementById("vid-input").hasChildNodes()) {
                                    document.getElementById("vid-input").removeChild(document.getElementById("vid-input").lastChild);
                                }
                            }
                        });
                    }
                }
            }
            else {
                alert("Missing Field");
            }
        }
        else
        {
            alert("Missing Field");
        }
    }

    function validateUrl() {
        var url = $('#vidUrl').val();
        if (url != undefined || url != '') {
            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
            var match = url.match(regExp);
            if (match && match[2].length == 11) {
                return true;
            }
            else
            {
                alert('Invalid URL');
                return false;
            }
        }
    }

    function getVideoId(url) {
        var id = '';
        url = url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
        if (url[2] !== undefined) {
            id = url[2].split(/[^0-9a-z_\-]/i);
            id = id[0];
        }
        else {
            id = url;
        }
        return id;
    }
</script>