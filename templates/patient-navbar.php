<!-- Static navbar -->

<script>
  // Add functionality to mark a link in the navbar as active (make it a darker color to indicate current page).
  // This is cleaner than a PHP solution.
  $(document).ready(function() {

    // get the full path to this URL
    var path_name = this.location.pathname;
    // get the name of this PHP script, eg: patient-intake.php
    var script_name = path_name.substr(path_name.lastIndexOf('/') + 1);
    //mark this anchor tag as active (have a darker gray color in navbar to denote current page)
    $('a[href="' + script_name + '"]').parent().addClass('active');

  });
</script>
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="patient-dash.php">Patient Dashboard</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="patient-dash.php">Videos</a></li>
        <li><a href="patient-surveys.php">Surveys</a></li>
        <li><a href="../core/database/logout.php">Logout</a></li>
      </ul>

    </div><!--/.nav-collapse -->
  </div>
</nav>

