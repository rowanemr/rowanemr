<?php
	if(isset($_GET['userexists'])) {
		$ue = clean_up($_GET['userexists']);
	}
	else {
    $ue = 0;
	}
?>
<div class="container">

  <div style="margin-top:10px;" class="mainbox col-md-12 text-center">
    <form action="core/database/add-user.php" class="form-horizontal" method="post" onsubmit="validate()">
    <fieldset>

    	<legend><div class="row text-center">Create a New User
      <?php 
        if($ue != 0) {
          echo '<br><h5>USER NAME ALREADY EXISTS!</h5>';
          echo '<h5>Please choose a different username!</h5>';
        }
      ?>
      </div></legend>
    	<form class="form-horizontal">
      <fieldset>

      <div class="form-group">
        <label class="col-md-3 control-label" for="textinput"></label>
        <div class="col-md-6">
        <input id="textinput" name="user_name" type="text" placeholder="User Name" class="form-control input-md" required="">

        </div>
      </div>

       <div class="form-group">
        <label class="col-md-3 control-label" for="textinput"></label>
        <div class="col-md-6">
        <input id="textinput" name="full_name" type="text" placeholder="Full Name" class="form-control input-md" required="">

        </div>
      </div>

      <div class="form-group">
        <label class="col-md-3 control-label" for="textinput"></label>
        <div class="col-md-6">
        <input id="textinput" name="password" type="password" placeholder="Password" class="form-control input-md" required="">

        </div>
      </div>

      <div class="form-group">
        <label class="col-md-3 control-label" for="selectbasic"></label>
        <div class="col-md-6">
          <select id="selectbasic" name="user_role" class="form-control">
            <option value="ADMIN">Administrator</option>
             <option value="DOCTOR">Doctor</option>
              <option value="MEDSTUDENT">Medical Student</option>
          </select>
        </div>
      </div>


      <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
       <div class="row text-center">
        <input type="submit" class="btn btn-primary" value="Add" />
         <a type="button" class="btn btn-primary" href="users.php" value="Back">Back</a>
      </div>

      
    </fieldset>
    </form>
  </div>
</div>