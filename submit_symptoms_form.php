<?php
include 'core/init.php';
/* For Debugging */
error_reporting(E_ALL);
ini_set('display_errors', 1);
//var_dump($_POST);
/* patient_id will be propgated through a GET variable */
if (!isset($_GET['token']) or !is_valid_token($_GET['token']))
	die("Valid Token required");

$token = $_GET['token'];
$patient_id = get_patient_id_from_token($token);
$slot_id = get_slot_id_from_token($token); // needed for FK `visit_date_id` in PatientSymptoms
  											// so we can associate a form with a visit date!

$symptom_array = isset($_POST['symptom']) ? $_POST['symptom'] : array();
$symptom = ""; // define as a string
if(empty($symptom_array)){ //no symptoms were checked off
	echo("no symptoms selected"); //FOR TESTING ONLY
}
else{ //some symptoms were checked off

	//construct query
	global $db;
	$query = "INSERT INTO PatientSymptoms (patient_id, visit_date_id, patient_symptom, created, last_modified) ";
	$query .= "VALUES (?,?,?, now(), now())";
	//prepare
	if (!($stmt = $db->prepare($query)))
	{
		echo "Prepare failed: (" . $db->errno . ") " . $db->error;
	}
	//bind
	if (!($stmt->bind_param("dds", $patient_id, $slot_id, $symptom)))
	{
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}

	//loop through array of symptoms and create a new row for each symptom. patient id will be the same.
	for($i=0; $i < count($symptom_array); $i++)
	{
    	//Probably not necessary to clean since they are values we specified in symptoms-content, but doing it since its standard practice
		//I want to add here that $symptoms is "binded" to the query, so we can change the variable
		// and re-run the query, and it will use the new value of $symptoms.
		$symptom = clean($symptom_array[$i]);
		//execute
		if ($stmt->execute()) {
			//record successfully added
			header("Location: existing-med-conditions.php?token=$token");
		} 
		else {
			echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
		}

	}

	$stmt->close();
	$db->close();
}





?>