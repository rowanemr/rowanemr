<?php
  include 'core/init.php';
  if(!logged_in())
		header("Location: index.php");
  include 'templates/header.php';
  include 'templates/navbar.php';
  include 'templates/patient-intake-content.php';
  include 'templates/footer.php';

 ?>

