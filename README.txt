Rowan EMR Setup Instructions:

1. To install the Rowan EMR Software unzip the file into your directory associated with your remote server.
2. Navigate to Rowan EMR/Version-2.0/core/database/connect.php and alter line 4 to the following:

   $db = new mysqli('[remote address for MySQL server]', '[your username]', '[your password]', 'RowanEMR');

3. Make sure you import and compile the MySQL schema located in: Rowan EMR/Version-2.0/core/ddl/schema.sql
4. Make sure the database name is RowanEMR or you will have to alter line 4 again from above where it says 'RowanEMR'.
5.Setup is complete.