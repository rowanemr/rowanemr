<?php
// AJAX functionality for Scheduler
//var_dump($_POST);

include 'core/init.php';

if ($_POST['action'] == "NEW_APPOINTMENT") {
    new_appointment();
}
elseif($_POST['action'] == "GET_PATIENTS_FOR_DAY") {
    get_patients_for_day();
}
elseif ($_POST['action'] == "DELETE_APPOINTMENT") {
    delete_appointment();
}

function new_appointment()
{
    //Log the action. First argument is the user's id, second is a string describing the action taken.
    create_log($_SESSION['id'], "Created patient appointment");

    global $db;
    // Do things for a new patient
    // This is for an AJAX call
    // We will insert this person into the database, if they exist
    $first_name = trim(clean($_POST['first_name']));
    $last_name = trim(clean($_POST['last_name']));
    $birth_day = clean(trim($_POST['birth_day']));
    $date = clean(trim($_POST['date']));
    $date_sql_friendly = sql_friendly_date($date);
    $timeslot = clean(trim($_POST['timeslot']));
    $timeslot = sql_friendly_timestamp($date." ".$timeslot); //convert something like 12/04/2015 7:30pm to date object
    $patient_id = get_patient_id_create_if_new($first_name, $last_name, $birth_day); //Creates if new
    $token = get_random_token();
    $query = "INSERT INTO Schedule";
    $query .= "(scheduled_patient_id, slot_date, start_time, token, created, last_modified)";
    $query .= "VALUES (?, ?, ?, ?, now(), now())";

    if (!($stmt = $db->prepare($query))) {
        echo "Prepare failed: (" . $db->errno . ") " . $db->error;
    }

    if (!($stmt->bind_param("dsss", $patient_id, $date_sql_friendly, $timeslot, $token))) {
        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    }

    if ($stmt->execute()) {
        //header("Location: ../../"); This will direct to the next form to be added
    } else {
        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
    }
    $slot_id = $stmt->insert_id; // The new slot_id of what we INSERTed
    $db->close();

    // Reply with JSON
    // Let PHP encode the JSON for
    $return_arr = array("slot_id" => $slot_id, "patient_id" => $patient_id, "timeslot" => $timeslot);
    die(json_encode($return_arr));

}

/*
 * Generate a unique random token
 * Tokens are necessary because we don't want patients to enter in data as someone else
 * We could just propagate the patient_id in GET variables, but those can be edited
 * A token that is hard to guess will prevent these issues
 */
function get_random_token()
{
    // No user input
    global $db;
    $token = "";
    $length = 5;
    $chars = "abcdefghijklmnopqrstuvwxyz0123456789"; // Only use lowercase and nums - easy to type on iPad.
    $size = strlen($chars);
    for ($i = 0; $i < $length; $i++) {
        $token .= $chars[rand(0, $size - 1)];
    }
    $result = $db->query("SELECT * FROM Schedule WHERE token = '$token'");
    if ($result->num_rows == 0)
        return $token; //doesn't exist in DB, good, so return it
    else
        return get_random_token(); //try again!
}

function get_patients_for_day()
{
    // Output JSON
    global $db;
    $date = clean($_POST['date']); //the date to fetch
    $date = sql_friendly_date($date);
    // The start_time is a SQL timestamp. Convert to format ex: 07:30PM or 9:30PM (12 hr, AM/PM).
    $query = "SELECT first_name, last_name, patient_id, slot_id, DATE_FORMAT(start_time, '%h:%i%p') AS start_time ";
    $query .= "FROM Schedule JOIN Patients ON ";
    $query .= "Schedule.scheduled_patient_id = Patients.patient_id WHERE slot_date = '$date'";
    $result = $db->query($query);
    $all = $result->fetch_all(MYSQLI_ASSOC); // fetch as associative array, not numeric
    die(json_encode($all));
}

function delete_appointment()
{
    //Log the action. First argument is the user's id, second is a string describing the action taken.
    create_log($_SESSION['id'], "Deleted patient appointment");

    global $db;
    $slot_id = clean($_POST['slot_id']);
    $query = "DELETE FROM Schedule WHERE slot_id = ?";
    /* prepare statement */
    if (!($stmt = $db->prepare($query)))
        echo "Prepare failed: (" . $db->errno . ") " . $db->error;

    if (!($stmt->bind_param("s", $slot_id)))
        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;

    if (!($stmt->execute()))
        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;

    $rows_affected = $stmt->affected_rows;
    $stmt->close();
    if ($rows_affected == 1)
        die("OK"); //no need for JSON here
}

?>